//
// Copyright (C) 2012 Opensim Ltd.
// Author: Tamas Borbely
// Copyright (C) 2013 Thomas Dreibholz
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include <iostream>

#include "REDDropperECN.h"
#include "inet/common/INETUtils.h"

using namespace inet;
using namespace omnetpp;

Define_Module(REDDropperECN);

simsignal_t REDDropperECN::markedSignal = registerSignal("marked");

void REDDropperECN::finish()
{
//    delete[] minths;
//    delete[] maxths;
//    delete[] maxps;
//    delete[] pkrates;
//    delete[] count;
//    delete[] marks;
//    delete marked;
}

REDDropperECN::~REDDropperECN()
{
    delete[] minths;
    delete[] maxths;
    delete[] maxps;
    delete[] pkrates;
    delete[] count;
    delete[] marks;
    q_time = 0;
}

void REDDropperECN::initialize()
{
    AlgorithmicDropperBase::initialize();

    wq = par("wq");
    if (wq < 0.0 || wq > 1.0)
        throw cRuntimeError("Invalid value for wq parameter: %g", wq);

    minths = new double[numGates];
    maxths = new double[numGates];
    maxps = new double[numGates];
    pkrates = new double[numGates];
    count = new double[numGates];
    marks = new double[numGates];

    cStringTokenizer minthTokens(par("minths"));
    cStringTokenizer maxthTokens(par("maxths"));
    cStringTokenizer maxpTokens(par("maxps"));
    cStringTokenizer pkrateTokens(par("pkrates"));
    cStringTokenizer markTokens(par("useEcn"));
    for (int i = 0; i < numGates; ++i)
    {
        minths[i] = minthTokens.hasMoreTokens() ? utils::atod(minthTokens.nextToken()) :
                   (i > 0 ? minths[i-1] : 5.0);
        maxths[i] = maxthTokens.hasMoreTokens() ? utils::atod(maxthTokens.nextToken()) :
                   (i > 0 ? maxths[i-1] : 50.0);
        maxps[i] = maxpTokens.hasMoreTokens() ? utils::atod(maxpTokens.nextToken()) :
                   (i > 0 ? maxps[i-1] : 0.02);
        pkrates[i] = pkrateTokens.hasMoreTokens() ? utils::atod(pkrateTokens.nextToken()) :
                    (i > 0 ? pkrates[i-1] : 150);
        marks[i] = markTokens.hasMoreTokens() ? utils::atod(markTokens.nextToken()) :
                    (i > 0 ? marks[i-1] : 0);
        count[i] = -1;

        if (minths[i] < 0.0)
            throw cRuntimeError("minth parameter must not be negative");
        if (maxths[i] < 0.0)
            throw cRuntimeError("maxth parameter must not be negative");
        if (minths[i] >= maxths[i])
            throw cRuntimeError("minth must be smaller than maxth");
        if (maxps[i] < 0.0 || maxps[i] > 1.0)
            throw cRuntimeError("Invalid value for maxp parameter: %g", maxps[i]);
        if (pkrates[i] < 0.0)
            throw cRuntimeError("Invalid value for pkrates parameter: %g", pkrates[i]);
        if (marks[i] != 0 && marks[i] != 1)
            throw cRuntimeError("Invalid value for marks parameter: %g", marks[i]);
    }

}

/*
 * Checks whether source support ECN and set CE bit. Return true upon success, false otherwise
 */
bool REDDropperECN::markECN(cPacket *packet)
{
    IPv4Datagram* ipDatagram = dynamic_cast<IPv4Datagram*>(packet);
    if (ipDatagram == nullptr) {
        ipDatagram = check_and_cast<IPv4Datagram*>(packet->decapsulate());  // this will create a copy
    }

    if(ipDatagram->getExplicitCongestionNotification() == IP_ECN_NOT_ECT) {
        delete ipDatagram;
        return false;
    } else {
        ipDatagram->setExplicitCongestionNotification(IP_ECN_CE);
        packet->encapsulate(ipDatagram->dup());
        delete ipDatagram;
        return true;
    }
}


bool REDDropperECN::shouldDrop(cPacket *packet)
{
    const int i = packet->getArrivalGate()->getIndex();
    ASSERT(i >= 0 && i < numGates);
    const double minth = minths[i];
    const double maxth = maxths[i];
    const double maxp = maxps[i];
    const double pkrate = pkrates[i];
    const double mark = marks[i];
    const int queueLength = getLength();

    if (queueLength > 0)
    {
        // TD: This following calculation is only useful when the queue is not empty!
        avg = (1 - wq) * avg + wq * queueLength;
    }
    else
    {
        // TD: Added behaviour for empty queue.
        const double m = SIMTIME_DBL(simTime() - q_time) * pkrate;
        avg = pow(1 - wq, m) * avg;
    }


    if (minth <= avg && avg < maxth)
    {
        count[i]++;
        const double pb = maxp * (avg - minth) / (maxth - minth);
        double pa = pb / (1 - count[i] * pb); // TD: Adapted to work as in [Floyd93].
        if(1 - count[i] * pb < 0)
            pa = 1;
        if (dblrand() < pa)
        {
            EV << "Random early packet drop (avg queue len=" << avg << ", pa=" << pb << ")\n";
            count[i] = 0;
            if(!mark) {
                return true;
            }
            else {
                bool ret = markECN(packet);
                if (!ret){
                    return true;
                }
            }
            emit(markedSignal,+1L);
        } else {
            emit(markedSignal,0L);
        }
    }
    else if (avg >= maxth)
    {
        EV << "Avg queue len " << avg << " >= maxth, dropping packet.\n";
        count[i] = 0;
        if (!mark) {
            return true;
        } else {
            bool ret = markECN(packet);
            if (!ret) {
                return true;
            }
        }
        emit(markedSignal,+1L);
    }
    else if (queueLength >= maxth)  // maxth is also the "hard" limit
    {
        EV << "Queue len " << queueLength << " >= maxth, dropping packet.\n";
        count[i] = 0;
        if (!mark) {
            return true;
        } else {
            bool ret = markECN(packet);
            if (!ret) {
                return true;
            }
        }
        emit(markedSignal,+1L);
    }
    else
    {
        count[i] = -1;
        emit(markedSignal,0L);
    }
    return false;
}

void REDDropperECN::sendOut(cPacket *packet)
{
    AlgorithmicDropperBase::sendOut(packet);
    // TD: Set the time stamp q_time when the queue gets empty.
    const int queueLength = getLength();
    if (queueLength == 0)
        q_time = simTime();
}

// checks if packet is a datagram (L3 device like a router), otherwise looks in encapsulated packet
// (useful is this dropper is used in a L2 switch)
IPv4Datagram* REDDropperECN::getIPDatagram(cPacket* packet) {

    IPv4Datagram* ipDatagram = dynamic_cast<IPv4Datagram*>(packet);
    if (ipDatagram == nullptr) {
        ipDatagram = check_and_cast<IPv4Datagram*>(packet->getEncapsulatedPacket());
    }
    return ipDatagram;
}
