//
// Copyright (C) 2012 Opensim Ltd.
// Author: Tamas Borbely
// Copyright (C) 2013 Thomas Dreibholz
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef __INET_REDDROPPERECN_H_
#define __INET_REDDROPPERECN_H_

#include "inet/common/INETDefs.h"
#include "AlgorithmicDropperBase.h"
#include "inet/networklayer/ipv4/IPv4Datagram.h"
#include "inet/networklayer/ipv4/IPv4Datagram_m.h"

namespace inet {
/**
 * Implementation of Random Early Detection (RED).
 */
class REDDropperECN : public AlgorithmicDropperBase
{
  protected:

    double wq;
    double *minths;
    double *maxths;
    double *maxps;
    double *pkrates;
    double *count;
    double *marks;

    double avg;
    simtime_t q_time;

    static simsignal_t markedSignal;

  public:
    REDDropperECN() : wq(0), minths(NULL), maxths(NULL), maxps(NULL), avg(0.0) {}

    bool markECN(cPacket *packet);

  protected:
    virtual ~REDDropperECN();
    virtual void initialize();
    virtual void finish();
    virtual bool shouldDrop(cPacket *packet);
    virtual void sendOut(cPacket *packet);
    virtual IPv4Datagram* getIPDatagram(cPacket* packet);
};

}

#endif
