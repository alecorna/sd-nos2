//
// Copyright (C) 2004 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef __INET_DCTCP_H
#define __INET_DCTCP_H

#include "inet/common/INETDefs.h"
//#include "distrib.h"

#include "inet/transportlayer/tcp/flavours/TCPTahoeRenoFamily.h"
#include "inet/transportlayer/tcp/flavours/TCPBaseAlg.h"

namespace inet {

namespace tcp {

/**
 * State variables for DCTCP family.
 */
class INET_API DCTCPFamilyStateVariables : public TCPTahoeRenoFamilyStateVariables
{
  public:
    DCTCPFamilyStateVariables();
    virtual std::string detailedInfo() const override;

  public:
    //DCTCP
    double dctcp_marked;
    double dctcp_windEnd;
    double dctcp_bytesAcked;
    double dctcp_bytesMarked;
    bool dctcp_CWR;
    double dctcp_total;
    double dctcp_totalSent;
    double dctcp_alpha;
    double dctcp_gamma;

};


/**
 * Implements DCTCP
 */
class INET_API DCTCP : public TCPTahoeRenoFamily
{
  protected:
    DCTCPFamilyStateVariables *&state; // alias to TCLAlgorithm's 'state'

    /** Create and return a TCPRenoStateVariables object. */
    virtual DCTCPFamilyStateVariables *createStateVariables() {
        return new DCTCPFamilyStateVariables();
    }

    /** Utility function to recalculate ssthresh */
    virtual void recalculateSlowStartThreshold();

    /** Redefine what should happen on retransmission */
    virtual void processRexmitTimer(TCPEventCode& event);

    //virtual void processPaceTimer(TCPEventCode& event);

  public:
    /** Ctor & Dtor*/
    DCTCP();
    ~DCTCP();

    /** Redefine what should happen when data got acked, to add congestion window management */
    virtual void receivedDataAck(uint32 firstSeqAcked);

    /** Redefine what should happen when dupAck was received, to add congestion window management */
    virtual void receivedDuplicateAck();

    virtual void initialize() override;

  public:

    cOutVector *fractionMarkedVector; // will record F samples, the fraction of marked bytes within the last window of data
    cOutVector *alphaVector; // will record dctcp alpha estimation
    cOutVector *markRunVector; // will record exact sequence of CE marks as notified by the receiver
};
}
}
#endif
