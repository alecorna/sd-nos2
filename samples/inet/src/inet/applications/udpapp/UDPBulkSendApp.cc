//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "inet/applications/udpapp/UDPBulkSendApp.h"

#include "inet/applications/base/ApplicationPacket_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"
#include "inet/transportlayer/udp/UDPPacket_m.h"
#include "inet/networklayer/ipv4/IPv4Datagram_m.h"
#include "inet/linklayer/ethernet/Ethernet.h"
#include <string>
#include <algorithm>
#include <locale>

using namespace std;

namespace inet {

Define_Module(UDPBulkSendApp);

simsignal_t UDPBulkSendApp::interArrivalTime = registerSignal("interArrivalTime");
simsignal_t UDPBulkSendApp::flwSizeSignal = registerSignal("flwSize");

void UDPBulkSendApp::initialize(int stage)
{
    UDPBasicApp::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        flowController = dynamic_cast<FlowGenerator*>(getModuleByPath("^.flowGenerator"));
    }
}

void UDPBulkSendApp::finish()
{
    UDPBasicApp::finish();
    delete flowController;
}

// XXX Alessandro sends an entire flow at once, creating datagrams of max size
void UDPBulkSendApp::bulkSend()
{
    long flowLength = flowController->getNewFlowLength();
    char* strAddr;
    flowController->getRandomDestination(&strAddr);
    L3Address destAddr;
    L3AddressResolver().tryResolve(strAddr, destAddr);
    if (destAddr.isUnspecified()) {
        EV_ERROR << "cannot resolve destination address: " << strAddr << endl;
    }
    emit(connectSignal,1L);

    EV_INFO << "Send new UDP flow (" << flowLength << "B)" << endl;
    cPacket* pk = new cPacket();
    pk->setByteLength(flowLength);
    emit(sentPkSignal, pk);
    delete pk;

    long byteSent = 0;
    do {
        std::ostringstream str;
        str << packetName << "-" << numSent;

        long toSend = min(par("messageLength").longValue(), flowLength - byteSent);
        ApplicationPacket *payload = new ApplicationPacket(str.str().c_str());
        payload->setByteLength(toSend);
        payload->setSequenceNumber(byteSent);
        payload->setLast(false);
        if (byteSent + toSend == flowLength) {
            EV_INFO << "Last packet of flow sent " << payload << endl;
            payload->setLast(true);
        }
        socket.sendTo(payload, destAddr, destPort);
        numSent++;
        byteSent = byteSent + toSend;

    } while (byteSent < flowLength);
}

void UDPBulkSendApp::processStart()
{
    socket.setOutputGate(gate("udpOut"));
    const char *localAddress = par("localAddress");
    socket.bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
    setSocketOptions();

    selfMsg->setKind(SEND);
    processSend();
}

void UDPBulkSendApp::processSend()
{
    bulkSend();
    simtime_t d = flowController->getSleepTime();
    if (stopTime < SIMTIME_ZERO || d < stopTime) {
        selfMsg->setKind(SEND);
        scheduleAt(d, selfMsg);
    } else {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}

bool UDPBulkSendApp::handleNodeStart(IDoneCallback *doneCallback)
{
    startTime = flowController->getSleepTime(); // XXX Alessandro
    simtime_t start = std::max(startTime, simTime());
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}


} // namespace inet

