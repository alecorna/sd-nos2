//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

package inet.applications.contract;

//
// TODO auto-generated type
//
simple IStatisticalTrafficApp
{
    parameters:
        int flowLength  @unit(B) = default(1MB);	// manually set flow length
        int minFlowSize @unit(B);
        int maxFlowSize @unit(B);
        string flowLengthDistribution = default("");	// exp , pareto, uniform	 
        int maxFlows = default(0);
        double expFlowSizeMean @unit(B) = default(-1B);
        // POISSON, ONOFF, if empty uses interFlowArrivalTime as fixed interval
        string flowArrivalProcess = default("POISSON");	
        // disabled, means uses exponential with mean interFlowArrivalTime
        volatile double offeredLoad = default(1);	// must be between 0 and 1 
        volatile double interFlowArrivalTime @unit(s) = default(1ms);
        bool considerAckLoad = default(true);
        
        double paretoShape = default(-1);
       
        @signal[flowCompletionTime](type=simtime_t);
        @signal[connectionPeers](type=long);
        @signal[flwSize](type=long);
        @signal[interArrivalTime](type=simtime_t);
        
        @statistic[fct](source=flowCompletionTime; unit=s; record=histogram,vector);
        @statistic[flwSize](title="Flow size distribution"; source=flwSize; record=mean,vector,sum);
        @statistic[connectionPeers](title="Random destination distribution"; record=histogram);
        @statistic[interArrivalTime](title="Inter-arrival time";record=vector,mean);
        
}
