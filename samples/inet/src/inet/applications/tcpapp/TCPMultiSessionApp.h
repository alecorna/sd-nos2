//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_TCPMULTISESSIONAPP_H
#define __INET_TCPMULTISESSIONAPP_H

#include "TCPSessionApp.h"
#include "inet/transportlayer/contract/tcp/TCPSocketMap.h"

namespace inet {

class INET_API TCPMultiSessionApp : public TCPSessionApp {

protected:
    const char* localAddress;
    int localPort;
    TCPSocketMap socketMap;
    unsigned generated = 0;
    unsigned maxNumFlows;

    static simsignal_t connectionPeerSignal;  // XXX Alessandro
    static simsignal_t flwSizeSignal;           // XXX Alessandro

    struct TCPSendContext
    {
        CommandVector* commands;
        TCPSocket* socket;
        int commandIndex = 0;
        TCPSendContext(CommandVector* cmd, TCPSocket* s) { commands = cmd; socket = s; }
    };

protected:
    virtual void initialize(int stage);
    virtual void socketClosed(int connId, void *ptr);
    virtual void handleMessage(cMessage* msg) override;
    virtual void scheduleNextConnect();
    virtual void sendData(cMessage* msg);
    virtual void sendPacket(cPacket *msg, TCPSocket* sock);
    virtual void parseScript(const char *script, CommandVector* cmd);
    virtual void socketEstablished(int connId, void *ptr);
    virtual void socketDeleted(int connId, void *yourPtr) override;
    virtual void handleTimer(cMessage *msg) override;
    virtual void connect(cMessage* msg);
    virtual void close(cMessage* msg);
    virtual void finish() override;

};

} /* namespace inet */

#endif /* __INET_TCPMULTISESSIONAPP_H */
