//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TCPMultiSessionApp.h"
#include "inet/common/RawPacket.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"

namespace inet {

Define_Module(TCPMultiSessionApp);

#define MSGKIND_CONNECT    1
#define MSGKIND_SEND       2
#define MSGKIND_CLOSE      3

simsignal_t TCPMultiSessionApp::connectionPeerSignal = registerSignal("connectionPeers");
simsignal_t TCPMultiSessionApp::flwSizeSignal = registerSignal("flwSize");

void TCPMultiSessionApp::scheduleNextConnect() {

    // stop generation process if limit is set and reached
    if(maxNumFlows > 0 && generated >= maxNumFlows){
        return;
    }

    // start counting generated flows after warmup period
    if(simTime() >= getSimulation()->getWarmupPeriod()){
        generated++;
    }
    CommandVector* commands = new CommandVector();

    // in the script there is the definition of chunks of app packets <numBytes>;<numByes>;<numBytes>
    // the interpacket time is chosen according to exponential distribution with mean tSend
    // if the script is empty nothing will be added and sendBytes will be tx in an unique app packet
    const char *script = par("sendScript");
    parseScript(script, commands);

    // if no sendScript generate traffic according to distribution
    if (commands->size() == 0){
        tSend = (simtime_t)par("tSend");    // inter-arrival time between connection opening and packet send

        if(flowController) {
            sendBytes = flowController->getNewFlowLength(); // random flow length or from sendBytes
        } else {
            sendBytes = (int)par("sendBytes");
        }
        emit(flwSizeSignal, sendBytes);
        commands->push_back(Command(tSend, sendBytes));
    }

    // get new flow arrival time
    simtime_t d;
    if(flowController) {
      d = flowController->getSleepTime();
      simtime_t ia = d - simTime();
      EV_INFO << "Inter-arrival time " << SIMTIME_STR(ia*1e3) << "ms" << endl;
    } else if (hasPar("interFlowArrivalTime")) {
       d = simTime() + (simtime_t)par("interFlowArrivalTime");
    } else {
        throw cRuntimeError("Inter-arrival time not provided and flow controller not present");
    }
    EV << "Next flow at " << SIMTIME_STR(d*1e3) << "ms" <<endl;

    timeoutMsg->setKind(MSGKIND_CONNECT);
    timeoutMsg->setContextPointer(commands);
    scheduleAt(d, timeoutMsg);

}

void TCPMultiSessionApp::initialize(int stage)
{
    TCPAppBase::initialize(stage);
    if (stage == INITSTAGE_LOCAL) {
        activeOpen = par("active");
        tClose = par("tClose");
        localAddress = par("localAddress");
        localPort = par("localPort");
        maxNumFlows = (int)par("maxFlows");
    }
    else if (stage == INITSTAGE_APPLICATION_LAYER) {
        timeoutMsg = new cMessage("timer");
        nodeStatus = dynamic_cast<NodeStatus *>(findContainingNode(this)->getSubmodule("status"));

        if (isNodeUp()) {
            scheduleNextConnect();
        }
    }
}

void TCPMultiSessionApp::parseScript(const char *script, CommandVector* cmd)
{
    const char *s = script;

    EV_DEBUG << "parse script \"" << script << "\"\n";
    while (*s) {
        // parse time
        while (isspace(*s))
            s++;

        if (!*s || *s == ';')
            break;

//        const char *s0 = s;
//        simtime_t tSend = strtod(s, &const_cast<char *&>(s));
//
//        if (s == s0)
//            throw cRuntimeError("Syntax error in script: simulation time expected");

        // parse number of bytes
//        while (isspace(*s))
//            s++;

        if (!isdigit(*s))
            throw cRuntimeError("Syntax error in script: number of bytes expected");

        long numBytes = strtol(s, nullptr, 10);

        while (isdigit(*s))
            s++;

        // add command
        simtime_t tSend = (simtime_t)par("tSend"); // inter app packet time

        EV_DEBUG << " add command (" << tSend << "s, " << "B)\n";
        cmd->push_back(Command(tSend, numBytes));

        // skip delimiter
        while (isspace(*s))
            s++;

        if (!*s)
            break;

        if (*s != ';')
            throw cRuntimeError("Syntax error in script: separator ';' missing");

        s++;

        while (isspace(*s))
            s++;
    }
    EV_DEBUG << "parser finished\n";
}

void TCPMultiSessionApp::connect(cMessage* msg)
{
    // connect
    int connectPort = par("connectPort");
    char* connectAddress;
    if(strcmp(par("connectAddress").stringValue(),"random")==0) {
        if(flowController){
            int destination = flowController->getRandomDestination(&connectAddress);
            emit(connectionPeerSignal,destination);
        } else {
            throw cRuntimeError("Flow controller is unspecified");
        }
    } else {
        connectAddress = new char[1+strlen(par("connectAddress").stringValue())]();
        strcpy(connectAddress, par("connectAddress").stringValue());
    }

    L3Address destination;
    L3AddressResolver().tryResolve(connectAddress, destination);
    if (destination.isUnspecified()) {
        EV_ERROR << "Connecting to " << connectAddress << " port=" << connectPort << ": cannot resolve destination address\n";
    }
    else {
        EV_INFO << "Connecting to " << connectAddress << "(" << destination << ") port=" << connectPort << endl;

        CommandVector* cmds = (CommandVector*)msg->getContextPointer();

        // create new socket to handle the connection.
        // It will be destroyed in socketClosed().
        TCPSocket* sock = new TCPSocket();
        TCPSendContext* sendContext = new TCPSendContext(cmds, sock);
        sock->readDataTransferModePar(*this);
        sock->bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
        sock->setOutputGate(gate("tcpOut"));
        sock->connect(destination, connectPort);
        sock->setCallbackObject(this, (void*)sendContext);    // ADDED no socket ref but commands ref
        socketMap.addSocket(sock);

        numSessions++;
        emit(connectSignal, 1L);
    }
    delete connectAddress;
}

void TCPMultiSessionApp::socketClosed(int connId, void *ptr)
{
    TCPAppBase::socketClosed(connId, ptr);
    delete socketMap.removeSocket(((TCPSendContext*)ptr)->socket); // remove from connections and delete
}

void TCPMultiSessionApp::socketDeleted(int connId, void *yourPtr) {
    delete (TCPSendContext*)yourPtr;
}

void TCPMultiSessionApp::handleTimer(cMessage *msg)
{
    switch (msg->getKind()) {
        case MSGKIND_CONNECT:
            if (activeOpen)
                connect(msg); // sending will be scheduled from socketEstablished()
            else
                ; //TODO
            scheduleNextConnect();
            break;

        case MSGKIND_SEND:
            sendData(msg);
            break;

        case MSGKIND_CLOSE:
            close(msg);
            break;

        default:
            throw cRuntimeError("Invalid timer msg: kind=%d", msg->getKind());
    }
}

void TCPMultiSessionApp::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage())
        handleTimer(msg);
    else {
        TCPSocket* sock = socketMap.findSocketFor(msg);
        sock->processMessage(msg);
    }
}

void TCPMultiSessionApp::finish() {
    TCPSessionApp::finish();
    // free memory
    socketMap.deleteSockets();
}

void TCPMultiSessionApp::sendData(cMessage* msg)
{
    TCPSendContext* sendContext = (TCPSendContext*)msg->getContextPointer();
    long numBytes = sendContext->commands->at(sendContext->commandIndex).numBytes;

    EV_INFO << "sending data with " << numBytes << " bytes\n";

    sendPacket(createDataPacket(numBytes),sendContext->socket);

    // other data to send : reschedule message (it is already of kind SEND)
    if (++sendContext->commandIndex < (int)sendContext->commands->size()) {
        simtime_t tSend = simTime() + sendContext->commands->at(sendContext->commandIndex).tSend;
        scheduleAt(tSend, msg);
    }
    else {
        cMessage* closeMsg = new cMessage("close timer");
        closeMsg->setKind(MSGKIND_CLOSE);
        closeMsg->setContextPointer(sendContext->socket);
        // tClose time offset from when send last data to close (may be seen as processing time)
        scheduleAt(tClose+simTime(), closeMsg);

        delete sendContext->commands;
        delete msg;

    }
}

// Send data packet using the socket inside timeoutMsg (last socket established)
void TCPMultiSessionApp::sendPacket(cPacket *msg, TCPSocket* sock)
{
    int numBytes = msg->getByteLength();
    emit(sentPkSignal, msg);
    sock->send(msg);
    packetsSent++;
    bytesSent += numBytes;
}

void TCPMultiSessionApp::close(cMessage* msg)
{
    EV_INFO << "issuing CLOSE command\n";
    TCPSocket* sock = (TCPSocket*)(msg->getContextPointer());
    delete msg;
    sock->close();
    emit(connectSignal, -1L);
}

void TCPMultiSessionApp::socketEstablished(int connId, void *ptr)
{
    TCPAppBase::socketEstablished(connId, ptr);

    TCPSendContext* sc = (TCPSendContext*)ptr;
    ASSERT(sc->commandIndex == 0);
    cMessage* sendMsg = new cMessage("send timer");
    sendMsg->setKind(MSGKIND_SEND);
    // add to the timeout a reference to the socket and to the command vector,
    //it will be used by sendPacket() to identify the socket willing to send data
    sendMsg->setContextPointer(ptr);
    simtime_t tSend = simTime() + sc->commands->at(sc->commandIndex).tSend;
    scheduleAt(std::max(tSend, simTime()), sendMsg);
}


} /* namespace inet */
