//
// Copyright (C) 2004 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "inet/applications/tcpapp/TCPSinkApp2.h"

#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeStatus.h"
#include "GenericAppMsg_m.h"

namespace inet {

Define_Module(TCPSinkApp2);

simsignal_t TCPSinkApp2::rcvdPkSignal = registerSignal("rcvdPk");
simsignal_t TCPSinkApp2::flwSize = registerSignal("flwSize");
simsignal_t TCPSinkApp2::flowGoodputSignal = registerSignal("connectionGoodput");
simsignal_t TCPSinkApp2::flowSourceIDSignal = registerSignal("flowSourceID");

const char* TCPSinkApp2::classNames[NUM_FLOW_CLASSES] = {"mice","medium","elephant"};

void TCPSinkApp2::initialize(int stage)
{
    cSimpleModule::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        bytesRcvd = 0;
        WATCH(bytesRcvd);
    }
    else if (stage == INITSTAGE_APPLICATION_LAYER) {
        bool isOperational;
        NodeStatus *nodeStatus = dynamic_cast<NodeStatus *>(findContainingNode(this)->getSubmodule("status"));
        isOperational = (!nodeStatus) || nodeStatus->getState() == NodeStatus::UP;
        if (!isOperational)
            throw cRuntimeError("This module doesn't support starting in node DOWN state");

        const char *localAddress = par("localAddress");
        int localPort = par("localPort");

        // XXX Alessandro
        for(int i=0; i<NUM_FLOW_CLASSES; i++){
            // register signal dynamically
            char signalName[32];
            sprintf(signalName, "fct-%s", TCPSinkApp2::classNames[i]);
            flowCompletionTimeSignal[i] = registerSignal(signalName);

            // add statistic recorder dynamically
            char statisticName[32];
            sprintf(statisticName, "fct-%s", TCPSinkApp2::classNames[i]);
            cProperty *statisticTemplate = getProperties()->get("statisticTemplate", "fct");
            getEnvir()->addResultRecorders(this, flowCompletionTimeSignal[i], statisticName, statisticTemplate);

            // register signal dynamically
            sprintf(signalName, "avgThroughput-%s", TCPSinkApp2::classNames[i]);
            flowAvgThruputSignal[i] = registerSignal(signalName);

            // add statistic recorder dynamically
            sprintf(statisticName, "avgThroughput-%s", TCPSinkApp2::classNames[i]);
            statisticTemplate = getProperties()->get("statisticTemplate", "avgThroughput");
            getEnvir()->addResultRecorders(this, flowAvgThruputSignal[i], statisticName, statisticTemplate);

        }
        thresholdController = dynamic_cast<ThresholdController*>(getModuleByPath("<root>.thresholdController"));

        // end changes here
        socket.setOutputGate(gate("tcpOut"));
        socket.readDataTransferModePar(*this);
        socket.bind(localAddress[0] ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
        socket.listen();
    }
}

// Sink application has been modified to handle flow completion time computation
// when we are using object transfer mode, that is we rx at application
// layer a unique packet alreay reassembled by TCP
void TCPSinkApp2::handleMessage(cMessage *msg)
{
    if (msg->getKind() == TCP_I_PEER_CLOSED) {
        // we close too
        msg->setName("close");
        msg->setKind(TCP_C_CLOSE);
        send(msg, "tcpOut");
    }
    else if (msg->getKind() == TCP_I_DATA || msg->getKind() == TCP_I_URGENT_DATA) {
        cPacket *pk = PK(msg);
        long packetLength = pk->getByteLength();
        bytesRcvd += packetLength;
        emit(rcvdPkSignal, pk);

        if (socket.getDataTransferMode() == TCP_TRANSFER_OBJECT && thresholdController) {   // XXX Alessandro

            simtime_t fct = simTime() - pk->getCreationTime();
            int flowClass = 0; // TODO thresholdController->getFlowClass(packetLength);
            int senderHostID = (check_and_cast<GenericAppMsg*>(pk))->getSenderHostID();
            // take into account warmup period
            if(simTime() >= getSimulation()->getWarmupPeriod()) {
                emit(flowCompletionTimeSignal[flowClass], fct);
                emit(flowAvgThruputSignal[flowClass], (double)pk->getBitLength() / fct);
                emit(flwSize, packetLength);
                emit(flowSourceIDSignal, senderHostID);
                emit(flowGoodputSignal, (double)pk->getBitLength() / (simTime()-pk->getCreationTime()));
            }

        }
        delete msg;
    }
    else {
        // must be data or some kind of indication -- can be dropped
        delete msg;
    }
}

void TCPSinkApp2::finish()
{
}

void TCPSinkApp2::refreshDisplay() const
{
    std::ostringstream os;
    os << TCPSocket::stateName(socket.getState()) << "\nrcvd: " << bytesRcvd << " bytes";
    getDisplayString().setTagArg("t", 0, os.str().c_str());
}

} // namespace inet

