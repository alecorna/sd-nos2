//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "DWRR.h"
#include "DropTailQueueII.h"
#include "PQScheduler.h"
#include "PFCPerFlowController.h"
Define_Module(DWRR);

bool DWRR::schedulePacket()
{
    int numQueues = gateSize("in");
    int nonEmptyQueues[numQueues];
    int totalNonEmptyQueues=0;
    int queueToServe=0;
    int pq=0;

    for(int i=0; i<inputQueues.size();i++) nonEmptyQueues[i]=0;

    // First try to serve the pause queue
    if(dynamic_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())!= nullptr){
        if( !check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->isEmpty() ){
            check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->requestPacket();
            EV << "Requesting packet from Pause Queue " << queueToServe << "\n";
            return true;
        }
    }
    for(pq=0; pq<(int) par("numberPriorities"); pq++){
        for(int i=1; i<numQueues; i++){ // start from i=1 because i=0 is the pause queue
            ASSERT(dynamic_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule()) != nullptr);

            if(!check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->isEmpty(pq)){
                nonEmptyQueues[i]=1;
                totalNonEmptyQueues++;
            }
        }
        if(totalNonEmptyQueues != 0) break;
    }
    if(totalNonEmptyQueues == 0){
        EV << "All queues are empty\n";
        return false;
    }

    for(int i=0; i<numQueues; i++){
        EV << nonEmptyQueues[i] << " ";
    }
    EV << "\n";

    queueToServe=getWeightedIndexToServe(nonEmptyQueues, totalNonEmptyQueues, gateSize("in"), pq);

    if(queueToServe < 0) throw cRuntimeError("Queue to serve index is negative!");

    EV << "Queue to serve: "<< queueToServe <<"\n";
    EV << "Requesting packet from queue " << queueToServe << "\n";

    check_and_cast<PQScheduler *>(gate("in",queueToServe)->getPathStartGate()->getOwnerModule())->requestPacket(pq);
    return true;
}


double* DWRR::getDiversityIndices(int* nonEmptyQueues, int totalNonEmptyQueues, int numQueues, int PQIndex){
    double diversity[numQueues];
    double *ccdfDiversity = new double[numQueues];
    int sum=0;

    for(int i=0; i<numQueues; i++){
        ccdfDiversity[i]=0;
        diversity[i]=0;
    }

    if(dynamic_cast<PFCPerFlowController *>(getModuleByPath(par("pfcControllerPath"))) == nullptr){
        throw cRuntimeError("PFCController is not a per flow controller");
    }

    PFCPerFlowController *pfcController = check_and_cast<PFCPerFlowController *>(getModuleByPath(par("pfcControllerPath")));

    for(int inputQueue=0; inputQueue < numQueues; inputQueue++){
        if(nonEmptyQueues[inputQueue]==0) continue;
        //diversity[inputQueue] = 1;
        diversity[inputQueue] = pfcController->getDiversityPerInput(inputQueue-1, PQIndex);
        sum += diversity[inputQueue];
    }

    if(sum==0){
        sum=totalNonEmptyQueues;
        for(int inputQueue=0; inputQueue < numQueues; inputQueue++){
                diversity[inputQueue] = nonEmptyQueues[inputQueue];
        }
    }

    for(int inputQueue=0; inputQueue < numQueues; inputQueue++){
        diversity[inputQueue] = diversity[inputQueue]/sum;
    }

    ccdfDiversity[0]=diversity[0];
    for(int i=1; i < numQueues; i++){
        ccdfDiversity[i] += ccdfDiversity[i-1] +  diversity[i];
    }

    // TODO: REMOVE
    EV << "SUM="<<sum<<" ccdfDiversity=\n";
    for(int i=0; i<numQueues; i++){
        EV << ccdfDiversity[i] << " ";
    }
    EV << "\n";


    return ccdfDiversity;
}


int DWRR::getWeightedIndexToServe(int* nonEmptyQueues, int totalNonEmptyQueues, int numQueues, int PQIndex){
    double *ccdf = getDiversityIndices(nonEmptyQueues, totalNonEmptyQueues, numQueues, PQIndex);
    double rNum;
    rNum=uniform(0, 1);

    for(int i=0; i<numQueues; i++){
        EV << ccdf[i] << " ";
    }
    EV << "\n";

    for(int i=0; i<numQueues; i++){
        if(rNum < ccdf[i]){
            EV << "Serving queue " << i << "\n";
            return i;
        }
    }
    return -1;
}

