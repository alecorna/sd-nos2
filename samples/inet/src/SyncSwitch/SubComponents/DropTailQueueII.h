//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_DROPTAILQUEUEII_H_
#define __INET_DROPTAILQUEUEII_H_

#include <omnetpp.h>
#include "inet/common/queue/DropTailQueue.h"
#include "inet/common/queue/IQueueAccess.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "inet/networklayer/common/L3AddressResolver.h"

using namespace inet;

class FlowInformation {
    public:
        L3Address srcIP;
        L3Address dstIP;
        int srcPort=0;
        int dstPort=0;
        int SWOutPort=0;
    public:
        bool operator==(FlowInformation flow1){
            if(flow1.srcIP == srcIP)
                if(flow1.dstIP == dstIP)
                    if(flow1.srcPort == srcPort)
                        if(flow1.dstPort == dstPort)
                            return true;
            return false;
        }
        //bool compareFlows(FlowInformation flow1, FlowInformation flow2);

};

class DropTailQueueII : public DropTailQueue
{
  protected:
    enum OperationalState { WORKING = 1, PAUSED =0};
    cMessage *pauseMessage;
    int state=1;
    bool inputPaused = false;
    double restartTime=0;
    std::map<FlowInformation, int> currentFlows;


  public:
    virtual int getLengthFrames();
    virtual int getLengthBytes();
    virtual bool isEmpty() override;
    virtual void pauseQueue(double timeSlots);
    virtual int getFrameCapacity();
    virtual double getPercentageFrameOccupancy();
    virtual void setInputPaused(bool value);
    virtual int getNextPacketLengthBytes();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    virtual cMessage *dequeue() override;
    virtual void sendOut(cMessage *msg) override;
    virtual void finish() override;

};



#endif
