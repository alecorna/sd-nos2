//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "PFCPerFlowController.h"
#include "RdmaApp/RDMABasicApp.h"
#include "RdmaApp/RDMA.h"
#include "inet/networklayer/ipv4/IPv4Datagram_m.h"
#include "RdmaApp/RDMAPacket_m.h"

Define_Module(PFCPerFlowController);

void PFCPerFlowController::initialize(){

    //numPorts = (int) par("numberPorts");
    numPorts = getModuleByPath("^.eth[0]")->getVectorSize();

    if((bool) par("isSwitch") == true)
        relay = check_and_cast<PQRelay *>(getModuleByPath(par("relayPath")));

    PFCFrameXON = par("PFCFramesXON");
    PFCFrameXOFF = par("PFCFramesXOFF");

    PFCBytesXON = par("PFCBytesXON");
    PFCBytesXOFF = par("PFCBytesXOFF");

    //numberOfPorts = (int) par("numberPorts");
    numberOfPorts = numPorts;
    numberOfPriorities = (int) par("numberPriorities");

    inputStates = new int*[numberOfPorts];
    outputStates = new int*[numberOfPorts];
    capacityOPQ = new int*[numberOfPorts];
    pfcEnabledOnQueue = new int[numberOfPriorities];

    for(int i=0; i<numberOfPorts; i++){
        outputStates[i]= new int[numberOfPriorities];
        capacityOPQ[i]= new int[numberOfPriorities];
        inputStates[i] = new int[numberOfPriorities];
    }
    for(int i=0; i<numberOfPorts; i++){
        for(int j=0; j<numberOfPriorities; j++){
            inputStates[i][j]=0;
            outputStates[i][j]=0;
            capacityOPQ[i][j]=0;
            pfcEnabledOnQueue[j]=0;
        }
    }



    const char *enabledPQs = par("PFCenabledPQs");
    cStringTokenizer tokenizer(enabledPQs);
    const char *token;

    while ((token = tokenizer.nextToken()) != nullptr) {
        pfcEnabledOnQueue[atoi(token)]=1;
    }

    currentFlows = new std::list<observedFlow_t>*[numPorts];
    blockedFlows = new std::list<blockedFlow_t>*[numPorts];

    for(int i=0; i< numPorts; i++){
        currentFlows[i] = new std::list<observedFlow_t>[numberOfPriorities];
        blockedFlows[i] = new std::list<blockedFlow_t>[numberOfPriorities];
    }

    WATCH(numPorts);
}

int PFCPerFlowController::notifyPacketArrival(EtherFrameCoS *frame){
    Enter_Method("notifyPacketArrival(...)");
    EV << "ENTERING PER FLOW PACKET NOTIFICATION\n";

    if(pfcEnabledOnQueue[frame->getPcp()] == 0) return frame->getPcp();

    updateQueueFlowState(frame, 1);

    if((bool) par("isSwitch") == false)
        runPFCHost(frame, PKT_ARRIVAL);
    else
        runPFCSwitch(frame, PKT_ARRIVAL);

    return frame->getPcp();
}

void PFCPerFlowController::notifyPacketDeparture(EtherFrameCoS *frame){
    Enter_Method("notifyPacketDeparture(...)");
    EV << "ENTERING PER FLOW PACKET NOTIFICATION\n";

    if(pfcEnabledOnQueue[frame->getPcp()] == 0) return;

    updateQueueFlowState(frame, -1);

    if((bool) par("isSwitch"))
        runPFCSwitch(frame, PKT_DEPARTURE);
    else
        runPFCHost(frame, PKT_DEPARTURE);
}

/*void PFCPerFlowController::runPFCSwitch(EtherFrameCoS *frame, int triggerState){
    int sourcePort = frame->getArrivalPort();
    int PQIndex = frame->getPcp();
    int queueOccupancyFrames = getTotalFramesPerInputPriority(sourcePort, PQIndex);

    int flowIndex = 0;
    FlowInformation *heaviestFlow;

    if(triggerState == PKT_ARRIVAL){
            if((queueOccupancyFrames + 1) > PFCFrameXON){
                while(isFlowBlocked(getHeaviestFlow(flowIndex))){ // get heaviest unblocked flow
                    flowIndex++;
                }
                heaviestFlow = getHeaviestFlow(flowIndex);
                blockedFlows.insert(*heaviestFlow, simTime());
                generateAndSendPerFlowPauseFrame(sourcePort, PQIndex, frame, heaviestFlow, getStandardPauseTime());
            }
    }
    else{
        if(queueOccupancyFrames <= PFCFrameXOFF && blockedFlows.size() > 0){
            // Get oldest blocked flow and unblock it
            generateAndSendPerFlowPauseFrame(sourcePort, PQIndex, frame, heaviestFlow, 0);
        }
    }
    // Find heaviest flow in the queue



}*/

/*
 * Check if the flow containing a newly arrived/departed packet is already present in the
 * local flow map. If not add it.
 */
void PFCPerFlowController::updateQueueFlowState(EtherFrameCoS* frame, int increment){
    FlowInformation incomingFlow;
    int sourcePort = frame->getArrivalPort();
    int PQIndex = frame->getPcp();
    EV << "Checking source port " << sourcePort << " PQ index " << PQIndex << "\n";
    EV << "Current buffer state:\n";
    for(auto it=currentFlows[sourcePort][PQIndex].begin(); it != currentFlows[sourcePort][PQIndex].end(); it++){
        EV << "Count: " << it->second << " for outport " << it->first.SWOutPort << " of " << it->first.srcIP.str() << "-->" << it->first.dstIP.str() << " " << it->first.srcPort << "-->" << it->first.dstPort << "\n";
    }
    EV << "End check\n";
    if(dynamic_cast<IPv4Datagram *>(frame->getEncapsulatedPacket()) != nullptr) {
        IPv4Datagram *datagram = check_and_cast<IPv4Datagram *>(frame->getEncapsulatedPacket());
        if(datagram->getTransportProtocol() == IP_PROT_ROCE || datagram->getTransportProtocol() == IP_PROT_UDP){
            UDPPacket *udpPkt = check_and_cast<UDPPacket *>(datagram->getEncapsulatedPacket());
            incomingFlow.srcIP = datagram->getSrcAddress();
            incomingFlow.dstIP = datagram->getDestAddress();
            incomingFlow.srcPort = udpPkt->getSrcPort();
            incomingFlow.dstPort = udpPkt->getDestPort();
            incomingFlow.SWOutPort = frame->getOutPort();

            updateOrInsertFlow(incomingFlow, sourcePort, PQIndex, increment);
        }
    }
}

/*
 * Check if the flow is already monitored. If yes modify it's packet count and move it inside the list
 * so that the sorted order is rpeserved. If not add it to the tail.
 */
void PFCPerFlowController::updateOrInsertFlow(FlowInformation incomingFlow, int sourcePort, int PQIndex, int increment){
    std::list<observedFlow_t>::iterator it;

    auto listStart = currentFlows[sourcePort][PQIndex].begin();
    auto listEnd = currentFlows[sourcePort][PQIndex].end();

    for(it=listStart; it != listEnd; ++it){
        // Match found. Insert in the propper sorted position.
        if(it->first == incomingFlow){
            it->second = it->second + increment;

            if(it->second == 0){
                currentFlows[sourcePort][PQIndex].erase(it);
                return;
            }

            if(std::next(it, 1) != listEnd)
                if(it->second < std::next(it, 1)->second)
                    std::swap(*it, *std::next(it, 1));
            if(it != listStart)
                if(it->second > std::prev(it, 1)->second)
                    std::swap(*it, *std::prev(it, 1));
            return;
        }
    }

    if(increment < 0)
        throw cRuntimeError("Packet departed from queue but its arrival was not accounted for");

    // Match not found insert in the tail.
    currentFlows[sourcePort][PQIndex].push_back(std::make_pair(incomingFlow, 1));
}


/*
 * Get n-th (specified by index) heaviest flow in the buffer
 */
FlowInformation* PFCPerFlowController::getHeaviestFlow(int inputQueue, int PQIndex, int index){
    return &std::next(currentFlows[inputQueue][PQIndex].begin(), index)->first;
}

int PFCPerFlowController::getDiversityPerInput(int inputQueue, int PQIndex){
    int totalNumberOfFlows=0;
    int flowsPerOutputPort[numPorts];
    //EV << "Input queue " << inputQueue << " PQ Index " << PQIndex <<"\n";
    //return 1;
    return currentFlows[inputQueue][PQIndex].size();

    // TODO: Useless loop for now
    for(auto it=currentFlows[inputQueue][PQIndex].begin(); it != currentFlows[inputQueue][PQIndex].end(); ++it){
        flowsPerOutputPort[it->first.SWOutPort]++;
    }
    return -1;
}


/*bool PFCPerFlowController::isFlowBlocked(FlowInformation *flow){
    if(blockedFlows.find(&flow) != blockedFlows.end())
        return true;
    return false;
}

FlowInformation* PFCPerFlowController::getOldestBlockedFlow(){
    std::vector<std::pair<FlowInformation, int> > sortedFlows(blockedFlows.begin(), blockedFlows.end());
    std::map<FlowInformation,int>::iterator it;
    FlowInformation *heaviestFlow;

    std::sort(sortedFlows.begin(), sortedFlows.end(),
              boost::bind(&std::pair<FlowInformation, simtime_t>::second, _1) <
              boost::bind(&std::pair<FlowInformation, simtime_t>::second, _2));

    return blockedFlows.end()[-1].first;
}

void PFCPerFlowController::generateAndSendPerFlowPauseFrame(int sourcePort, int PQIndex, EtherFrameCoS* frame, FlowInformation* flow, double timeToStop){
    relay->sendPausePerFlowFrame(sourcePort, PQIndex, frame, flow, timeToStop); //TODO
}*/
