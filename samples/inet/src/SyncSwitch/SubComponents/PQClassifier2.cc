//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "PQClassifier2.h"


Define_Module(PQClassifier2);

void PQClassifier2::initialize() {
    PQClassifier::initialize();
    numPriorities = (int)getParentModule()->par("numPriorities");
    int numGates = gateSize("out");
    for (int i = 0; i < numGates; ++i) {
        cGate *outGate = gate("out", i);
        cGate *connectedGate = outGate->getPathEndGate();
        if (!connectedGate)
            throw cRuntimeError("Classifier out gate %d is not connected", i);
        IQueueAccess *outModule = dynamic_cast<IQueueAccess *>(connectedGate->getOwnerModule());
        if (outModule) {
            outQueueSet.insert(outModule);
        }
    }
}

/*
 * Pop of priority
 */
int PQClassifier2::classify(cMessage* msg){
    EtherFrameCoS *message = check_and_cast<EtherFrameCoS *>(msg);
    int p = message->getPriorities().top();
    message->getPriorities().pop();

    return p % numPriorities;
}

/*
 * Classifier knows queue length if they are IQueueAccess modules
 */
int PQClassifier2::getLength() const
{
    if (outQueueSet.empty()) {
        throw cRuntimeError("Classifier not connected to any simple module implementing IQueueAccess");
    }
    int len = 0;
    for (const auto & elem : outQueueSet)
        len += (elem)->getLength();
    return len;
}

int PQClassifier2::getByteLength() const
{
    if (outQueueSet.empty()) {
        throw cRuntimeError("Classifier not connected to any simple module implementing IQueueAccess");
    }
    int len = 0;
    for (const auto & elem : outQueueSet)
        len += (elem)->getByteLength();
    return len;
}
