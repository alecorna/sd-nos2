//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TDMController.h"
#include "TimeSlottedScheduler.h"
Define_Module(TDMController);

void TDMController::initialize()
{
    //TODO make it versatile
    numHosts = (int) par("numHosts");
    numSources = (int) par("numSources");
    linkRate = (int) par("linkRate");
    slotDurationMSS = (int) par("slotDurationMSS"); // duration in MSS
    slotDurationSeconds = (simtime_t) 8 * (slotDurationMSS * (MSS_BYTES + ETH_HEADER + ETH_CRC + ETH_PREAMBLE + ETH_IPG)) / linkRate;
    allocatedSlots = new simtime_t[numSources];
    allocatedSlots[0]=0;

    for(int i=1; i<numSources; i++)
        allocatedSlots[i]=allocatedSlots[i-1] + slotDurationSeconds;

    frameDuration = allocatedSlots[numSources-1];

    nextAvailableSlot=simTime();

    slotTriggerMessage = new cMessage();
    slotTriggerMessage->setKind(1);

    if((bool) par("rateLimiting") == false)
        scheduleAt(simTime() + slotDurationSeconds, slotTriggerMessage);

    WATCH(allocatedSlots[0]);
    WATCH(allocatedSlots[1]);
    WATCH(allocatedSlots[2]);


    //WATCH(allocatedSlots[i]);
    WATCH(frameDuration);
    WATCH(slotDurationSeconds);
    WATCH_MAP(moduleIDs);
    WATCH_MAP(moduleNames);
    WATCH(numConcurrentTransmissions);
    //WATCH(servingOrder);
}

void TDMController::handleMessage(cMessage *msg){
    int numHosts = moduleIDs.size();
    if(moduleNames.empty())
        return;
    if(msg->getKind() == 1){ // first trigger
        for(auto it = moduleNames.rbegin(); it != moduleNames.rend(); ++it){
            servingOrder.push(it->first);
        }
        msg->setKind(0);
    }
    for(int i=0; i<servingOrder.size()+1; i++ ){
        int moduleId = (int) servingOrder.front();
        servingOrder.pop();
        servingOrder.push(moduleId);
        TimeSlottedScheduler* scheduler = check_and_cast<TimeSlottedScheduler *>((getModuleByPath( (char*) (moduleNames.find(moduleId)->second).c_str())));
        if(scheduler->getNumNonEmptyQueues() != 0){
            scheduler->startSending((simtime_t) (simTime() + slotDurationSeconds));
            //scheduler->startSendingMSS(slotDurationMSS);
            break;
        }
    }

    scheduleAt(simTime() + slotDurationSeconds, slotTriggerMessage);
}

int TDMController::registerListenerScheduler(int hostID, std::string fullName){
    //Enter_Method("registerListenerScheduler");
    int hostIndex = 0;

    if(moduleIDs.find(hostID) == moduleIDs.end()){
        hostIndex = findMaxHostIndex() + 1;
        moduleIDs.insert(std::make_pair(hostID, hostIndex));
        moduleNames.insert(std::make_pair(hostID, fullName));
    }
    else
        hostIndex = moduleIDs.find(hostID)->second;

    return hostIndex;
}

int TDMController::registerListener(int hostID, std::string fullName, bool moreToSend){
    int hostIndex = 0;

    if(moduleIDs.find(hostID) == moduleIDs.end()){
        hostIndex = findMaxHostIndex() + 1;
        moduleIDs.insert(std::make_pair(hostID, hostIndex));
        moduleNames.insert(std::make_pair(hostID, fullName));
    }
    else
        hostIndex = moduleIDs.find(hostID)->second;

    return hostIndex;
}

simtime_t TDMController::whenToSend(int hostID, std::string fullName, bool moreToSend){
    //TODO: remove
    Enter_Method("whenToSend(...)");

    int hostIndex = registerListenerScheduler(hostID, fullName);

    allocatedSlots[hostIndex] += frameDuration + slotDurationSeconds;

    return allocatedSlots[hostIndex];
}

simtime_t TDMController::adapativeWhenToSend(int hostID, std::string fullName, bool moreToSend){
    int hostIndex = registerListenerScheduler(hostID, fullName);

    nextAvailableSlot += slotDurationSeconds;

    while(true)
        if(nextAvailableSlot < simTime())
            nextAvailableSlot += slotDurationSeconds;
        else
            break;

    EV << "Assigning transmission time " << nextAvailableSlot << " to host" << hostID << "\n";

    return nextAvailableSlot;

}

simtime_t TDMController::whenToStop(int hostID){
    if(moduleIDs.find(hostID) == moduleIDs.end())
        throw cRuntimeError("Something bad with the map in TDMController");
    int hostIndex = moduleIDs.find(hostID)->first;

    return allocatedSlots[hostIndex] + slotDurationSeconds;
}

int TDMController::findMaxHostIndex(){
    int maxID = -1;

    for(auto it = moduleIDs.begin(); it != moduleIDs.end(); ++it){
        if(it->second > maxID)
            maxID = it->second;
    }
    return maxID;

}

int TDMController::getLinkRate(){
    return linkRate;
}

simtime_t TDMController::getFrameDuration(){
    return frameDuration;
}

simtime_t TDMController::getSlotDuration(){
    return slotDurationSeconds;
}

void TDMController::finalize(){
    delete allocatedSlots;
}

simtime_t TDMController::getAdaptiveTransmissionRate(int hostID, std::string fullName, int increment){
    int hostIndex = registerListenerScheduler(hostID, fullName);
    int multiplier = 1;

    numConcurrentTransmissions += increment;
    if(numConcurrentTransmissions != 0)
        multiplier = numConcurrentTransmissions;

    return (simtime_t) 8 * (MSS_BYTES + ETH_HEADER + ETH_OH_PHY_BYTES) / ( (int) par("linkRate")/multiplier);
}

simtime_t TDMController::getNonAdaptiveTransmissionRate(int hostID, std::string fullName, int moreToSend){
    return (simtime_t) 8 * (MSS_BYTES + ETH_HEADER + ETH_OH_PHY_BYTES + DUMMY_BYTE) / ( (int) par("linkRate")/((int) par("numSources")));
}

simtime_t TDMController::getAdaptiveTransmissionRatePerFlow(int hostID, std::string fullName, int increment){
    int hostIndex = registerListenerScheduler(hostID, fullName);
    int multiplier = 1;

    numConcurrentTransmissions += increment;
    if(numConcurrentTransmissions != 0)
        multiplier = numConcurrentTransmissions;

    return (simtime_t) 8 * getAverageFlowSize() * (MSS_BYTES + ETH_HEADER + ETH_OH_PHY_BYTES) / ( (int) par("linkRate")/multiplier);
}

int TDMController::getAverageFlowSize(){
    return ((int) par("maxSeqLen") + (int) par("minSeqLen"))/2;
}
