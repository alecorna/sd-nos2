//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SYNCSWITCH_SUBCOMPONENTS_THRESHOLDCONTROLLER_H_
#define SYNCSWITCH_SUBCOMPONENTS_THRESHOLDCONTROLLER_H_

#include <omnetpp.h>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include "inet/common/InitStages.h"
#include "inet/networklayer/ipv4/IPv4Datagram_m.h"
#include "FatTreeIPv4NetworkConfigurator.h"

using namespace inet;
using namespace std;
using namespace omnetpp;

class ThresholdController : public cSimpleModule {

protected:
        enum PriorityScheme { EQUAL_PERCENTILES, LOAD_BALANCE, OPTIMAL, NONE };
        enum FlowSizeDistribution {UNIFORM, EXPONENTIAL, PARETO, BIMODAL, PARETO_WEBSEARCH, PARETO_DATAMINING };

        FatTreeIPv4NetworkConfigurator* confg = nullptr;
        // parameters
        unsigned numPrioritiesPerPort;
        unsigned numPriorities;
        int numSpines;
        bool enableSpatialDiversity;
        const char* filename;
        FlowSizeDistribution distrib;
        PriorityScheme sa;
        PriorityScheme downroutingPriorityScheme;
        IPProtocolId protocol;

        // thresholds computed at boot-time
        unordered_map<int,vector<double>> thresholds;

        // statistics
        static simsignal_t thresholdsSignal;

    protected:
            virtual int numInitStages() const override { return NUM_INIT_STAGES; };
            virtual void initialize(int stage) override;
            virtual void computeThresholds();
            virtual vector<double> equalSplit(int numPercentiles);
            virtual double equalTrafficSplit();
            virtual int findIndexOfFirstGreaterThreshold(unsigned N, unsigned long numBytesSent);

    private:
            void readFromFile();
            void computeInternally();
            void runScript();
            vector<double> parseFromString(string,char);

    public:
        int getPriority();
        int getFlowPriority(unsigned long numBytesSent, unsigned N);
        int getPacketPriority(unsigned long numBytesSent, unsigned pkSize,unsigned N);
};

#endif /* SYNCSWITCH_SUBCOMPONENTS_THRESHOLDCONTROLLER_H_ */
