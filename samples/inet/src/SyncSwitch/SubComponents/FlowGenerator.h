//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SYNCSWITCH_SUBCOMPONENTS_FLOWGENERATOR_H_
#define SYNCSWITCH_SUBCOMPONENTS_FLOWGENERATOR_H_

#include "inet/common/INETDefs.h"
#include "inet/networklayer/ipv4/IPv4Datagram_m.h"
#include "inet/transportlayer/tcp_common/TCPSegment_m.h"
#include "inet/transportlayer/udp/UDPPacket_m.h"
#include "inet/linklayer/ethernet/Ethernet.h"
#include <string>
#include <algorithm>
#include <locale>
#include <stack>

using namespace std;
using namespace inet;

class FlowGenerator : public cSimpleModule {

    public:
        virtual int getRandomDestination(char**);
        virtual long getNewFlowLength();
        virtual simtime_t getSleepTime();

    protected:

        static simsignal_t interArrivalTime;

        // traffic generation properties
        double accLinkRate;
        // number of tcp clients in the host
        // (used to re-scale offered load of this app, in order to have the desired load on access link)
        int numTors;
        double lambda;
        IPProtocolId protocol;
        long requestedFlows;

        enum class FlowSizeDistribution {
            UNIFORM, EXPONENTIAL, PARETO, PARETO_WS, PARETO_DM, BIMODAL, NONE
        };

        FlowSizeDistribution flowLengthDistribution;

        virtual void initialize(int stage) override;
        virtual int intuniform_except(int a, int b, int excluded_from, int excluded_to);
        virtual void parseDistributionParameters();
        virtual double getAverageFlowSize();
        virtual long getGivenFlowLength();
        virtual simtime_t getGivenInterFlowTime();
        //virtual int getNumberTcpClient();
        virtual double truncated_pareto(double alpha, double L, double H);
        virtual void computeArrivalRate();
};

#endif /* SYNCSWITCH_SUBCOMPONENTS_FLOWGENERATOR_H_ */
