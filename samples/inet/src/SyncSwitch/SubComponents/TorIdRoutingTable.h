/*
 * TorIdRoutingTable.h
 *
 *  Created on: Aug 8, 2019
 *      Author: alessandrocornacchia
 */

#ifndef SYNCSWITCH_SUBCOMPONENTS_TORIDROUTINGTABLE_H_
#define SYNCSWITCH_SUBCOMPONENTS_TORIDROUTINGTABLE_H_

#include <omnetpp.h>
#include <unordered_map>
#include <vector>

#define RANDOM_ROUTE_IDX -1

using namespace std;
using namespace omnetpp;

class TorIdRoutingTable {

    private:
        vector<unordered_map<int,vector<int>>> routes;   // ToR ID { priority --> [ interfaces ] }
        vector<bool> routeExist;

    public:
        void addRoute(int torId, int outInterface, int priority = BEST_EFFORT); // add route for ToR
        int getNumRoutesOfPriority(int torId, int priority);  // number of possible next hop to reach a given ToR
        int getRouteOfPriority(int torId, int priority, int routeIdx = RANDOM_ROUTE_IDX);
        void setSize(int numEntries);

    public:
        static const int BEST_EFFORT = -1;
};

#endif /* SYNCSWITCH_SUBCOMPONENTS_TORIDROUTINGTABLE_H_ */
