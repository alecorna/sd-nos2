//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.

// Author : Alessandro Cornacchia

#include "SourceBasedRelay.h"
#include "inet/common/LayeredProtocolBase.h"
#include "inet/common/ModuleAccess.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "inet/linklayer/configurator/Ieee8021dInterfaceData.h"
#include "inet/linklayer/ieee8021d/relay/Ieee8021dRelay.h"
#include "inet/networklayer/common/InterfaceEntry.h"

Define_Module(SourceBasedRelay);

/* This class assumes the layer 2 forwarding being computed a priori by the source ( with the help
 * a central controller and the forwarding information being inserted in the EtherFrame message
 * as a stack of interface labels. Thus, the relay unit is only left with popping an interface identifier
 * and dispatch the packet accordingly.
 */
void SourceBasedRelay::handleAndDispatchFrame(EtherFrame *frame)
{
    int arrivalGate = frame->getArrivalGate()->getIndex();
    Ieee8021dInterfaceData *arrivalPortData = getPortInterfaceData(arrivalGate);
    learn(frame);
    // BPDU Handling
    if (isStpAware && (frame->getDest() == MACAddress::STP_MULTICAST_ADDRESS || frame->getDest() == bridgeAddress) && arrivalPortData->getRole() != Ieee8021dInterfaceData::DISABLED) {
        EV_DETAIL << "Deliver BPDU to the STP/RSTP module" << endl;
        deliverBPDU(frame);    // deliver to the STP/RSTP module
    }
    else if (isStpAware && !arrivalPortData->isForwarding()) {
        EV_INFO << "The arrival port is not forwarding! Discarding it!" << endl;
        numDroppedFrames++;
        delete frame;
    }
    else if (frame->getDest().isBroadcast()) {    // broadcast address
        broadcast(frame);
    }
    else {
        int outGate = frame->getOutPortStack().top();  // get the last label
        frame->getOutPortStack().pop();                // remove it
        // Not known -> broadcast
        if (outGate == -1) {
            EV_DETAIL << "Destination address = " << frame->getDest() << " unknown, broadcasting frame " << frame << endl;
            broadcast(frame);
        }
        else {
            if (outGate != arrivalGate) {
                Ieee8021dInterfaceData *outPortData = getPortInterfaceData(outGate);

                if (!isStpAware || outPortData->isForwarding())
                    dispatch(frame, outGate);
                else {
                    EV_INFO << "Output port " << outGate << " is not forwarding. Discarding!" << endl;
                    numDroppedFrames++;
                    delete frame;
                }
            }
            else {
                EV_DETAIL << "Output port is same as input port, " << frame->getFullName() << " destination = " << frame->getDest() << ", discarding frame " << frame << endl;
                numDroppedFrames++;
                delete frame;
            }
        }
    }
}


