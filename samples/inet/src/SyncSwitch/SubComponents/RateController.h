//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef __INET_RateController_H_
#define __INET_RateController_H_

#include <omnetpp.h>
#include <queue>
using namespace omnetpp;

#define MSS_BYTES 1500
#define ETH_HEADER 18
#define ETH_IPG 12
#define ETH_CRC 4
#define ETH_PREAMBLE 4
#define ETH_OH_PHY_BYTES 20
#define DUMMY_BYTE 1
#define IP_OH 20
#define MIN(a,b) (a>=b ? b : a)
#define MAX(a,b) (a>b ? a : b)

class RequestInformation
{
public:
    ~RequestInformation(){};
    RequestInformation(int sID, int dID, simtime_t T, int B) {
        srcID = sID;
        dstID = dID;
        requestTime = T;
        residualBytes = B;
    };
    virtual void setSrcID(int ID) {srcID = ID;};
    virtual void setDstID(int ID) {dstID = ID;};
    virtual void setRequestTime(simtime_t T) { requestTime = T;};
    virtual void setResidualBytes(int B) {residualBytes = B;};
    virtual void setLastServedTime(int time) {lastServedTime = time;}
    virtual void setAssignedTor(int t) {assignedTor = t;}
    virtual void setAssignedRate(double t) {assignedRate = t;}
    virtual void setAssignedSpine(int t) {assignedSpine = t;}


    virtual int getLastServedTime() {return lastServedTime;}
    virtual int getSrcID() { return srcID;};
    virtual int getDstID() { return dstID;};
    virtual simtime_t getRequestTime() { return requestTime;};
    virtual int getResidualBytes() {return residualBytes;};
    virtual int getAssignedTor() {return assignedTor;};
    virtual int getAssignedSpine() {return assignedSpine;};
    virtual double getAssignedRate() {return assignedRate;};

protected:
    int srcID;
    int dstID;
    simtime_t requestTime = -1;
    int lastServedTime = -1;
    int residualBytes;
    int assignedTor = -1;
    int assignedSpine = -1;
    double assignedRate = -1;
};

class RateController : public cSimpleModule
{

protected:
    int numHosts = -1;
    int numTors = -1;
    int numSpines = -1;
    int linkRate = 0;
    int colorsPerSpine = -1;
    RequestInformation ***requests;
    std::map<int, int> moduleIDs; // <IP, Unique ID starting from 0>
    std::map<int, int> moduleIDsReversed; // <Unique ID starting from 0, IP>
    std::map<int, std::string> modulePaths; // <IP, Full path to VOQ>
    std::map<int, int> moduleToRs; // <Unique host ID, ToR ID>
    cMessage *slotTriggerMessage;
    int numConcurrentTransmissions = 0;
    int numberOfActiveRequests = 0;
    simtime_t slotDurationSeconds;

    enum SupportedSchedulers {RL=0, TDM};
    enum MSM_TYPE {RANDOM = 0, OF, SRJF, MMF};
    int usedScheduler = TDM;
    int usedMSM = RANDOM;
    double ***loadBalancingMatrix;
    double **flowRateAssignments;
    int **flowAssignment;
    int **servingFlowIDs;
    double **requestMatrix;
    cOutVector **assignedRatesVector;

public:
    ~RateController();
    virtual int getLinkRate(){return linkRate;};
    virtual simtime_t getSlotDuration(){return slotDurationSeconds;};
    virtual void registerListener(std::string VOQPath, int VOQIPAddress, int moduleIndex, int torIndex);
    virtual void updateRequest(int srcIP, int dstIP, int totalBytes, simtime_t enqueueTime, int usedScheduler);
    virtual int getIDFromIP(int IP);

    virtual int getAverageFlowSize();
    virtual simtime_t getTXEndTimeOfFrame(int frameLengthBytes);
    virtual simtime_t getTXEndTimeOfAppPkt(int appPktLengthBytes);
    virtual double getAccumulatedBytesWithRate(double rate, simtime_t time);


protected:
    virtual void initialize() override;
    virtual void finish() override;
    virtual void handleMessage(cMessage *msg) override;

    virtual void updateRL(int srcIP, int dstIP);
    virtual void computeMSMandSchedule();
    virtual int** scheduleAndServeRandommsm();
    virtual int** scheduleAndServeOFmsm();
    virtual int** scheduleAndServeSRJFmsm();
    virtual int** scheduleAndServeMMFmsm();
    virtual void printCurrentRequestMatrixResidualBytes();
    virtual void printCurrentRequestMatrixRequestTime();
    virtual void printAndDestoryCurrentServingMatrix(int **servingMatrix);
    virtual void printAndDestroyNonBlockingPaths(int **nonBlockingPaths);
    virtual void printCurrentServingMatrix(int **servingMatrix);
    virtual void printCurrentRequestMatrixRequestServed();
    virtual int findMaxHostIndex();
    virtual void resetRequests();
    virtual void resetRequest(int srcID, int dstID);
    virtual void updateNumberOfActiveRequests();
    virtual int** selectNonBlockingPath(int** currentServingMatrix);
    virtual int getLessLoadedSpine(int sourceToR, int destinationToR, double*** spineLoad, double loadToPlace, double* excess);

    virtual bool rearrangePaull(int sourceTor, int destinationTor, bool*** colorMatrix);

    virtual int getCommonFreeColors(bool* freeColors1, bool* freeColors2, int exclusionColor);
    virtual int getConflictOnRow(bool*** colorMatrix, int row, int color, int exclusion);
    virtual int getConflictOnColumn(bool*** colorMatrix, int column, int color, int exclusion);
    virtual void printCurrentColorMatrix(bool*** colorMatrix);

    virtual void rebuildRM();
    virtual void rebuildRMSRJF();
    virtual void normalizeRow(double** M);
    virtual void normalizeCol(double ** M);
    virtual void normalizeRow(double** M, int index);
    virtual void normalizeCol(double ** M, int index);
    virtual double minOnRow(double** M, int index);
    virtual double minOnCol(double** M, int index);
    virtual void underAllocateRow(double** M);

};

#endif
