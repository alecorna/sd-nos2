//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "LQFScheduler.h"
#include "DropTailQueueII.h"
#include "PQScheduler.h"
Define_Module(LQFScheduler);

bool LQFScheduler::schedulePacket()
{

    int nonEmptyQueues[gateSize("in")+1];
    int totalNonEmptyQueues=0;
    int maxLength = 0;
    int length = 0;
    int queueToServe=-1;
    int pq=0;
    for(int i=0; i<inputQueues.size();i++) nonEmptyQueues[i]=0;

    // First try to serve the pause queue
    if(dynamic_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())!= nullptr){
        if( !check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->isEmpty() ){
            check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->requestPacket();
            EV << "Requesting packet from Pause Queue " << queueToServe << "\n";
            return true;
        }
    }

    for(pq=0; pq<(int) par("numberPriorities"); pq++){
        for(int i=1; i<gateSize("in"); i++){ // start from i=1 because i=0 is the pause queue
            ASSERT(dynamic_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule()) != nullptr);

            if(!check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->isEmpty(pq)){
                length = check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->getQueueLength(pq);
                if(length > maxLength){
                    maxLength = length;
                    queueToServe = i;
                }
            }
        }
        if(queueToServe > 0) break;
    }
    if(queueToServe < 0){
        EV << "All queues are empty\n";
        return false;
    }

    EV << "Queue to serve: "<< queueToServe <<"\n";

    check_and_cast<PQScheduler *>(gate("in",queueToServe)->getPathStartGate()->getOwnerModule())->requestPacket(pq);
    return true;
}
