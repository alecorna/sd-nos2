//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_ECMPRELAY_H_
#define __INET_ECMPRELAY_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "PQRelay.h"
#include "RdmaApp/RDMAPacket_m.h"
using namespace inet;

/**
 * TODO - Generated class
 */
class ECMPRelay : public PQRelay
{
protected:
    //virtual void initialize();
    //virtual void handleMessage(cMessage *msg);
    virtual void handleAndDispatchFrame(EtherFrame *frame) override;
    //virtual bool isDestinationLocal();
    virtual void initialize(int stage) override;
    virtual int updateAddressTable(int inputPort, MACAddress &srcMAC, int numberOfHops);
    virtual void printTable();
    virtual int getECMPPortForAddress(EtherFrameCoS *frame);


protected:
    std::map<MACAddress, int*> kAddressTable;

};

#endif
