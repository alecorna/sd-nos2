//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_ETHERMACPAUSE_H_
#define __INET_ETHERMACPAUSE_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/EtherMAC.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
using namespace inet;

/**
 * TODO - Generated class
 */
class EtherMACPause : public EtherMAC
{
#define MSS_BYTES 1500
#define ETH_OH_BYTES 66

    protected:
        virtual void processFrameFromUpperLayer(EtherFrame *msg) override;
        virtual bool dropFrameNotForUs(EtherFrame *frame) override;
        virtual void frameReceptionComplete() override;
        virtual void getNextFrameFromQueue()  override;
        virtual void handleEndTxPeriod() override;
        virtual void processReceivedDataFrame(EtherFrame *frame) override;




};

#endif
