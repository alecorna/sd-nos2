//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "AppQueue.h"
#include "RateController.h"
#include "inet/networklayer/common/L3AddressResolver.h"
//#include "inet/applications/base/ApplicationPacket_m.h"
#include "inet/transportlayer/contract/ITransportPacket.h"
#define DEC_MULT 1

Define_Module(AppQueue);
simsignal_t AppQueue::wastedTimeSignal = registerSignal("wastedTime");


int AppQueue::numInitStages() const{
    return 10;
}

void AppQueue::initialize(int stage)
{
    if(stage != 9) return;

    if(getModuleByPath("^.rdmaApp[0]") == nullptr) {
        numInputQueues=0;
    } else {
        numInputQueues = getModuleByPath("^.rdmaApp[0]")->getVectorSize();
    }
    if(numInputQueues == 0) numInputQueues = 1;

    appQueues = new cQueue[numInputQueues];
    queueOccupancyVector = new cOutVector[numInputQueues];
    queueOccupancyStatistics = new cStdDev[numInputQueues];
    for(int i = 0; i < numInputQueues; i++){
        char name[100];
        sprintf(name, "I%d", i);
        appQueues[i].setName(name);
        sprintf(name, "QOCC-I%d", i);
        queueOccupancyVector[i].setName(name);
        sprintf(name, "QOCCstats-I%d", i);
        queueOccupancyStatistics[i].setName(name);
    }
    totalQueueOccupancyVector.setName("TotalQOcc");
    emissionRate.setName("FER");
    normalizedEmissionRate.setName("nFER");
    flowEmissionRateVector.setName("FERVector");
    usedScheduler = RANDOM;

    if(!strncmp(par("inputSchedulerType").stringValue(), "TDMScheduler",10))
        initializeTDMScheduler();
    else if(!strncmp(par("inputSchedulerType").stringValue(), "RLScheduler",10))
        initializeRLScheduler();

    assignedRates = new double[numInputQueues];
    assignedPaths = new int[numInputQueues];
    for(int i=0; i<numInputQueues; i++){
        assignedRates[i] = 0;
        assignedPaths[i] = -1;
    }

    WATCH(numInputQueues);
    WATCH(usedScheduler);
    WATCH(thisModuleIPAddress);
    WATCH(txEndTime);
}

void AppQueue::finish(){
    delete[] appQueues;
    delete[] queueOccupancyVector;
    delete[] assignedPaths;
    delete[] assignedRates;

    if(usedScheduler == RL){
        delete[] tokenArrivalRates;
        for(int i = 0; i < numInputQueues; i++){
            queueOccupancyStatistics[i].record();
            cancelAndDelete(tokenMessages[i]);
        }
        delete[] tokenMessages;
    }

    emissionRate.record();
    normalizedEmissionRate.record();
}

void AppQueue::initializeTDMScheduler(){

    usedScheduler = TDM;

    registerSchedulerModule();
    registerSchedulerListener();
    txEndTime = SIMTIME_ZERO;

}

void AppQueue::initializeRLScheduler(){
    usedScheduler = RL;
    registerSchedulerModule();
    registerSchedulerListener();

    tokenBucketSize = (int) par("tokenBucketSize");

    tokenArrivalRates = new simtime_t[numInputQueues];
    tokenMessages = new cMessage*[numInputQueues];
    totalAccumulatedBytes = new double[numInputQueues];

    for(int i = 0; i < numInputQueues; i++){
        tokenMessages[i] = new cMessage("TOKEN MESSAGE");
        tokenMessages[i]->setKind(TOKEN_EVENT);
        tokenMessageID.insert(std::make_pair(tokenMessages[i], i));

        totalAccumulatedBytes[i] = 0;
    }

}

void AppQueue::handleMessage(cMessage *msg)
{
    if(msg->isSelfMessage())
        manageSelfMessages(msg);
    else
        manageFrameArrivals(msg);
}

void AppQueue::manageSelfMessages(cMessage *msg){
    if(msg->getKind() == TOKEN_EVENT){
        if(tokenMessageID.find(msg) == tokenMessageID.end()) throw cRuntimeError("Token not registered");
        serveQueueRL(tokenMessageID.find(msg)->second);
        totalAccumulatedBytes[tokenMessageID.find(msg)->second] = 0;
        //scheduleAt(simTime() + tokenArrivalRates[tokenMessageID.find(msg)->second], msg);
    }
}

bool AppQueue::isFlowOldOrNew(int index){
    if(appQueues[index].isEmpty()) return true;

    cPacket* frontPkt = ((cPacket*) appQueues[index].front())->getEncapsulatedPacket();

    if(dynamic_cast<RDMAPacket*>(frontPkt) == nullptr) throw cRuntimeError("Not an RDMA packet.");

    if(check_and_cast<RDMAPacket*>(frontPkt)->getSequenceNumber() == 0) return true;
    return false;
}

void AppQueue::serveQueueRL(int iqIndex){
    int dstIPAdd = getDstAddressOfHOLPacket(iqIndex);
    if(assignedPaths[iqIndex] >= 0)
        setSelectedPathForHOLPacket(iqIndex, assignedPaths[iqIndex]);
    sendOut(iqIndex);
    notifyControllerAboutNextPacket(iqIndex, dstIPAdd);
}

void AppQueue::manageFrameArrivals(cMessage *msg){
    int iqIndex = 0;


    // handle frames FOR TL
    if(msg->getArrivalGate() == gate("ipIn")){
        send(msg, "transportOut");
        return;
    }

    if(usedScheduler == RANDOM || dynamic_cast<ITransportPacket*>(msg) == nullptr){
        EV << "Bypassing centralized scheduler\n";
        EV << "Scheduler is " << usedScheduler <<"\n";
        //EV << "Packet is " msg->getClassName()->str() << "\n";
        if(msg->getArrivalGate() == gate("transportIn")){
            send(msg, "ipOut");
            return;
        }
    }

    iqIndex = check_and_cast<RateController*>(fabricScheduler)->getIDFromIP(getDstAddressOfPacket(msg));

    appQueues[iqIndex].insert(msg);
    queueOccupancyVector[iqIndex].record(appQueues[iqIndex].getLength());
    queueOccupancyStatistics[iqIndex].collect(appQueues[iqIndex].getLength());
    totalQueueOccupancyVector.record(++totPackets);

    if(usedScheduler == TDM || usedScheduler == RL){
        cPacket *rxPkt = check_and_cast<cPacket*>(msg);
        if(dynamic_cast<RDMAPacket* >(rxPkt->getEncapsulatedPacket()) != nullptr){
            if(check_and_cast<RDMAPacket* >(rxPkt->getEncapsulatedPacket())->getIsFinal() == true){
                EV << "Last packet of sequence. SeqNo=" << check_and_cast<RDMAPacket* >(rxPkt->getEncapsulatedPacket())->getSequenceNumber() << "Notifying RateLimiter.\n";
                check_and_cast<RateController*>(fabricScheduler)->updateRequest(thisModuleIPAddress, getDstAddressOfPacket(msg), getTotalQueueByteSize(iqIndex), getHOLPktEnqueueTime(iqIndex), usedScheduler);
            }
        }
    }
}

void AppQueue::registerSchedulerListener(){
    const char *hostName = getParentModule()->getFullName();
    thisModuleIPAddress = L3AddressResolver().resolve(hostName, L3AddressResolver::ADDR_IPv4).toIPv4().getInt();
    thisModuleToRIndex = getParentModule()->gate("ethg$o",0)->getNextGate()->getOwnerModule()->getIndex();
    thisModuleIndex = getParentModule()->getIndex();

    check_and_cast<RateController*>(fabricScheduler)->registerListener(this->getFullPath(), thisModuleIPAddress, thisModuleIndex, thisModuleToRIndex);
}

void AppQueue::sendOut(int iqIndex){
    if(appQueues[iqIndex].getLength() == 0) throw cRuntimeError("Trying to pop from an empty queue");

    cPacket* msg = check_and_cast<cPacket* >(appQueues[iqIndex].front());
    if(dynamic_cast<RDMAPacket*>(msg->getEncapsulatedPacket()) != nullptr){
        RDMAPacket *pkt = check_and_cast<RDMAPacket*>(msg->getEncapsulatedPacket());
        pkt->setEmissionTime(simTime());
        if(pkt->getIsFinal()){
            emissionRate.collect(simTime() - pkt->getFlowStartTime());
            flowEmissionRateVector.record(simTime() - pkt->getFlowStartTime());
            normalizedEmissionRate.collect((simTime() - pkt->getFlowStartTime())/(pkt->getSequenceNumber()+1));

        }

    }
    send(check_and_cast<cMessage* >(appQueues[iqIndex].pop()), "ipOut");
    queueOccupancyVector[iqIndex].record(appQueues[iqIndex].getLength());
    queueOccupancyStatistics[iqIndex].collect(appQueues[iqIndex].getLength());

    totalQueueOccupancyVector.record(--totPackets);
}

void AppQueue::notifyControllerAboutNextPacket(int iqIndex, int currentPktdstIPAdd){

    if((usedScheduler == TDM) && !appQueues[iqIndex].isEmpty()){
        EV << "Queue is not empty, notifying\n";
        check_and_cast<RateController*>(fabricScheduler)->updateRequest(thisModuleIPAddress, getDstAddressOfHOLPacket(iqIndex), getTotalQueueByteSize(iqIndex), getHOLPktEnqueueTime(iqIndex), usedScheduler);
    }
    else if((usedScheduler == TDM || usedScheduler == RL) && appQueues[iqIndex].isEmpty()){
        EV << "Queue is empty, notifying\n";
        check_and_cast<RateController*>(fabricScheduler)->updateRequest(thisModuleIPAddress, currentPktdstIPAdd, 0, -1, usedScheduler);
    }
    else if(usedScheduler == RL && !appQueues[iqIndex].isEmpty()){
        EV << "Emitting a packet destined to module " << iqIndex << ". With rate " << assignedRates[iqIndex] << " and IPG " << check_and_cast<RateController*>(fabricScheduler)->getTXEndTimeOfAppPkt(getByteSizeOfHOLPacket(iqIndex) + 1 )/assignedRates[iqIndex] <<"\n";
        EV << "Allocated rates: \n";
        for(int i = 0; i< numInputQueues; i++){
            EV << assignedRates[i] <<" ";
        }
        EV << "\n";
        scheduleAt(simTime() + (simtime_t) check_and_cast<RateController*>(fabricScheduler)->getTXEndTimeOfAppPkt(getByteSizeOfHOLPacket(iqIndex) +1 )/assignedRates[iqIndex], tokenMessages[iqIndex]);
        tokenMessages[iqIndex]->setTimestamp(simTime());
    }

}

void AppQueue::registerSchedulerModule(){
    if(dynamic_cast<RateController*>(getModuleByPath(getAncestorPar("rateControllerPath"))) == nullptr)
        throw cRuntimeError("Rate controller path invalid or not found");

    fabricScheduler = check_and_cast<RateController*>(getModuleByPath(getAncestorPar("rateControllerPath")));
}

int AppQueue::getByteSizeOfHOLPacket(int iqIndex){
    if(appQueues[iqIndex].isEmpty()) return -1;
    cPacket *frontPkt = (cPacket*) appQueues[iqIndex].front();
    return frontPkt->getByteLength();
}

int AppQueue::getDstAddressOfHOLPacket(int iqIndex){
    if(appQueues[iqIndex].isEmpty()) return -1;
    cPacket *frontPkt = (cPacket*) appQueues[iqIndex].front();
    IPv4ControlInfo *ipControlInfo = check_and_cast<IPv4ControlInfo*>(frontPkt->getControlInfo());
    if(ipControlInfo == nullptr) throw cRuntimeError("Front packet does not have a valid IP address");
    return ipControlInfo->getDestAddr().getInt();

}

int AppQueue::getDstAddressOfPacket(cMessage *msg){
    IPv4ControlInfo *ipControlInfo = check_and_cast<IPv4ControlInfo*>(msg->getControlInfo());
    if(ipControlInfo == nullptr) throw cRuntimeError("Front packet does not have a valid IP address");
    return ipControlInfo->getDestAddr().getInt();
}

void AppQueue::setTokenArrivalRate(simtime_t arrivalRate, int iqIndex){
    Enter_Method("setTokenArrivalRate(...)");

    if(arrivalRate < 0){ // Queue is empty. No need for rate-limiting
        cancelEvent(tokenMessages[iqIndex]);
        return;
    }

    tokenArrivalRates[iqIndex] = arrivalRate;
    if(!tokenMessages[iqIndex]->isScheduled()){
        tokenMessages[iqIndex]->setTimestamp(simTime());
        scheduleAt(simTime() + arrivalRate, tokenMessages[iqIndex]);
    }
}

void AppQueue::setTransmissionEnd(simtime_t txEndTimeL, int iqIndex, int selectedPath){
    char buff[100]; sprintf(buff, "setTransmissionEnd(%f, %d)", txEndTimeL.dbl(), iqIndex);
    Enter_Method(buff);

    txEndTime = txEndTimeL;
    serveQueueTDM(iqIndex, selectedPath);
}

void AppQueue::serveQueueTDM(int iqIndex, int selectedPath){
    simtime_t timeConsumed = simTime();
    simtime_t txDuration;

    if(simTime() > txEndTime) return;

    int dstIPAdd = getDstAddressOfHOLPacket(iqIndex);

    while(true){

        if(appQueues[iqIndex].isEmpty()){
            EV << "Queue is empty breaking\n";
            break;
        }

        txDuration = check_and_cast<RateController*>(fabricScheduler)->getTXEndTimeOfAppPkt(getByteSizeOfHOLPacket(iqIndex));

        if( (timeConsumed + txDuration) < txEndTime)
            timeConsumed += txDuration;
        else break;

        EV << "Selected path " << selectedPath << "\n";

        if(selectedPath >= 0)
            setSelectedPathForHOLPacket(iqIndex, selectedPath);

        sendOut(iqIndex);
    }

    notifyControllerAboutNextPacket(iqIndex, dstIPAdd);
    emit(wastedTimeSignal, (txEndTime-timeConsumed));
}

void AppQueue::setSelectedPathForHOLPacket(int iqIndex, int selectedPath){
    if(appQueues[iqIndex].front() == nullptr) throw cRuntimeError("Trying to access empty queue in getHOLPktEnqueueTime");
    cPacket* frontPkt = ((cPacket*) appQueues[iqIndex].front())->getEncapsulatedPacket();

    if(dynamic_cast<RDMAPacket*>(frontPkt) == nullptr) throw cRuntimeError("Not an RDMA packet.");

    check_and_cast<RDMAPacket*>(frontPkt)->setExplixitRouting(true);
    check_and_cast<RDMAPacket*>(frontPkt)->setExplicitSpineIndex(selectedPath);
}

int AppQueue::getTotalQueueByteSize(int iqIndex){
    int totalBytes = 0;

    if(appQueues[iqIndex].isEmpty()) return totalBytes;

    if(appQueues[iqIndex].front() == nullptr) throw cRuntimeError("Trying to access empty queue in getTotalQueueByteSize");

    if((bool) par("perFlowletSRJF") == false)
        for(int i=0; i<appQueues[iqIndex].getLength(); i++)
            totalBytes += check_and_cast<cPacket*>(appQueues[iqIndex].get(i))->getByteLength();
    else{
        for(int i=0; i<appQueues[iqIndex].getLength(); i++){
            cPacket* frontPkt = check_and_cast<cPacket*>(appQueues[iqIndex].get(i));
            if(check_and_cast<RDMAPacket*>(frontPkt->getEncapsulatedPacket())->getSequenceNumber() == 0 && i!=0) return totalBytes;
            totalBytes += check_and_cast<cPacket*>(appQueues[iqIndex].get(i))->getByteLength();
        }
    }
    return totalBytes;
}

simtime_t AppQueue::getHOLPktEnqueueTime(int iqIndex){
    if(appQueues[iqIndex].front() == nullptr) throw cRuntimeError("Trying to access empty queue in getHOLPktEnqueueTime");

    return check_and_cast<cPacket*>(appQueues[iqIndex].front())->getCreationTime();
}

void AppQueue::renormalizeRates(){
    return;
}

void AppQueue::updateAllRates(double* rates){
    Enter_Method_Silent();
    double oldRate;
    //double totSum = 0;
    for(int i=0;i<numInputQueues; i++){
        oldRate = assignedRates[i];
        assignedRates[i] = rates[i];

        if(assignedRates[i] == 0) continue;

        if(!tokenMessages[i]->isScheduled()){
            scheduleAt(simTime() + (simtime_t) check_and_cast<RateController*>(fabricScheduler)->getTXEndTimeOfAppPkt(getByteSizeOfHOLPacket(i) +1 )/assignedRates[i], tokenMessages[i]);
            tokenMessages[i]->setTimestamp(simTime());
        }
        else {
            simtime_t elapsedTime = simTime() - tokenMessages[i]->getSendingTime();
            double lastAccumulatedBytes = check_and_cast<RateController*>(fabricScheduler)->getAccumulatedBytesWithRate(oldRate, elapsedTime);
            totalAccumulatedBytes[i] += lastAccumulatedBytes;
            simtime_t newResidualTime = check_and_cast<RateController*>(fabricScheduler)->getTXEndTimeOfAppPkt(getByteSizeOfHOLPacket(i) + 1 - floor(totalAccumulatedBytes[i]))/assignedRates[i];
            EV << "pktSize=" << getByteSizeOfHOLPacket(i) << " schedulingTime="<< tokenMessages[i]->getSendingTime() << " elapsedTime=" << elapsedTime << " oldRate=" << oldRate << " totalAccumulatedBytes=" << totalAccumulatedBytes[i] << "\n";
            EV << "newRate=" << assignedRates[i] << " newSchedulingTime="<< simTime() + newResidualTime << "\n";

            cancelEvent(tokenMessages[i]);
            scheduleAt(simTime() + newResidualTime, tokenMessages[i]);

        }
    }
}

void AppQueue::setPathRL(int dstIP, int assignedSpine){
    assignedPaths[dstIP] = assignedSpine;
}
