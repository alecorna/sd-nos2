//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_PQSCHEDULER_H_
#define __INET_PQSCHEDULER_H_

#include <omnetpp.h>
#include <vector>
#include "inet/common/queue/PriorityScheduler.h"
#include "PFCController.h"
#include "PFCPerFlowController.h"

using namespace inet;

/**
 * TODO - Generated class
 */
class PQScheduler : public PriorityScheduler
{
    protected:
        PFCController *pfcController;

    protected:
        virtual void initialize() override;
        virtual bool schedulePacket() override;
        virtual void sendOut(cMessage *msg) override;
    public:
        virtual bool isEmpty(int PQIndex);
        virtual bool schedulePacket(int PQIndex);
        virtual void requestPacket(int PQIndex);
        virtual int getQueueLength(int PQIndex);
        virtual int getNextPacketLengthBytes(int PQIndex);

};

#endif
