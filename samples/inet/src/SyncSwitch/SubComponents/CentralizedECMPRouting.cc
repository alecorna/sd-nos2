//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "CentralizedECMPRouting.h"

Define_Module(CentralizedECMPRouting);

void CentralizedECMPRouting::initialize()
{
    // Initialize topology
    topo = new cTopology("Topology");
    std::vector<std::string> nedTypes;
    nedTypes.push_back("SyncSwitch5");
    nedTypes.push_back("StandardHostCos");
    topo->extractByProperty("node");

    int numNodes = topo->getNumNodes();

    EV << numNodes << "nodes. Module names:\n";
    for(int i=0; i<numNodes; i++){
        EV << i << " " << topo->getNode(i)->getModule()->getFullName() << "\n";
    }

    // Find all hosts and extract their MACs
    for(int i=0; i<numNodes; i++){
        if(!topo->getNode(i)->getModule()->getProperties()->getAsBool("switch")){
            cModule *host = topo->getNode(i)->getModule();
            std::string macAddress = host->getSubmodule("eth", 0)->getSubmodule("mac", 0)->par("address");
            MACAddress *mac = new MACAddress(macAddress.c_str());
            hosts.insert(std::make_pair(*mac, i));
        }
    }

    // Find all ToRs
    for(auto it=hosts.begin(); it != hosts.end(); ++it){
        for(int j=0;j<topo->getNode(it->second)->getNumOutLinks(); j++){

            int torSwitchID = getNodeIndex(topo->getNode(it->second)->getLinkOut(j)->getRemoteNode()->getModule());
            if(switchClasses.find(torSwitchID) == switchClasses.end()){
                switchClasses.insert(std::make_pair(torSwitchID, TOR));
            }
        }
    }

    //Find all leaves

    std::map<int, int> switchClassesTemp = switchClasses;
    for(int i=0;i<topo->getNumNodes();i++){

        if(!isTor(i)) continue;

        EV << "Considering node " << i << " " <<  topo->getNode(i)->getModule()->getFullName() << "\n";

        cTopology::Node *tor = topo->getNode(i);
        for(int j=0; j<tor->getNumOutLinks(); j++){
            cTopology::Node *endNode = tor->getLinkOut(j)->getRemoteNode();
            EV << "Considering end-node " << getNodeIndex(endNode->getModule()) << " " << endNode->getModule()->getFullName() << "\n";
            if(isHost(getNodeIndex(endNode->getModule())) || isTor(getNodeIndex(endNode->getModule()))) continue;
            EV << "Leaf found, adding " << getNodeIndex(endNode->getModule()) << "\n";
            switchClasses.insert(std::make_pair(getNodeIndex(endNode->getModule()), LEAF));

        }
    }

    //Find all spines
    for(auto it=switchClasses.begin(); it != switchClasses.end(); it++){
        if(it->second == LEAF){
            cTopology::Node *leaf = topo->getNode(it->first);
            for(int j=0; j<leaf->getNumOutLinks(); j++){
                cTopology::Node *endNode = leaf->getLinkOut(j)->getRemoteNode();

                if(isHost(getNodeIndex(endNode->getModule())) || isTor(getNodeIndex(endNode->getModule()))) continue;

                if(isTor(endNode->getModuleId())) continue;

                switchClasses.insert(std::make_pair(getNodeIndex(endNode->getModule()), SPINE));
            }
        }
    }

    /*for(auto it=switchClasses.begin(); it= switchClasses.begin(); ++it){
        if(it->second != TOR) continue;
        cTopology::Node *tor = topo->getNode(it->first);
        for(int j=0; j<tor->getNumOutLinks(); j++){
            tor->getLinkOut(j)->getRemoteGate()
        }
    }*/

    WATCH_MAP(hosts);
    WATCH_MAP(switchClasses);

}

int CentralizedECMPRouting::getNodeIndex(cModule* module){
    for(int i=0;i<topo->getNumNodes(); i++){
        if(topo->getNode(i) == topo->getNodeFor(module))
            return i;
    }
    throw cRuntimeError("Node not found in the topology");
}

bool CentralizedECMPRouting::isSpine(int nodeId){
    auto node = switchClasses.find(nodeId);

    if(node == switchClasses.end())
        return false;
    else if(node->second == SPINE)
        return true;
    else
        return false;
}


bool CentralizedECMPRouting::isTor(int nodeId){
    auto node = switchClasses.find(nodeId);

    if(node == switchClasses.end())
        return false;
    else if(node->second == TOR)
        return true;
    else
        return false;
}

bool CentralizedECMPRouting::isLeaf(int nodeId){
    auto node = switchClasses.find(nodeId);

    if(node == switchClasses.end())
        return false;
    else if(node->second == LEAF)
        return true;
    else
        return false;
}

bool CentralizedECMPRouting::isHost(int nodeId){
    for(auto it=hosts.begin(); it != hosts.end(); it++)
        if(it->second == nodeId) return true;

    return false;
}

