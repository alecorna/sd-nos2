//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "PFCController.h"
#include "RdmaApp/RDMABasicApp.h"
#include "RdmaApp/RDMA.h"
Define_Module(PFCController);

void PFCController::initialize()
{
    //numPorts = (int) par("numberPorts");
    if( getModuleByPath("^.eth[0]") != nullptr)
        numberOfPorts = getModuleByPath("^.eth[0]")->getVectorSize();
    else
        throw cRuntimeError("Impossible to get number of ports");

    if((bool) par("isSwitch") == true)
        relay = check_and_cast<PQRelay *>(getModuleByPath(par("relayPath")));

    PFCFrameXON = par("PFCFramesXON");
    PFCFrameXOFF = par("PFCFramesXOFF");

    PFCBytesXON = par("PFCBytesXON");
    PFCBytesXOFF = par("PFCBytesXOFF");

    //numberOfPorts = (int) par("numberPorts");
    //numberOfPorts = numPorts;
    numberOfPriorities = (int) par("numberPriorities");

    inputStates = new int*[numberOfPorts];
    outputStates = new int*[numberOfPorts];
    capacityOPQ = new int*[numberOfPorts];
    pfcEnabledOnQueue = new int[numberOfPriorities];

    for(int i=0; i<numberOfPorts; i++){
        outputStates[i]= new int[numberOfPriorities];
        capacityOPQ[i]= new int[numberOfPriorities];
        inputStates[i] = new int[numberOfPriorities];
    }
    for(int i=0; i<numberOfPorts; i++){
        for(int j=0; j<numberOfPriorities; j++){
            inputStates[i][j]=0;
            outputStates[i][j]=0;
            capacityOPQ[i][j]=0;
            pfcEnabledOnQueue[j]=0;
        }
    }

    // Enable PFC queues
    const char *enabledPQs = par("PFCenabledPQs");
    if(strcmp("ALL", enabledPQs) == 0) {
        for(int i=0; i<numberOfPriorities; i++) {
            pfcEnabledOnQueue[i]=1;
        }
    } else {
        cStringTokenizer tokenizer(enabledPQs);
        const char *token;

        while ((token = tokenizer.nextToken()) != nullptr) {
            pfcEnabledOnQueue[atoi(token)]=1;
        }
    }


    WATCH(numPorts);
    WATCH(numberOfPorts);

}

void PFCController::finish(){
    for(int i=0; i<numberOfPorts; i++){
            delete[] outputStates[i];
            delete[] capacityOPQ[i];
            delete[] inputStates[i];
    }
    delete[] outputStates;
    delete[] capacityOPQ;
    delete[] inputStates;
    delete[] pfcEnabledOnQueue;
}

void PFCController::handleMessage(cMessage *msg)
{

}

// check if PFC is enabled for the PQ, and run it in the two versions ( switch and host )
// In any case returns the packet priority as queue index
int PFCController::notifyPacketArrival(EtherFrameCoS *frame){
    Enter_Method("notifyPacketArrival(...)");

    if(pfcEnabledOnQueue[frame->getPcp()] == 0) return frame->getPcp();

    if((bool) par("isSwitch") == false)
        runPFCHost(frame, PKT_ARRIVAL);
    else
        runPFCSwitch(frame, PKT_ARRIVAL);

    return frame->getPcp();
}

/*
 * Packet msg leaving the queue. Check if the corresponding input is paused and react accordingly.
 *
 */
void PFCController::notifyPacketDeparture(EtherFrameCoS *frame){
    Enter_Method("notifyPacketDeparture(...)");

    int PQIndex = frame->getPcp();

    if(pfcEnabledOnQueue[PQIndex] == 0) return;

    if((bool) par("isSwitch"))
        runPFCSwitch(frame, PKT_DEPARTURE);
    else
        runPFCHost(frame, PKT_DEPARTURE);
}

bool PFCController::isInputPaused(int sourcePort, int PQIndex){
    return (inputStates[sourcePort][PQIndex] == PAUSED) ? true : false;
}

void PFCController::changeInputState(int sourcePort, int PQIndex, InputState state){
    inputStates[sourcePort][PQIndex]=state;

}

void PFCController::generateAndSendPauseFrame(int sourcePort, int PQIndex, double timeToStop){
    getQueueByIndex(sourcePort)->generatePausePacket(PQIndex, timeToStop);
}

VOQ* PFCController::getQueueByIndex(int interface){
    char path[100];
    sprintf(path, "^.eth[%d].queue", interface);
    cModule* VOQUncasted = getModuleByPath(path);

    if(dynamic_cast<VOQ*>(VOQUncasted) == nullptr)
        throw cRuntimeError("Impossible to get VOQ by path");

    return check_and_cast<VOQ *>(VOQUncasted);

}

int PFCController::getTotalFrameCapacityPerInputPriority(int sourcePort, int PQIndex){
    int capacity = 0;

    for(int i = 0; i < numPorts; i++)
        capacity += getQueueByIndex(i)->getFrameCapacityPerInputPriority(sourcePort, PQIndex);

    return capacity;
}

int PFCController::getTotalFrameOccupancyPerInputPriority(int sourcePort, int PQIndex){
    int numFrames = 0;

    for(int i=0; i<numPorts; i++)
        numFrames += getQueueByIndex(i)->getFrameOccupancyPerInputPriority(sourcePort, PQIndex);

    return numFrames;
}

void PFCController::notifyPauseArrival(int sourcePort, int PQIndex, int timeToPause){
    for(int i=0; i<numPorts; i++){
        getQueueByIndex(i)->pauseQueue(sourcePort, PQIndex, timeToPause);
    }
}

void PFCController::pauseRDMAApps(double timeToPause){
    cModule *module = getModuleByPath(par("rdmaModule"));
    RDMA *rdmaModule = check_and_cast<RDMA *>(module);
    ASSERT(rdmaModule != nullptr);
    rdmaModule->pauseAllRDMATransmissions(timeToPause * (double) par("pauseGranularity"));
}

double PFCController::getStandardPauseTime(){
    return (double)par("pauseDuration")*(double)par("pauseGranularity");
}

void PFCController::runPFCHost(EtherFrameCoS *frame, int triggerState){
    int PQIndex = frame->getPcp();

    int queueOccupancyFrames = getTotalFrameOccupancyPerInputPriority(0, PQIndex);

    EV << "Current occupancy: " << queueOccupancyFrames <<" frames\n";

    if(triggerState == PKT_DEPARTURE){
        if(queueOccupancyFrames <= PFCFrameXOFF && pausedApplication == 1){
            pausedApplication = 0;
            pauseRDMAApps(0);
        }
    }
    else{
        if(queueOccupancyFrames+1 > PFCFrameXON && pausedApplication == 0){
            pausedApplication = 1;
            pauseRDMAApps(getStandardPauseTime());
        }
    }
}

void PFCController::runPFCSwitch(EtherFrameCoS *frame, int triggerState){
    int sourcePort = frame->getArrivalPort();
    int PQIndex = frame->getPcp();

    int queueOccupancyFrames = getTotalFrameOccupancyPerInputPriority(sourcePort, PQIndex);

    EV << "Current occupancy: " << queueOccupancyFrames<<" frames\n";

    // activate and deactivate PFC pause
    if(triggerState == PKT_ARRIVAL){
        if((queueOccupancyFrames + 1) > PFCFrameXON && !isInputPaused(sourcePort, PQIndex)){
            changeInputState(sourcePort, PQIndex, PAUSED);
            generateAndSendPauseFrame(sourcePort, PQIndex, getStandardPauseTime());
        }
    }
    else{
        if(queueOccupancyFrames <= PFCFrameXOFF && isInputPaused(sourcePort, PQIndex)){
            changeInputState(sourcePort, PQIndex, OPERATIONAL);
            generateAndSendPauseFrame(sourcePort, PQIndex, 0);
        }
    }
}
