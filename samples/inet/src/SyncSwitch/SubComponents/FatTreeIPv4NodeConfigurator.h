/*
 * FatTreeIPv4NodeConfigurator.h
 *
 *  Created on: Sep 5, 2019
 *      Author: alessandrocornacchia
 */

#ifndef SYNCSWITCH_SUBCOMPONENTS_FATTREEIPV4NODECONFIGURATOR_H_
#define SYNCSWITCH_SUBCOMPONENTS_FATTREEIPV4NODECONFIGURATOR_H_

#include "inet/common/INETDefs.h"
#include "inet/common/lifecycle/ILifecycle.h"
#include "inet/common/lifecycle/NodeStatus.h"
#include "inet/networklayer/contract/IInterfaceTable.h"
#include "inet/networklayer/ipv4/IIPv4RoutingTable.h"
#include "SyncSwitch/SubComponents/FatTreeIPv4NetworkConfigurator.h"


/**
 * This module provides the static configuration for the IPv4RoutingTable and
 * the IPv4 network interfaces of a particular node in the network.
 *
 * For more info please see the NED file.
 */
class INET_API FatTreeIPv4NodeConfigurator : public cSimpleModule, public ILifecycle
{
  protected:
    NodeStatus *nodeStatus;
    IInterfaceTable *interfaceTable;
    IIPv4RoutingTable *routingTable;
    FatTreeIPv4NetworkConfigurator *networkConfigurator;    // XXX Alessandro

  public:
    FatTreeIPv4NodeConfigurator();

  protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void handleMessage(cMessage *msg) override { throw cRuntimeError("this module doesn't handle messages, it runs only in initialize()"); }
    virtual void initialize(int stage) override;
    virtual bool handleOperationStage(LifecycleOperation *operation, int stage, IDoneCallback *doneCallback) override;
    virtual void prepareNode();
    virtual void prepareInterface(InterfaceEntry *interfaceEntry);
    virtual void configureInterface();
    virtual void configureRoutingTable();
};



#endif /* SYNCSWITCH_SUBCOMPONENTS_FATTREEIPV4NODECONFIGURATOR_H_ */
