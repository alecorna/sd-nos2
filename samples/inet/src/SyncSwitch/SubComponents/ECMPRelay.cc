//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "ECMPRelay.h"
#include "inet/common/LayeredProtocolBase.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/ILifecycle.h"
#include "inet/linklayer/ethernet/EtherMACBase.h"
#include "inet/networklayer/ipv4/IPv4Datagram.h"
#include "inet/transportlayer/udp/UDPPacket.h"
#include "inet/networklayer/common/IPProtocolId_m.h"

#define ECMP 1
#define PACKET_SPRAYING 2

Define_Module(ECMPRelay);

void ECMPRelay::initialize(int stage)
{
    if (stage == INITSTAGE_LOCAL) {
        // number of ports
        numPorts = gate("ifOut", 0)->size();
        if (gate("ifIn", 0)->size() != numPorts)
            throw cRuntimeError("the sizes of the ifIn[] and ifOut[] gate vectors must be the same");

        numProcessedFrames = numDiscardedFrames = 0;

        addressTable = check_and_cast<IMACAddressTable *>(getModuleByPath(par("macTablePath")));

        WATCH(numProcessedFrames);
        WATCH(numDiscardedFrames);
    }
    else if (stage == INITSTAGE_LINK_LAYER) {
        NodeStatus *nodeStatus = dynamic_cast<NodeStatus *>(findContainingNode(this)->getSubmodule("status"));
        isOperational = (!nodeStatus) || nodeStatus->getState() == NodeStatus::UP;
    }
    WATCH_MAP(kAddressTable);
}

void ECMPRelay::handleAndDispatchFrame(EtherFrame *frame)
{
    int inputport = frame->getArrivalGate()->getIndex();
    //isDestinationLocal();
    numProcessedFrames++;

    if(dynamic_cast<EtherFrameCoS*>(frame) == nullptr)  throw cRuntimeError("Frame is not a CoS frame");

    EtherFrameCoS* cosFrame = check_and_cast<EtherFrameCoS *>(frame);
    cosFrame->setNumberHops(cosFrame->getNumberHops()+1);

    if(updateAddressTable(inputport, frame->getSrc(), cosFrame->getNumberHops()) == 0){
        EV << "Received looped frame. Dropping.\n";
        delete frame;
        return;
    }
    // update address table
    //addressTable->updateTableWithAddress(inputport, frame->getSrc());


    // handle broadcast frames first
    if (frame->getDest().isBroadcast()) {
        EV << "Broadcasting broadcast frame " << frame << endl;
        broadcastFrame(frame, inputport);
        return;
    }

    /*if(dynamic_cast<PriorityPauseFrame *>(frame) != nullptr){
        PriorityPauseFrame *ppFrame = check_and_cast<PriorityPauseFrame *>(frame);
        EV << "Received priority pause frame destined to port " << ppFrame->getPausePort() << " and priority " << ppFrame->getPausePriority() << ".\n";
        send(frame, "ifOut", ppFrame->getPausePort());
        return;
    }*/

    // Finds output port of destination address and sends to output port
    // if not found then broadcasts to all other ports instead
    //int outputport = addressTable->getPortForAddress(frame->getDest());
    //int outputport = getECMPPortForAddress(cosFrame); XXX Alessandro
    // TODO add parameter to control source routing behaviour
    int outputport = -1;
    if(dynamic_cast<EtherFrameCoS *>(frame) != nullptr){
        outputport = check_and_cast<EtherFrameCoS *>(frame)->getOutPortStack().top();  // get the last label
        frame->getOutPortStack().pop();                // remove it
    }

    // should not send out the same frame on the same ethernet port
    // (although wireless ports are ok to receive the same message)
    if (inputport == outputport) {
        EV << "Output port is same as input port, " << frame->getFullName()
                                                   << " dest " << frame->getDest() << ", discarding frame\n";
        numDiscardedFrames++;
        delete frame;
        return;
    }

    if (outputport >= 0) {
        EV << "Sending frame " << frame << " with dest address " << frame->getDest() << " to port " << outputport << endl;
        emit(LayeredProtocolBase::packetSentToLowerSignal, frame);
        if(dynamic_cast<EtherFrameCoS *>(frame) != nullptr){
            check_and_cast<EtherFrameCoS *>(frame)->setOutPort(outputport);
        }
        send(frame, "ifOut", outputport);
    }
    else {
        EV << "Dest address " << frame->getDest() << " unknown, broadcasting frame " << frame << endl;
        broadcastFrame(frame, inputport);
    }
}

// add to address table entry if never seen, update existing entry if path of smaller cost
// is found. Return 1 if something changed, otherwise 0
int ECMPRelay::updateAddressTable(int inputPort, MACAddress &srcMAC, int numberOfHops){
    auto it = kAddressTable.find(srcMAC);
    //printTable();
    if(it == kAddressTable.end()){ //No entry with that MAC is present
        int* outputPortCosts  = new int[numPorts];
        for(int i=0; i < numPorts; i++)
            outputPortCosts[i] = -1;
        outputPortCosts[inputPort] = numberOfHops;
        kAddressTable.insert(std::pair<MACAddress&,int*>(srcMAC,outputPortCosts));
    }
    else{
        if( (it->second[inputPort] >= numberOfHops) || (it->second[inputPort] == -1) )
            it->second[inputPort] = numberOfHops;
        else
            return 0;
    }
    return 1;
}

void ECMPRelay::printTable(){
    for(auto it=kAddressTable.begin(); it != kAddressTable.end(); ++it){
        EV << it->first << ": ";
        for(int i=0; i< numPorts; i++){
            EV << it->second[i] << " ";
        }
        EV << "\n";
    }
}

int ECMPRelay::getECMPPortForAddress(EtherFrameCoS *frame){
    auto it = kAddressTable.find(frame->getDest());
    IPv4Datagram *ipv4Datagram;
    EV << "Frame destination " << frame->getDest() << "\n";
    if(it == kAddressTable.end()){
        EV << "Frame destination not found!\n";
        return -1;
    }

    int numberOfECMPPorts = 0;
    int shortestPathCost = -1;
    int lastSeenPort = -1;

    // find output port corresponding to shortest path
    for(int i=0; i < numPorts; i++){
        if( (it->second[i] != -1) && (it->second[i] < shortestPathCost || shortestPathCost == -1)){
            shortestPathCost = it->second[i];
        }
    }

    if(shortestPathCost == -1) return shortestPathCost;

    // evaluate number of equal cost paths
    for(int i=0; i < numPorts; i++){
        if(it->second[i] != -1 && it->second[i] == shortestPathCost){
            numberOfECMPPorts++;
            lastSeenPort = i;
        }
    }

    if(numberOfECMPPorts == 1) return lastSeenPort;

    if(numberOfECMPPorts == 0) throw cRuntimeError("MAC is present but the output ports are not");

    int outputPort = (frame->getDest().getInt() + frame->getSrc().getInt());

    // sum fields for ECMP
    if(dynamic_cast<IPv4Datagram *>(frame->getEncapsulatedPacket()) != nullptr){
        ipv4Datagram = check_and_cast<IPv4Datagram*>(frame->getEncapsulatedPacket());
        outputPort += ipv4Datagram->getSrcAddress().getInt();
        outputPort += ipv4Datagram->getDestAddress().getInt();
        outputPort += ipv4Datagram->getTransportProtocol();

        if(dynamic_cast<ITransportPacket*>(ipv4Datagram->getEncapsulatedPacket()) != nullptr){
            if(ipv4Datagram->getTransportProtocol() == IP_PROT_ROCE || ipv4Datagram->getTransportProtocol() == IP_PROT_UDP){
                UDPPacket *udpPacket = check_and_cast<UDPPacket*>(ipv4Datagram->getEncapsulatedPacket());
                outputPort += udpPacket->getSrcPort();
                outputPort += udpPacket->getDestPort();

                if(dynamic_cast<RDMAPacket*>(udpPacket->getEncapsulatedPacket()) != nullptr){
                    RDMAPacket* rdmaPacket = check_and_cast<RDMAPacket*>(udpPacket->getEncapsulatedPacket());
                    if(rdmaPacket->getExplixitRouting() == true){
                        rdmaPacket->setExplixitRouting(false);
                        return rdmaPacket->getExplicitSpineIndex();
                    }
                }
            }
        }
    }
    if((int) par("loadBalancing") == ECMP)
        outputPort = outputPort % numberOfECMPPorts;
    else if((int) par("loadBalancing") == PACKET_SPRAYING)
        outputPort = intuniform(0, numberOfECMPPorts-1);

    // use outputPort that ranges in [ 0, numECMPPorts ], as index in a vector containing ALL
    // possible output ports, including those associated to higher cost (finds the i^th shortest path port)
    int t=0;
    for(int i=0; i<numPorts; i++){
        if(it->second[i] == shortestPathCost){
            if(t == outputPort){
                return i;
            }
            else t++;
        }
    }
    return -1;
}
