//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __SYNCNET_PQCLASSIFIER_H_
#define __SYNCNET_PQCLASSIFIER_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/EtherFrameClassifier.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "inet/common/queue/IQueueAccess.h"
#include "PFCController.h"
#include "PFCPerFlowController.h"


using namespace inet;

/**
 * TODO - Generated class
 */
class PQClassifier : public EtherFrameClassifier
{
protected:
    PFCController *pfcController;
  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    virtual int classify(cMessage* msg);

};

#endif
