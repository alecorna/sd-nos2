//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_PFCCONTROLLER_H_
#define __INET_PFCCONTROLLER_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "PQRelay.h"
#include "VOQ.h"
using namespace inet;

/**
 * TODO - Generated class
 */
class PFCController : public cSimpleModule
{
    protected:
        enum InputState {OPERATIONAL = 0, PAUSED};
        enum TriggerType {PKT_ARRIVAL=0, PKT_DEPARTURE};
        PQRelay *relay;

        int** inputStates;
        int** outputStates;
        int numberOfPorts = 0;
        int numberOfPriorities = 0;
        int** capacityOPQ;
        int* pfcEnabledOnQueue;
        int numPorts = 0;
        double PFCFrameXON = 0;
        double PFCFrameXOFF = 0;
        double PFCBytesXON = 0;
        double PFCBytesXOFF = 0;
        int pausedApplication = 0;
    protected:
        virtual void initialize();
        virtual void finish();
        virtual void handleMessage(cMessage *msg);
        virtual void changeInputState(int sourcePort, int PQIndex, InputState state);
        virtual bool isInputPaused(int sourcePort, int PQIndex);
        virtual double getStandardPauseTime();
        virtual void pauseRDMAApps(double timeToPause);
        virtual int getTotalFrameCapacityPerInputPriority(int sourcePort, int PQIndex);
        virtual int getTotalFrameOccupancyPerInputPriority(int sourcePort, int PQIndex);
        virtual void runPFCHost(EtherFrameCoS *frame, int triggerState);
        virtual void runPFCSwitch(EtherFrameCoS *frame, int triggerState);
        virtual void generateAndSendPauseFrame(int sourcePort, int PQIndex, double timeToStop);
        virtual VOQ* getQueueByIndex(int interface);




    public:
        virtual int notifyPacketArrival(EtherFrameCoS *frame);
        virtual void notifyPacketDeparture(EtherFrameCoS *frame);
        virtual void notifyPauseArrival(int sourcePort, int PQIndex, int timeToPause);
        //virtual void generatePause();
        //virtual void registerQueue(); // TODO: register abs path toqueues. Make everything more versatile.

};

#endif
