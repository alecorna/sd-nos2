//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_VOQ_H_
#define __INET_VOQ_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "inet/common/queue/PassiveQueueBase.h"

using namespace inet;

/**
 * TODO - Generated class
 */
class VOQ : public cSimpleModule,  public IPassiveQueue
{
public:
    virtual void generatePausePacket(int PQIndex, double timeToStop);
    virtual int getFrameCapacityPerInputPriority(int iqIndex, int pqIndex);
    virtual int getFrameOccupancyPerInputPriority(int iqIndex, int pqIndex);
    virtual void pauseQueue(int iqIndex, int pqIndex, double timeToPause);
    virtual void unpauseQueue(int iqIndex, int pqIndex);


protected:
    virtual void initialize(int stage) override;
    virtual int numInitStages() const override;
    virtual void finish() override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void manageSelfMessages(cMessage *msg);
    virtual void manageFrameArrivals(cMessage *msg);
    virtual void sendOut(int queueIndex, int PQIndex);
    virtual int getRandomIndexToServe(bool* nonEmptyQueues);
    virtual void sendDiscoveryMessage();
    virtual int getTotalQueueSize();
    virtual int getNonEmptyQueues(bool* nonEmptyQueues);
    virtual bool serveQueueRandomScheduler();

protected:
    cModule *pfcController;
    enum QueueStatus {OPERATIONAL, PAUSED};
    enum SelfMessageKinds {PAUSE_EVENT, DISCOVERY, TOKEN_EVENT, TDM_EVENT };
    enum SupportedSchedulers {RANDOM=0};
    int **queueStatus;
    //int *queueStatus;
    cMessage*** pauseMessages;
    cQueue** viQueues;
    //cQueue* viQueues;
    int numInputQueues = 0;
    int numPriorities = 0;
    int iqFrameCapacity = 0;
    int packetsRequestedFromUs = 0;
    cOutVector **queueOccupancyVector;
    cStdDev **queueOccupancyStatistics;
    int usedScheduler = RANDOM;
    bool pfcEnabled = false;
    cOutVector totalQueueOccupancy;


public:
    virtual void requestPacket() override;
    virtual int getNumPendingRequests() override;
    virtual bool isEmpty() override;
    virtual void clear() override;
    virtual cMessage *pop() override;
    virtual void addListener(IPassiveQueueListener *listener) override;
    virtual void removeListener(IPassiveQueueListener *listener) override;
};

class VOQPauseControlInfo : public ::omnetpp::cObject
{
protected:
    int sourcePort;
    int priority;
    simtime_t enqueueTime;
    simtime_t dequeueTime;

public:
    virtual void setSourcePort(int sourcePort){ this->sourcePort=sourcePort; }
    virtual int getSourcePort(){ return this->sourcePort; }
    virtual void setPriority(int priority){ this->priority=priority; }
    virtual int getPriority(){ return this->priority; }
    virtual void setEnqueueTime(simtime_t enqueueTime){ this->enqueueTime=enqueueTime; }
    virtual simtime_t getEnqueueTime(){ return this->enqueueTime; }

};

#endif
