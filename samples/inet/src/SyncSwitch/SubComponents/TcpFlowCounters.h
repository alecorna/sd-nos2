//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SYNCSWITCH_SUBCOMPONENTS_TCPFLOWCOUNTERS_H_
#define SYNCSWITCH_SUBCOMPONENTS_TCPFLOWCOUNTERS_H_

#include <omnetpp.h>
#include <unordered_map>
#include "inet/networklayer/ipv4/IPv4Datagram.h"
#include "inet/transportlayer/tcp_common/TCPSegment.h"

using namespace omnetpp;
using namespace std;
using namespace inet;

class TcpFlowCounters {
    private:
        unordered_map<int,pair<long,simtime_t>> byteSent;
        long flushCounter = 1000;

    private:
        int computeKeyFromPacket(IPv4Datagram const *msg);
        bool isSyn(IPv4Datagram const* msg);
        void checkAndFlush();

    public:
        void notifyPacketArrival(IPv4Datagram const *msg);
        long getFlowBytes(IPv4Datagram const *msg);

};

#endif /* SYNCSWITCH_SUBCOMPONENTS_TCPFLOWCOUNTERS_H_ */
