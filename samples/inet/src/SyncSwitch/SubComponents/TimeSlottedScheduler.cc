//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TimeSlottedScheduler.h"
#include "DropTailQueueII.h"
#include "PQScheduler.h"

Define_Module(TimeSlottedScheduler);

simsignal_t TimeSlottedScheduler::MSSPerSlotSignal = registerSignal("MSSPerSlotSig");
simsignal_t TimeSlottedScheduler::MSSPerSlotQueueEmptySignal = registerSignal("MSSPerSlotQueueEmptySig");

void TimeSlottedScheduler::initialize()
{
    numInputs = gateSize("in");
    for (int i = 0; i < numInputs; ++i) {
        cGate *inGate = isGateVector("in") ? gate("in", i) : gate("in");
        cGate *connectedGate = inGate->getPathStartGate();
        if (!connectedGate)
            throw cRuntimeError("Scheduler input gate %d is not connected", i);
        IPassiveQueue *inputModule = dynamic_cast<IPassiveQueue *>(connectedGate->getOwnerModule());
        if (!inputModule)
            throw cRuntimeError("Scheduler input gate %d should be connected to an IPassiveQueue", i);
        inputModule->addListener(this);
        inputQueues.push_back(inputModule);
    }

    outGate = gate("out");

    // TODO update state when topology changes

    if(dynamic_cast<TDMController*>(getModuleByPath(par("tdmControllerPath"))) == nullptr)
            throw cRuntimeError("TDMController path invalid");

    tdmController = check_and_cast<TDMController*>(getModuleByPath(par("tdmControllerPath")));
    frameDuration = tdmController->getFrameDuration();
    slotDuration = tdmController->getSlotDuration();
    transmissionEvent = new cMessage();
    tdmController->registerListener(this->getId(), this->getFullPath(), allQueuesEmpty);

    WATCH(transmissionStartTime);
    WATCH(packetsRequestedFromUs);
    WATCH(packetsToBeRequestedFromInputs);
    WATCH(slotEndTime);
    WATCH(MSStoSendPerSlot);
    WATCH(MSSSentPerslot);

}

void TimeSlottedScheduler::startSending(simtime_t txEndTime){
    Enter_Method_Silent();
    slotEndTime = txEndTime;
    schedulePacket();
    //scheduleAt(simTime() + slotEndTime, transmissionEvent);
}

void TimeSlottedScheduler::startSendingMSS(int MSStoSend){
    Enter_Method_Silent();
    MSStoSendPerSlot = MSStoSend;
    MSSSentPerslot = 0;
    schedulePacket();
    //scheduleAt(simTime()+slotDurationSeconds, transmissionEvent);
}


void TimeSlottedScheduler::requestPacket(){
    Enter_Method("requestPacket()");

    packetsRequestedFromUs++;
    //packetsToBeRequestedFromInputs++;
    bool success = schedulePacket();
    if (success)
        packetsToBeRequestedFromInputs--;
}


bool TimeSlottedScheduler::schedulePacket()
{
    int nonEmptyQueues[gateSize("in")+1];
    int totalNonEmptyQueues=0;
    int queueToServe=0;
    int pq=0;

    if(packetsRequestedFromUs <= 0) return false;

    //if(slotEndTime < simTime()) return false;
    if(slotEndTime <= simTime()){
        EV << "Can't transmit. Not my slot.\n";
        return false;
    }
    /*if(transmissionEvent->isScheduled() && transmissionEvent->getKind() == START){
        EV << "Can't transmit. Not my slot.\n";
        return false;
    }*/

    for(int i=0; i<inputQueues.size();i++) nonEmptyQueues[i]=0;

    // First try to serve the pause queue
    if(dynamic_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())!= nullptr){
        if( !check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->isEmpty() ){
            check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->requestPacket();
            EV << "Requesting packet from Pause Queue " << queueToServe << "\n";
            return true;
        }
    }


    for(pq=0; pq<(int) par("numberPriorities"); pq++){
        for(int i=1; i<gateSize("in"); i++){ // start from i=1 because i=0 is the pause queue
            ASSERT(dynamic_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule()) != nullptr);

            if(!check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->isEmpty(pq)){
                nonEmptyQueues[i]=1;
                totalNonEmptyQueues++;
            }
        }
        if(totalNonEmptyQueues != 0) break;
    }

    if(totalNonEmptyQueues == 0){
        EV << "All queues are empty\n";
        allQueuesEmpty = true;
        emit(MSSPerSlotQueueEmptySignal, MSSSentPerslot);
        return false;
    }

    queueToServe=getRandomIndexToServe(nonEmptyQueues, totalNonEmptyQueues, gateSize("in"));
    EV << "Queue to serve: "<< queueToServe <<"\n";

    ASSERT(queueToServe != -1);
    double possibleTxBytesInCurrentSlot = ((slotEndTime - simTime()) * tdmController->getLinkRate()/8).dbl();
    int pktLength = check_and_cast<PQScheduler *>(gate("in",queueToServe)->getPathStartGate()->getOwnerModule())->getNextPacketLengthBytes(pq);
    int totalBytesToTxForCurrentPkt = pktLength + ETH_CRC + ETH_PREAMBLE + ETH_IPG;
    simtime_t pktTxEndTime = simTime() + (simtime_t) (8 * totalBytesToTxForCurrentPkt / (tdmController->getLinkRate()));
    EV << "NEXT PACKET LENGTH BYTES = " << pktLength << "\n";
    EV << "Slot end time: " << slotEndTime << "\nPkt tx end time: " << pktTxEndTime << "\n";
    EV << "Can accomodate only " << possibleTxBytesInCurrentSlot << " bytes.\n";

    if(totalBytesToTxForCurrentPkt > possibleTxBytesInCurrentSlot){
        EV << "Next packet exceeds the length of the allocated slot.\n";
        return false;
    }

    /*if(MSSSentPerslot >= MSStoSendPerSlot){
        EV << "MAX MSS per slot reached\n";
        emit(MSSPerSlotSignal, MSSSentPerslot);
        return false;
    }
    MSSSentPerslot++;*/

    EV << "Requesting packet from queue " << queueToServe << "\n";
    check_and_cast<PQScheduler *>(gate("in",queueToServe)->getPathStartGate()->getOwnerModule())->requestPacket(pq);

    return true;

}

void TimeSlottedScheduler::handleMessage(cMessage *msg){
    if(msg->isSelfMessage()){
        slotEndTime = simTime() + slotDuration;

        if((bool) par("adaptiveSlots") == true)
            scheduleAdaptiveSlot(msg);
        else
            scheduleNonAdaptiveSlot(msg);
    }
    else{
        ASSERT(packetsRequestedFromUs > 0);
        packetsRequestedFromUs--;
        sendOut(msg);
    }
}

void TimeSlottedScheduler::scheduleNonAdaptiveSlot(cMessage *msg){

    if(msg->getKind() == START){
        msg->setKind(END);
        scheduleAt(slotEndTime, msg);
        if(packetsRequestedFromUs > 0)
            schedulePacket();
    }
    else if(msg->getKind() == END){
        msg->setKind(START);
        transmissionStartTime = tdmController->whenToSend(this->getId(), this->getFullPath(), allQueuesEmpty);
        scheduleAt(transmissionStartTime, msg);
    }
}

void TimeSlottedScheduler::scheduleAdaptiveSlot(cMessage *msg){
    scheduleAt(slotEndTime, msg);

    if(msg->getKind() == START){
        msg->setKind(END);
        schedulePacket();
    }
    else{
        if(numNonEmptyQueues() == 0)
            msg->setKind(WAIT);

        else{
            msg->setKind(START);
            transmissionStartTime = tdmController->adapativeWhenToSend(this->getId(), this->getFullPath(), allQueuesEmpty);
            cancelEvent(msg);
            scheduleAt(transmissionStartTime, msg);
        }
    }

}

int TimeSlottedScheduler::getRandomIndexToServe(int* nonEmptyQueues, int totalNonEmptyQueues, int numQueues){
    int queueToServe = 0;
    int j=0;
    queueToServe=intuniform(0, totalNonEmptyQueues-1);

    for(int i=0; i<numQueues; i++){
        if(nonEmptyQueues[i] == 1){
            if(j == queueToServe){
                return i;
            }
            j++;

        }
    }
    return -1;
}

void TimeSlottedScheduler::finalize()
{
    for (auto & elem : inputQueues)
        (elem)->removeListener(this);
    delete transmissionEvent;

}
int TimeSlottedScheduler::getNumNonEmptyQueues(){
    Enter_Method_Silent();
    return numNonEmptyQueues();
}


int TimeSlottedScheduler::numNonEmptyQueues(){
    int totalNonEmptyQueues = 0;

    for(int pq=0; pq<(int) par("numberPriorities"); pq++){
        for(int i=1; i<gateSize("in"); i++){
            ASSERT(dynamic_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule()) != nullptr);

            if(!check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->isEmpty(pq)){
                totalNonEmptyQueues++;
            }
        }
    }
    return totalNonEmptyQueues;
}
