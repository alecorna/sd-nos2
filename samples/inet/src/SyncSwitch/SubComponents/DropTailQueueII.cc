//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "DropTailQueueII.h"
#include "PQClassifier.h"

Define_Module(DropTailQueueII);

void DropTailQueueII::initialize()
{
    PassiveQueueBase::initialize();

    queue.setName(par("queueName"));

    //statistics
    emit(queueLengthSignal, queue.getLength());

    outGate = gate("out");

    // configuration
    frameCapacity = par("frameCapacity");
    pauseMessage = new cMessage();
    WATCH(state);
    WATCH(restartTime);
}

int DropTailQueueII::getLengthFrames(){
    return queue.getLength();

}
int DropTailQueueII::getLengthBytes(){
    int totalSizeBytes=0;
    for(int i=1;i<queue.getLength();i++){
        if(queue.get(i) != NULL)
            //EtherFrameCoS *frame = check_and_cast<EtherFrameCoS *>(queue.get(i));
            totalSizeBytes += check_and_cast<EtherFrameCoS *>(queue.get(i))->getByteLength();
    }
    return totalSizeBytes;
}

int DropTailQueueII::getFrameCapacity(){
    return (int) par("frameCapacity");
}

double DropTailQueueII::getPercentageFrameOccupancy(){
    double percentageOccupancy = 0;
    percentageOccupancy = (double) ((double) getLengthFrames()/((double) par("frameCapacity")));
    return percentageOccupancy;
}

void DropTailQueueII::handleMessage(cMessage *msg)
{
    // Manage queue-pause message
    if(msg->isSelfMessage()){
        state=WORKING;
        notifyListeners();
        return;
    }

    numQueueReceived++;

    emit(rcvdPkSignal, msg);

    if (packetRequested > 0) {
        packetRequested--;
        emit(enqueuePkSignal, msg);
        emit(dequeuePkSignal, msg);
        emit(queueingTimeSignal, SIMTIME_ZERO);
        sendOut(msg);
    }
    else {
        msg->setArrivalTime(simTime());
        cMessage *droppedMsg = enqueue(msg);
        if (msg != droppedMsg)
            emit(enqueuePkSignal, msg);

        if (droppedMsg) {
            numQueueDropped++;
            emit(dropPkByQueueSignal, droppedMsg);
            delete droppedMsg;
        }
        else
            notifyListeners();
    }
}

bool DropTailQueueII::isEmpty(){
    if(!pauseMessage->isScheduled())
        return queue.isEmpty();

    return true;
}

void DropTailQueueII::pauseQueue(double timeSlots){
    Enter_Method("pauseQueue(...)");
    EV << "Queue " << this->getFullName() << " pausing for "<< timeSlots << " seconds.\n";
    if(pauseMessage->isScheduled()){
        EV << "Cancelling pause interval.\n";
        cancelEvent(pauseMessage);
    }
    EV << "Rescheduling pause interval.\n";
    state=PAUSED;
    restartTime = simTime().dbl() + (double) timeSlots;
    scheduleAt(simTime() + (double) timeSlots, pauseMessage);
}

cMessage *DropTailQueueII::dequeue()
{
    if (isEmpty())
        return nullptr;

    cMessage *msg = (cMessage *)queue.pop();

    // statistics
    emit(queueLengthSignal, queue.getLength());

    return msg;
}

void DropTailQueueII::sendOut(cMessage *msg)
{
    if(inputPaused){
        return;
    }
    send(msg, outGate);
}

void DropTailQueueII::setInputPaused(bool value){
    inputPaused = value;
}

int DropTailQueueII::getNextPacketLengthBytes(){
    cPacket *nextPkt = (cPacket *) queue.get(0);

    if(nextPkt == nullptr) return -1;

    return nextPkt->getByteLength();
}

void DropTailQueueII::finish(){
    cancelAndDelete(pauseMessage);
}


