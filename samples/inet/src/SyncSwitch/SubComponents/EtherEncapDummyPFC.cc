//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EtherEncapDummyPFC.h"

Define_Module(EtherEncapDummyPFC);

void EtherEncapDummyPFC::initialize()
{
    // TODO - Generated method body
}

void EtherEncapDummyPFC::handleMessage(cMessage *frame)
{
    if(dynamic_cast<PriorityPauseFrame *>(frame) != nullptr){
            PriorityPauseFrame *ppFrame = check_and_cast<PriorityPauseFrame *>(frame);

            if(ppFrame->getArrivalGate() == gate("lowerLayerIn")){
                if(!ppFrame->getIsLocal()){
                    EV_INFO << "Received PFC frame " << frame << " from another switch.\n Sending to queues\n";
                    send(frame, "lowerLayerOut");
                }
                else{
                    EV_INFO << "Received PFC frame " << frame << " from same switch.\n Sending to relay\n";
                    //ppFrame->setIsLocal(false);
                    send(frame, "upperLayerOut");
                }
                return;
            }
    }

    if(frame->getArrivalGate() == gate("upperLayerIn")){
        send(frame, "lowerLayerOut");
    }
    else if(frame->getArrivalGate() == gate("lowerLayerIn")){
        send(frame, "upperLayerOut");
    }

}
