//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "RateLimitingScheduler.h"
#include "DropTailQueueII.h"
#include "PQScheduler.h"
Define_Module(RateLimitingScheduler);

void RateLimitingScheduler::initialize()
{
    numInputs = gateSize("in");
    for (int i = 0; i < numInputs; ++i) {
        cGate *inGate = isGateVector("in") ? gate("in", i) : gate("in");
        cGate *connectedGate = inGate->getPathStartGate();
        if (!connectedGate)
            throw cRuntimeError("Scheduler input gate %d is not connected", i);
        IPassiveQueue *inputModule = dynamic_cast<IPassiveQueue *>(connectedGate->getOwnerModule());
        if (!inputModule)
            throw cRuntimeError("Scheduler input gate %d should be connected to an IPassiveQueue", i);
        inputModule->addListener(this);
        inputQueues.push_back(inputModule);
    }

    outGate = gate("out");

    if(dynamic_cast<TDMController*>(getModuleByPath(par("tdmControllerPath"))) == nullptr)
            throw cRuntimeError("TDMController path invalid");

    tdmController = check_and_cast<TDMController*>(getModuleByPath(par("tdmControllerPath")));

    tokenBucketSize = (int) par("tokenBucketSize");

    operationalState = 0;

    if((bool) par("adaptiveSlots") == true){
       if((bool) par("perFlowRate") == false)
           tokenArrivalRate = tdmController->getAdaptiveTransmissionRate(this->getId(), this->getFullName(), 0);
       else
           tokenArrivalRate = tdmController->getAdaptiveTransmissionRatePerFlow(this->getId(), this->getFullName(), 0);
   }
    else
        tokenArrivalRate = tdmController->getNonAdaptiveTransmissionRate(this->getId(), this->getFullName(), 0);

    transmissionEvent = new cMessage();
    transmissionEvent->setKind(START);

    scheduleAt(simTime() + tokenArrivalRate + uniform(0, tokenArrivalRate), transmissionEvent);

    if((bool) par("perFlowRate") == true){
        cMessage* startOfTrafficGeneration = new cMessage();
        startOfTrafficGeneration->setKind(1);
        scheduleAt(par("startTime"), startOfTrafficGeneration);
    }

    WATCH(packetsRequestedFromUs);
    WATCH(packetsToBeRequestedFromInputs);
    WATCH(tokenArrivalRate);
    WATCH(tokenBucketSize);
    WATCH(bucket);
    WATCH(transmittedPacketsForCurrentToken);
}

void RateLimitingScheduler::handleMessage(cMessage *msg)
{
    int increment = 0;

    if(msg->isSelfMessage()){

        if((msg->getKind() == 1) && ((bool) par("perFlowRate") == true)){
            transmittedPacketsForCurrentToken = 0;
            delete msg;
            return;
        }

        if(numNonEmptyQueues() == 0 && operationalState == 1){
            increment = -1;
            operationalState = 0;
        }
        else if(numNonEmptyQueues() != 0 && operationalState == 0){
            increment = 1;
            operationalState = 1;
        }

        if((bool) par("perFlowRate") == true){
            if(bucket == 0)
                bucket++;
        }
        else{
            bucket++;
            if(bucket > tokenBucketSize)
                bucket = tokenBucketSize;
        }




        if((bool) par("adaptiveSlots") == true){
            if((bool) par("perFlowRate") == false){
                tokenArrivalRate = tdmController->getAdaptiveTransmissionRate(this->getId(), this->getFullName(), increment);
                transmittedPacketsForCurrentToken=0;
            }
            else
                tokenArrivalRate = tdmController->getAdaptiveTransmissionRatePerFlow(this->getId(), this->getFullName(), increment);
        }
        scheduleAt(simTime() + tokenArrivalRate, msg);
        if((bool) par("isLeaky") == true) {
            bucket = 0;
        }
        schedulePacket();
    }
    else{
        //ASSERT(packetsRequestedFromUs > 0);
        //packetsRequestedFromUs--;
        sendOut(msg);
    }
}


int RateLimitingScheduler::getRandomIndexToServe(int* nonEmptyQueues, int totalNonEmptyQueues, int numQueues){
    int queueToServe = 0;
    int j=0;
    queueToServe=intuniform(0, totalNonEmptyQueues-1);

    for(int i=0; i<numQueues; i++){
        if(nonEmptyQueues[i] == 1){
            if(j == queueToServe){
                return i;
            }
            j++;

        }
    }
    return -1;
}

void RateLimitingScheduler::finalize()
{
    for (auto & elem : inputQueues)
        (elem)->removeListener(this);
    delete transmissionEvent;

}


bool RateLimitingScheduler::schedulePacket()
{
    int nonEmptyQueues[gateSize("in")+1];
    int totalNonEmptyQueues=0;
    int queueToServe=0;
    int pq=0;

    if(packetsRequestedFromUs == 0) return false;

    if( (bucket == 0) && ((bool) par("isLeaky") == false))
        return false;
    else
        if(((bool) par("perFlowRate") == false) && ((bool) par("isLeaky") == true)){
            if(transmittedPacketsForCurrentToken != 0)
                return false;
            else
                transmittedPacketsForCurrentToken++;
        }
        else if( ((bool) par("perFlowRate") == true) && (transmittedPacketsForCurrentToken == tdmController->getAverageFlowSize())){
            bucket=0;
            transmittedPacketsForCurrentToken = 0;
        }
        else{
            transmittedPacketsForCurrentToken++;
        }


    for(int i=0; i<inputQueues.size();i++) nonEmptyQueues[i]=0;

    // First try to serve the pause queue
    if(dynamic_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())!= nullptr){
        if( !check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->isEmpty() ){
            check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->requestPacket();
            EV << "Requesting packet from Pause Queue " << queueToServe << "\n";
            return true;
        }
    }


    for(pq=0; pq<(int) par("numberPriorities"); pq++){
        for(int i=1; i<gateSize("in"); i++){ // start from i=1 because i=0 is the pause queue
            ASSERT(dynamic_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule()) != nullptr);

            if(!check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->isEmpty(pq)){
                nonEmptyQueues[i]=1;
                totalNonEmptyQueues++;
            }
        }
        if(totalNonEmptyQueues != 0) break;
    }
    if(totalNonEmptyQueues == 0){
        EV << "All queues are empty\n";
        return false;
    }

    queueToServe=getRandomIndexToServe(nonEmptyQueues, totalNonEmptyQueues, gateSize("in"));
    EV << "Queue to serve: "<< queueToServe <<"\n";

    ASSERT(queueToServe != -1);

    EV << "Requesting packet from queue " << queueToServe << "\n";
    check_and_cast<PQScheduler *>(gate("in",queueToServe)->getPathStartGate()->getOwnerModule())->requestPacket(pq);
    ASSERT(packetsRequestedFromUs > 0);
    packetsRequestedFromUs--;
    return true;

}

int RateLimitingScheduler::numNonEmptyQueues(){
    int totalNonEmptyQueues = 0;

    for(int pq=0; pq<(int) par("numberPriorities"); pq++){
        for(int i=1; i<gateSize("in"); i++){
            ASSERT(dynamic_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule()) != nullptr);

            if(!check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->isEmpty(pq)){
                totalNonEmptyQueues++;
            }
        }
    }
    return totalNonEmptyQueues;
}
