/*
 * IPv4AddressHashTemplate.h
 *
 *  This class extends the std hash<> template in order to enable Ipv4Address class
 *  to be used as key. The hash function is a simple wrapper for an hash on the
 *  string representation of the IP address
 *
 *  Created on: Jul 30, 2019
 *      Author: AlessandroCornacchia
 */

#ifndef SYNCSWITCH_SUBCOMPONENTS_IPV4ADDRESSHASHTEMPLATE_H_
#define SYNCSWITCH_SUBCOMPONENTS_IPV4ADDRESSHASHTEMPLATE_H_


#include "inet/networklayer/contract/ipv4/IPv4Address.h"

namespace std {
    using namespace inet;
    template <> struct hash<IPv4Address> {
      std::size_t operator()(const IPv4Address& t) const { return hash<string>()(t.str()); }
    };
}

#endif
