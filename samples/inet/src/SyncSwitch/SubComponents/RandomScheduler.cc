//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "RandomScheduler.h"
//#include <crandom.h>
#include "DropTailQueueII.h"
#include "PQScheduler.h"
Define_Module(RandomScheduler);

void RandomScheduler::initialize()
{
    int numInputs = gateSize("in");
    for (int i = 0; i < numInputs; ++i) {
        cGate *inGate = isGateVector("in") ? gate("in", i) : gate("in");
        cGate *connectedGate = inGate->getPathStartGate();
        if (!connectedGate)
            throw cRuntimeError("Scheduler input gate %d is not connected", i);
        IPassiveQueue *inputModule = dynamic_cast<IPassiveQueue *>(connectedGate->getOwnerModule());
        if (!inputModule)
            throw cRuntimeError("Scheduler input gate %d should be connected to an IPassiveQueue", i);
        inputModule->addListener(this);
        inputQueues.push_back(inputModule);
    }

    outGate = gate("out");
    WATCH(packetsRequestedFromUs);
    WATCH(packetsToBeRequestedFromInputs);
    // TODO update state when topology changes
}

bool RandomScheduler::schedulePacket()
{

    int nonEmptyQueues[gateSize("in")+1];
    int totalNonEmptyQueues=0;
    int queueToServe=0;
    int pq=0;
    for(int i=0; i<inputQueues.size();i++) nonEmptyQueues[i]=0;

    // First try to serve the pause queue
    if(dynamic_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())!= nullptr){
        if( !check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->isEmpty() ){
            check_and_cast<DropTailQueueII *>(gate("in",0)->getPathStartGate()->getOwnerModule())->requestPacket();
            EV << "Requesting packet from Pause Queue " << queueToServe << "\n";
            return true;
        }
    }
    for(pq=0; pq<(int) par("numberPriorities"); pq++){
        for(int i=1; i<gateSize("in"); i++){ // start from i=1 because i=0 is the pause queue
            ASSERT(dynamic_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule()) != nullptr);

            if(!check_and_cast<PQScheduler *>(gate("in",i)->getPathStartGate()->getOwnerModule())->isEmpty(pq)){
                nonEmptyQueues[i]=1;
                totalNonEmptyQueues++;
            }
        }
        if(totalNonEmptyQueues != 0) break;
    }
    if(totalNonEmptyQueues == 0){
        EV << "All queues are empty\n";
        return false;
    }

    queueToServe=getRandomIndexToServe(nonEmptyQueues, totalNonEmptyQueues, gateSize("in"));
    EV << "Queue to serve: "<< queueToServe <<"\n";
    //return true;
    ASSERT(queueToServe != -1);
    EV << "Requesting packet from queue " << queueToServe << "\n";
    check_and_cast<PQScheduler *>(gate("in",queueToServe)->getPathStartGate()->getOwnerModule())->requestPacket(pq);
    return true;
}

int RandomScheduler::getRandomIndexToServe(int* nonEmptyQueues, int totalNonEmptyQueues, int numQueues){
    int queueToServe = 0;
    int j=0;
    queueToServe=intuniform(0, totalNonEmptyQueues-1);

    for(int i=0; i<numQueues; i++){
        if(nonEmptyQueues[i] == 1){
            if(j == queueToServe){
                return i;
            }
            j++;

        }
    }
    return -1;
}
