//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "ThresholdController.h"
#include <string>
#include <sstream>
#include <fstream>
#include <limits>

#ifndef __SYSEXECUTE__
    #define __SYSEXECUTE__
    string sys_execute(string);
#endif

Define_Module(ThresholdController);

simsignal_t ThresholdController::thresholdsSignal = registerSignal("thresholds");

//simsignal_t ThresholdController::assignedPrioritiesSignal = registerSignal("assignedPriorities");

/* Finds the proper threshold given the number of bytes sent. Cost O( log N ) */
int ThresholdController::getPacketPriority(unsigned long numBytesSent, unsigned pkSize,  unsigned N) {
    unsigned p = findIndexOfFirstGreaterThreshold(N, numBytesSent);

    // If with the current pksize we would overcome the threshold,
    // means that we are tx u = numBytesSent + pkSize - thresholds(p)
    // additional bytes on priority p, that ideally should be served at
    // prioriry p+1. This is inevitable on packet simualtor, thus
    // we tx on next priority with probability u / pksize
    // to obtain more accurate load balancing
    if(numBytesSent + pkSize > thresholds[N][p] && p != numPriorities-1) {
        double u = (numBytesSent + pkSize - thresholds[N][p]) / pkSize;
       // With probability u transmit on next priority level
        if(uniform(0,1) < u){
            p = p+1;
        }
    }
    return p;
}

// this is used if priority assingment is done a priori and not packet by packet
int ThresholdController::getFlowPriority(unsigned long numBytesSent, unsigned N) {
    Enter_Method("getPriority(...)");
    return findIndexOfFirstGreaterThreshold(N, numBytesSent);
}

// this returns a random priority
int ThresholdController::getPriority() {
    Enter_Method("getPriority(...)");
    return intrand(numPriorities);
}


// assign to a given number of bytes, the index associated to the first element in threshold greater than
// numBytesSent. Example: threshold [50K 75K 1M] numByesSent = 52.6K ==>> idx = 1
int ThresholdController::findIndexOfFirstGreaterThreshold(unsigned N, unsigned long numBytesSent) {
    auto upper_th = lower_bound(thresholds[N].begin(), thresholds[N].end(), numBytesSent);
    if(upper_th==thresholds[N].end()) {
        upper_th--;
    }
    return distance(thresholds[N].begin(), upper_th);    // return the index i.e the priority
}

void ThresholdController::initialize(int stage) {
    if(INITSTAGE_LOCAL == stage) {
        numPrioritiesPerPort = (int)par("numPriorities");
        enableSpatialDiversity = (bool)par("enableSpatialDiversity");
        numSpines = (int)par("numSpines");
        confg = dynamic_cast<FatTreeIPv4NetworkConfigurator*>(getModuleByPath("^.configurator"));
        if(confg == nullptr){
            throw cRuntimeError("NetworkConfigurator path invalid or not found");
        }
        numPriorities = confg->getNumPriorities();
        if(strcmp("ES", par("splitAlgorithm").stringValue()) == 0) {
            sa = EQUAL_PERCENTILES;
        } else if (strcmp("LB", par("splitAlgorithm").stringValue()) == 0) {
            sa = LOAD_BALANCE;
        } else if (strcmp("OPT", par("splitAlgorithm").stringValue()) == 0) {
            sa = OPTIMAL;
        }
        if(strcmp("ES", par("downroutingPriorityScheme").stringValue()) == 0) {
            downroutingPriorityScheme = EQUAL_PERCENTILES;
        } else if (strcmp("LB", par("downroutingPriorityScheme").stringValue()) == 0) {
            downroutingPriorityScheme = LOAD_BALANCE;
        } else if (strcmp("OPT", par("downroutingPriorityScheme").stringValue()) == 0) {
            downroutingPriorityScheme = OPTIMAL;
        } else if (strcmp("NOT NEEDED", par("downroutingPriorityScheme").stringValue()) == 0) {
            downroutingPriorityScheme = NONE;
        } else {
            throw cRuntimeError("Please specify how to handle prioritites in down-send");
        }
        filename = (const char*) par("filename");
        // read flow length distribution
       string fld = par("flowLengthDistribution").stdstringValue();
       transform(fld.begin(), fld.end(), fld.begin(), ::tolower);

       if (fld == "exp") {
           distrib = FlowSizeDistribution::EXPONENTIAL;
       }
       else if (fld == "uniform") {
           distrib = FlowSizeDistribution::UNIFORM;
       } else if (fld == "pareto") {
           distrib = FlowSizeDistribution::PARETO;
       } else if (fld == "bimodal") {
           distrib = BIMODAL;
       } else if (fld == "pareto_websearch") {
           distrib = PARETO_WEBSEARCH;
       } else if (fld == "pareto_datamining") {
           distrib = PARETO_DATAMINING;
       }
       string transport_protocol = par("tprotocol").stdstringValue();
       transform(transport_protocol.begin(), transport_protocol.end(), transport_protocol.begin(), ::tolower);
       if (transport_protocol == "udp") {
           protocol = IP_PROT_UDP;
       } else if (transport_protocol == "tcp") {
           protocol = IP_PROT_TCP;
       } else {
           throw cRuntimeError("Protocol unspecified or unsupported");
       }

      computeThresholds();
      for(auto m : thresholds) {
        WATCH_VECTOR(m.second);
      }
    }

}

void ThresholdController::readFromFile() {

    vector<double> t;
    // read file line by line and store thresholds
    std::ifstream file(filename);
    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            t.push_back(stod(line,nullptr));
        }
        file.close();
    } else {
        throw cRuntimeError(("File " + string(filename) + " not found").c_str());
    }
    thresholds.insert({numPriorities, t});
}

void ThresholdController::computeInternally() {
    if(sa == EQUAL_PERCENTILES) {
            thresholds.insert({numPriorities , equalSplit(numPriorities)});
            thresholds.insert({numPrioritiesPerPort , equalSplit(numPrioritiesPerPort)});
        } else if (sa == LOAD_BALANCE) {
            throw cRuntimeError("Not implemented load balance policy yet");
    }
}

string sys_execute(string command) {
    string result = "";
    command = command + " 2>&1";
    // Open pipe to file
   FILE* pipe = popen(command.c_str(), "r");
   if (!pipe) {
      throw cRuntimeError("Error executing command: pipe opening failed");
   }

   char buffer[512];
   // read till end of process:
   while (!feof(pipe)) {

      // use buffer to read and add to result
      if (fgets(buffer, 512, pipe) != NULL)
         result += buffer;
   }
   pclose(pipe);
   return result;
}

/*
 * Run external script and store result
 */
void ThresholdController::runScript() {
        string task, cdf, command, result, numqueues, numspatialdemotions, mtu;
        string extra = "";

        if (this->distrib == FlowSizeDistribution::PARETO_WEBSEARCH) {
            cdf = "'ws_bp'";
        } else if (this->distrib == FlowSizeDistribution::PARETO_DATAMINING){
            cdf = "'dm_bp'";
        }
        if (protocol == IP_PROT_TCP) {
            mtu = "1460";
        } else if (protocol == IP_PROT_UDP) {
            mtu = "1472";
        }
        char load[8];
        double offeredLoad = (double)par("offeredLoad");
        if (offeredLoad < 0 || offeredLoad >= 1) {
            throw cRuntimeError("Invalid offered load");
        }
        sprintf(load, "%.2f", offeredLoad);

       string scriptname = par("filename").stdstringValue();
       scriptname = sys_execute("echo -n " + scriptname);  // useful if path contains some env variables
       // if SD : compute thresholds both for KxN queues (used to split among spines) and N queues
       // (used to split in ToR to Server i.e. last hop)
        if (enableSpatialDiversity) {
           numspatialdemotions = to_string(confg->getNumSpatialDemotions());
           if (sa == PriorityScheme::OPTIMAL) { // spatial diversity with optimal split
               task = "'sdp'";
               numqueues = to_string(numPrioritiesPerPort);
           } else if (this->sa == PriorityScheme::LOAD_BALANCE) {
               task = "'etsn'";
               numqueues = to_string(numPriorities);
               if (confg->getPriorityAllocationPolicy() == FatTreeIPv4NetworkConfigurator::PriorityAllocationPolicy::REPEAT_LOW) {
                   numqueues = to_string(numPriorities - 1);
                   extra = " --ignore-above 0.8";
               }
           }
           // spatial diversity threshold set
           command = "python3 " + scriptname + " --bytes --mtu " + mtu + " --num_queues " + numqueues + " --num_server " + numspatialdemotions +
                   " --task " + task + " --lambda " + string(load) + " --cdf " + cdf + extra;
           EV_INFO << "Executing " << command << endl;
           result = sys_execute(command);
           thresholds.insert({numPriorities, parseFromString(result, '\n')});

           // single queue threshold set: how to recombine priorities in downrouting
           // at last hop
           if (downroutingPriorityScheme != PriorityScheme::NONE) { // in case SD-IDEAL we do not need downrouting schemes
               if (downroutingPriorityScheme == PriorityScheme::OPTIMAL) {
                  task = "'mlfq'";
               } else if (this->downroutingPriorityScheme == PriorityScheme::EQUAL_PERCENTILES) {
                   task = "'esn'";
               } else if (this->downroutingPriorityScheme == PriorityScheme::LOAD_BALANCE) {
                  task = "'etsn'";
               }
               command = "python3 " + scriptname + " --bytes --mtu " + mtu + " --num_queues " + to_string(numPrioritiesPerPort) +
                      " --task " + task + " --lambda " + string(load) + " --cdf " + cdf;
               EV_INFO << "Executing " << command << endl;
               result = sys_execute(command);
               thresholds.insert({numPrioritiesPerPort, parseFromString(result, '\n')});
           }
        } else { // no spatial diversity.
            if (this->sa == PriorityScheme::EQUAL_PERCENTILES) {
                task = "'esn'";
            } else if (this->sa == PriorityScheme::LOAD_BALANCE) {
                task = "'etsn'";
            } else if (this->sa == PriorityScheme::OPTIMAL) {
                task = "'mlfq'";
            }
            command = "python3 " + scriptname + " --bytes --mtu " + mtu + " --num_queues " + to_string(numPriorities) +
                         " --task " + task + " --lambda " + string(load) + " --cdf " + cdf;
            EV_INFO << "Executing " << command << endl;
            result = sys_execute(command);
            thresholds.insert({numPriorities, parseFromString(result, '\n')});
        }
}

// compute thresholds for flow classification and priority assignment
void ThresholdController::computeThresholds() {
    string how = par("computeMethod").stdstringValue();
    transform(how.begin(), how.end(), how.begin(), ::toupper);

    if (how == "COMPUTE") {             // compute here
        computeInternally();
    }
    else if (how == "RUN_EXTERNAL") {   // run external script
      runScript();
    } else if (how == "READ_FILE") {    // read from file
      readFromFile();
    } else if (how == "GIVEN") {    // read from parameter
        string th_str = par("thresholds").stringValue();
        thresholds[numPriorities] = parseFromString(th_str, ' ');
    }

    // if we have more than numPriorities thresholds, something is wrong, if we have exactly NP-1 thresholds,
    // we assume is missing the last thresholds which is actually redundant (we always set to +Inf for convenience)
    if(thresholds[numPriorities].size() > numPriorities ||
            thresholds[numPrioritiesPerPort].size() > numPrioritiesPerPort ) {
        throw cRuntimeError("Number of threshold not coherent with number of priorities");
    }
    if (thresholds[numPriorities].size() == numPriorities-1) {
        thresholds[numPriorities].push_back(std::numeric_limits<double>::infinity());
    }
    if (thresholds[numPrioritiesPerPort].size() == numPrioritiesPerPort-1) {
        thresholds[numPrioritiesPerPort].push_back(std::numeric_limits<double>::infinity());
    }
    EV << "Thresholds: ";
    for(unsigned i=0; i < numPriorities; i++) {
        EV << thresholds[numPriorities][i] << "B ";
        emit(ThresholdController::thresholdsSignal, thresholds[numPriorities][i]);
    }
    EV << endl;

    if (enableSpatialDiversity) {
        EV << "Downrouting thresholds: ";
        for(auto t : thresholds[numPrioritiesPerPort]) {
            EV << t << "B ";
        }
        EV << endl;
    }

}

vector<double> ThresholdController::equalSplit(int numPercentiles) {
    vector<double> thrvec;
    switch(distrib) {
        case UNIFORM: {
            long minFs = (long)par("minFlowSize");
            long maxFs = (long)par("maxFlowSize");

            for(int i=1; i <= numPercentiles; i++) {
                double th = (maxFs-minFs)*(double)i/numPercentiles + minFs;    // invert random uniform CDF
                thrvec.push_back(th);
            }
            break;
        }
        case EXPONENTIAL: {
            for(int i=1; i <= numPercentiles; i++) {
                double p = 1.0/(double)par("expFlowSizeMean");  // Geometric distribution since bytes are discrete quantity
                double y = (double)i/numPercentiles;
                //double u = (double)par("minFlowSize");
                if (y == 1) {
                    y = 0.999999999;
                }
                double th = log( 1-y ) / log(1-p);    // invert random exponential CDF
                thrvec.push_back(th);
            }
            break;
        }
        case PARETO: {
            for(int i=1; i <= numPercentiles; i++) {
                double alpha = (double) par("paretoShape");
                double k = (double) par("minFlowSize");
                double p = (double) par("maxFlowSize");
                double y = (double)i/numPercentiles;
                if (y == 1) {
                    y = 0.99999;
                }
                double denom = pow(p,alpha) - y*(pow(p,alpha) - pow(k,alpha));

                double th = p * k / pow(denom, 1/alpha);
                thrvec.push_back(th);
            }
            break;
        }
        default:
            break;
    }
    return thrvec;
}

vector<double> ThresholdController::parseFromString(string th_str, char separator){
    vector<double> thrvec;

    if (th_str == "nan") {
        throw cRuntimeError("Distribution for threshold computation not handled, please set thresholds manually");
    }
    stringstream check1(th_str);
    string intermediate;

    // Tokenizing w.r.t. space ' '
    while(getline(check1, intermediate, separator))
    {
        stringstream ss(intermediate);
        double th;
        ss >> th;
        if (ss.fail()) {
            throw cRuntimeError(th_str.c_str());
        }

        thrvec.push_back(th);
    }
    return thrvec;
}

/*
 * Till now we support only two priorities (i.e 2 spines)
 */
double ThresholdController::equalTrafficSplit() {

    double th;

    switch(distrib) {
        case UNIFORM: {
           long minFs = (long)par("minFlowSize");
           long maxFs = (long)par("maxFlowSize");
           th = maxFs - sqrt((pow(maxFs,2) - pow(minFs,2)) / 2.0);
           break;
        }
        case BIMODAL: {
           long minFs = (long)par("minFlowSize");
           long maxFs = (long)par("maxFlowSize");
           double pa = par("bimodalMinFlowSizeProbability").doubleValue();
           double pb = 1 - pa;
           th = (pb * (minFs + maxFs) - minFs) / (2 * pb);
           break;
        }
        case EXPONENTIAL: {
            double p = 1./(double)par("expFlowSizeMean");

            th = log(0.5)/log(1-p);
            break;
        }
        default: {
            throw cRuntimeError("This distribution is not handled by Equal Traffic Split");
        }
    }
    return th;
}
