//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "SourceTagger.h"
#include "PFCController.h"

Define_Module(SourceTagger);

void SourceTagger::initialize()
{
    // TODO - Generated method body
}

void SourceTagger::handleMessage(cMessage *message)
{
    int portIndex = getParentModule()->getIndex();

    // Manage pause frame arrival -> Talk with PFCController
    if(dynamic_cast<PriorityPauseFrame *>(message) != nullptr){
        PriorityPauseFrame *ppFrame = check_and_cast<PriorityPauseFrame *>(message);
        PFCController *pfcController = check_and_cast<PFCController *>(getModuleByPath(par("pfcControllerPath")));
        ASSERT(pfcController != nullptr);
        pfcController->notifyPauseArrival(portIndex, ppFrame->getPausePriority(), ppFrame->getPauseTime());
        delete message;
    }
    else if(dynamic_cast<EtherFrameCoS *>(message) != NULL){
        EtherFrameCoS *msg = check_and_cast<EtherFrameCoS *>(message);
        msg->setArrivalPort(portIndex);
        send(message, "out");
    }
}
