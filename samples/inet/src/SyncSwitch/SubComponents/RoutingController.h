//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef SYNCSWITCH_SUBCOMPONENTS_ROUTINGCONTROLLER_H_
#define SYNCSWITCH_SUBCOMPONENTS_ROUTINGCONTROLLER_H_

#include <omnetpp.h>
#include "inet/networklayer/configurator/ipv4/IPv4NetworkConfigurator.h"
#include "SyncSwitch/SubComponents/FatTreeIPv4NetworkConfigurator.h"
#include "inet/common/InitStages.h"
#include "inet/linklayer/common/MACAddress.h"
#include "inet/networklayer/contract/ipv4/IPv4Address.h"
#include "inet/networklayer/configurator/base/NetworkConfiguratorBase.h"
#include "inet/networklayer/ipv4/IPv4Datagram.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "inet/networklayer/common/InterfaceEntry.h"
#include "SyncSwitch/SubComponents/IPv4AddressHashTemplate.h"
#include "SyncSwitch/SubComponents/TorIdRoutingTable.h"
#include "ThresholdController.h"
#include <cstdint>
#include <stack>
#include <unordered_map>
#include <string>
#include <iostream>

#include "TcpFlowCounters.h"


typedef FatTreeIPv4NetworkConfigurator::Node Node;
typedef FatTreeIPv4NetworkConfigurator::InterfaceInfo FatTreeInterfaceInfo;

using namespace omnetpp;
using namespace inet;
using namespace std;

// wrapper for return value of routing information method
struct RoutingInfoCoS {
    // return values
    stack<int> outPorts;
    stack<uint8_t> priorities;

    // arguments
    int desiredPriority = -1;
    unsigned long transmittedBytes;
    unsigned packetSize;
};

enum LoadBalancing { PER_FLOW_ECMP, PACKET_SPRAYING };

class RoutingController : public cSimpleModule {

    protected:
        NetworkConfiguratorBase::Topology *topology;        // network topology
        unordered_map<IPv4Address, Node*> ipToNodePtr;      // fast conversion IP->Node*
        ThresholdController* thCtrl;                        // manages the priority thresolds
        LoadBalancing lbpolicy;
        bool staticClassification;
        int numPrioritiesPerSpine;
        int numSpines;
        int numPriorities;
        bool spatialDiversity;

    public:
        virtual RoutingInfoCoS requestRoutingInformation(cPacket* msg);

    protected:
        virtual int numInitStages() const override { return NUM_INIT_STAGES; };
        virtual void initialize(int stage) override;
        virtual void handleMessage(cMessage *msg) override;
        virtual void finish() override;

    private:
        void createIPAddressToNodeMapping();
        void setStaticRoutes();
        Node* getTorFor(const IPv4Address& addr);
        //void initializeRoutingTables(); OLD
        void routingLookup(Node* currentSwitch, Node* destination, IPv4Datagram* datagram, RoutingInfoCoS *outGateStack);
        int forward(IPv4Datagram* datagram, Node* src, Node* dst, RoutingInfoCoS* routeInfo);
        int getInterfaceToHost(const IPv4Address& host);
        int loadBalance(IPv4Datagram* ipv4Datagram, int numECMPPorts);
        void fillPriorityAtInterface(FatTreeInterfaceInfo* iface, RoutingInfoCoS* ri);
};

#endif /* SYNCSWITCH_SUBCOMPONENTS_ROUTINGCONTROLLER_H_ */
