//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_TDMCONTROLLER_H_
#define __INET_TDMCONTROLLER_H_

#include <omnetpp.h>
#include <queue>
using namespace omnetpp;

/**
 * TODO - Generated class
 */

#define MSS_BYTES 1500
#define ETH_HEADER 18
#define ETH_IPG 12
#define ETH_CRC 4
#define ETH_PREAMBLE 4
#define ETH_OH_PHY_BYTES 20
#define DUMMY_BYTE 1

class TDMController : public cSimpleModule
{

    protected:
        virtual void initialize() override;
        virtual int findMaxHostIndex();
        virtual void handleMessage(cMessage *msg) override;

        //virtual void handleMessage(cMessage *msg);

        simtime_t *allocatedSlots;
        simtime_t frameDuration;
        simtime_t slotDurationSeconds;
        simtime_t nextAvailableSlot;
        int numHosts = 0;
        int numSources = 0;
        int slotDurationMSS = 0;
        int linkRate = 0;
        std::map<int, int> moduleIDs; // <RealModuleID, ProgressiveID>
        std::map<int, std::string> moduleNames; // <RealModuleID, Full_path_of_the_module>

        cMessage *slotTriggerMessage;
        int lastServed = 0;
        std::queue<int> servingOrder;

    public:
        virtual simtime_t whenToSend(int hostID, std::string fullName, bool moreToSend);
        virtual simtime_t whenToStop(int hostID);
        virtual int getLinkRate();
        virtual simtime_t getFrameDuration();
        virtual simtime_t getSlotDuration();
        virtual void finalize();
        virtual simtime_t adapativeWhenToSend(int hostID, std::string fullName, bool moreToSend);
        virtual int registerListener(int hostID, std::string fullName, bool moreToSend);
        virtual int registerListenerScheduler(int hostID, std::string fullName);

    public:
        virtual simtime_t getAdaptiveTransmissionRate(int hostID, std::string fullName, int increment);
        virtual simtime_t getNonAdaptiveTransmissionRate(int hostID, std::string fullName, int moreToSend);
        virtual int getAverageFlowSize();
        virtual simtime_t getAdaptiveTransmissionRatePerFlow(int hostID, std::string fullName, int increment);
    protected:
        int numConcurrentTransmissions = 0;


};

#endif
