//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_ETHERENCAPCOS_H_
#define __INET_ETHERENCAPCOS_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/EtherEncap.h"
#include "inet/common/INETDefs.h"
#include "RdmaApp/RDMABasicApp.h"
#include "inet/linklayer/ethernet/Ethernet.h"
#include "inet/networklayer/ipv4/IPv4Datagram.h"
#include "SyncSwitch/SubComponents/RoutingController.h"

#define RDMA_UDP_DST_PORT 4792

using namespace inet;

/**
 * TODO - Generated class
 */
class EtherEncapCos : public EtherEncap
{
  protected:
    virtual void processPacketFromHigherLayer(cPacket *msg) override;
    virtual void processFrameFromMAC(EtherFrame *msg) override;

  protected:
      RoutingController* controller;

  public:
      virtual int numInitStages() const override { return NUM_INIT_STAGES; };
      virtual void initialize(int stage) override;

};

#endif
