//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "PQScheduler.h"
#include "DropTailQueueII.h"

Define_Module(PQScheduler);

void PQScheduler::initialize()
{
    int numInputs = gateSize("in");
    for (int i = 0; i < numInputs; ++i) {
        cGate *inGate = isGateVector("in") ? gate("in", i) : gate("in");
        cGate *connectedGate = inGate->getPathStartGate();
        if (!connectedGate)
            throw cRuntimeError("Scheduler input gate %d is not connected", i);
        IPassiveQueue *inputModule = dynamic_cast<IPassiveQueue *>(connectedGate->getOwnerModule());
        if (!inputModule)
            throw cRuntimeError("Scheduler input gate %d should be connected to an IPassiveQueue", i);
        inputModule->addListener(this);
        inputQueues.push_back(inputModule);
    }

    outGate = gate("out");

    pfcController = dynamic_cast<PFCController *>(getModuleByPath(par("pfcControllerPath")));
    //ASSERT(pfcController != nullptr);
    WATCH(packetsRequestedFromUs);
    WATCH(packetsToBeRequestedFromInputs);
    // TODO update state when topology changes
}


bool PQScheduler::schedulePacket()
{
    for (auto inputQueue : inputQueues) {

        if (!inputQueue->isEmpty()) {
            inputQueue->requestPacket();
            return true;
        }
    }
    return false;
}

void PQScheduler::sendOut(cMessage *msg)
{
    send(msg, outGate);

    if(pfcController != nullptr) {
        pfcController->notifyPacketDeparture(check_and_cast<EtherFrameCoS*>(msg));
    }

}

bool PQScheduler::isEmpty(int PQIndex){

    ASSERT(dynamic_cast<DropTailQueueII *>(gate("in",PQIndex)->getPathStartGate()->getOwnerModule()) != nullptr);

    if(!check_and_cast<DropTailQueueII *>(gate("in",PQIndex)->getPathStartGate()->getOwnerModule())->isEmpty())
        return false;

    return true;
}

int PQScheduler::getQueueLength(int PQIndex){
    ASSERT(dynamic_cast<DropTailQueueII *>(gate("in",PQIndex)->getPathStartGate()->getOwnerModule()) != nullptr);
    return check_and_cast<DropTailQueueII *>(gate("in",PQIndex)->getPathStartGate()->getOwnerModule())->getLengthFrames();
}


void PQScheduler::requestPacket(int PQIndex)
{
    Enter_Method("requestPacket()");

    packetsRequestedFromUs++;
    packetsToBeRequestedFromInputs++;
    bool success = schedulePacket(PQIndex);
    if (success)
        packetsToBeRequestedFromInputs--;
}

int PQScheduler::getNextPacketLengthBytes(int PQIndex){
    return check_and_cast<DropTailQueueII *>(gate("in",PQIndex)->getPathStartGate()->getOwnerModule())->getNextPacketLengthBytes();

}

bool PQScheduler::schedulePacket(int PQIndex)
{
    DropTailQueueII *queueToRequest = check_and_cast<DropTailQueueII *>(gate("in",PQIndex)->getPathStartGate()->getOwnerModule());

    if(!queueToRequest->isEmpty()){
        queueToRequest->requestPacket();
        return true;
    }

    return false;
}
