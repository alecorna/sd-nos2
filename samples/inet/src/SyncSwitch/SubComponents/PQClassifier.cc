//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "../SubComponents/PQClassifier.h"
#include "DropTailQueueII.h"


Define_Module(PQClassifier);

void PQClassifier::initialize(){
    pfcController = dynamic_cast<PFCController *>(getModuleByPath(par("pfcControllerPath")));
}

void PQClassifier::handleMessage(cMessage *msg)
{
    int queueIndexToSend = (int) par("defaultQueue");
    if(dynamic_cast<EtherFrameCoS *>(msg) != nullptr){

        queueIndexToSend = classify(msg);

        EtherFrameCoS *message = check_and_cast<EtherFrameCoS *>(msg);
        if(pfcController != nullptr) {
            queueIndexToSend = pfcController->notifyPacketArrival(message);
        }
        send(msg,"out", queueIndexToSend);
    }
    else
        send(msg, "out", queueIndexToSend);
}

int PQClassifier::classify(cMessage* msg) {
    EtherFrameCoS *message = check_and_cast<EtherFrameCoS *>(msg);
    return message->getPcp();
}
