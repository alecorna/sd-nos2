//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_PFCPERFLOWCONTROLLER_H_
#define __INET_PFCPERFLOWCONTROLLER_H_

#include <omnetpp.h>
#include "PFCController.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "DropTailQueueII.h"
#include "PQRelay.h"
#include "inet/networklayer/common/IPProtocolId_m.h"
using namespace inet;

typedef std::pair<FlowInformation, int> observedFlow_t;
typedef std::pair<FlowInformation, simtime_t> blockedFlow_t;


class PFCPerFlowController : public PFCController
{
    protected:
        std::list<observedFlow_t> **currentFlows;
        std::list<blockedFlow_t> **blockedFlows;

    protected:
        virtual void initialize() override;
        virtual void updateQueueFlowState(EtherFrameCoS* frame, int increment);
        FlowInformation* getHeaviestFlow(int sourcePort, int PQIndex, int index);
        virtual void updateOrInsertFlow(FlowInformation newFlow, int sourcePort, int PQIndex, int increment);
    public:
        virtual int notifyPacketArrival(EtherFrameCoS *frame) override;
        virtual void notifyPacketDeparture(EtherFrameCoS *frame) override;
        virtual int getDiversityPerInput(int inputQueue, int PQIndex);

};

#endif
