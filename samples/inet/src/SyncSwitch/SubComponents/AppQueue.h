//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_APPQUEUE_H_
#define __INET_APPQUEUE_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "inet/common/queue/PassiveQueueBase.h"
#include "inet/networklayer/contract/ipv4/IPv4ControlInfo.h"
#include "inet/networklayer/contract/ipv6/IPv6ControlInfo.h"
#include "RdmaApp/RDMAPacket_m.h"
using namespace inet;

/**
 * TODO - Generated class
 */
class AppQueue : public cSimpleModule
{
protected:
    static simsignal_t wastedTimeSignal;

    enum SelfMessageKinds {PAUSE_EVENT, DISCOVERY, TOKEN_EVENT, TDM_EVENT };

    cOutVector *queueOccupancyVector;
    cOutVector totalQueueOccupancyVector;
    cOutVector flowEmissionRateVector;
    cStdDev *queueOccupancyStatistics;
    cStdDev emissionRate;
    cStdDev normalizedEmissionRate;
    cModule *fabricScheduler;
    cQueue* appQueues;
    int numInputQueues = 0;
    int totPackets = 0;
    int thisModuleIndex = -1;
    int thisModuleToRIndex = -1;
    int thisModuleIPAddress = -1;
    int usedScheduler = RL;
    cMessage** tokenMessages;
    simtime_t* tokenArrivalRates;
    double *assignedRates;
    double *totalAccumulatedBytes;
    int *assignedPaths;
    std::map<cMessage*, int> tokenMessageID;
    int tokenBucketSize;
    simtime_t txEndTime;

    int numberOfActiveRequests = 0;

protected:
    virtual int numInitStages() const override;
    virtual void initialize(int stage) override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void finish() override;
    virtual void manageSelfMessages(cMessage *msg);
    virtual void manageFrameArrivals(cMessage *msg);
    virtual void sendOut(int iqIndex);

    virtual void notifyControllerAboutNextPacket(int iqIndex, int currPktDstAdd);
    virtual void registerSchedulerModule();
    virtual void registerSchedulerListener();

    virtual int getDstAddressOfHOLPacket(int iqIndex);
    virtual int getDstAddressOfPacket(cMessage *msg);
    virtual simtime_t getHOLPktEnqueueTime(int iqIndex);
    virtual int getTotalQueueByteSize(int iqIndex);
    virtual void setSelectedPathForHOLPacket(int iqIndex, int selectedPath);

    virtual void initializeTDMScheduler();
    virtual void initializeRLScheduler();
    virtual void serveQueueRL(int iqIndex);
    virtual void serveQueueTDM(int iqIndex, int selectedPath);

    // Periodic renormalization
    virtual void renormalizeRates();

public:
    enum SupportedSchedulers {RANDOM=0, RL, TDM};
    virtual void setTokenArrivalRate(simtime_t arrivalRate, int iqIndex);
    virtual void setTransmissionEnd(simtime_t txEndTimeL, int iqIndex, int selectedPath);
    virtual int getByteSizeOfHOLPacket(int iqIndex);
    virtual bool isFlowOldOrNew(int index);
    virtual void updateAllRates(double* rates);
    virtual void setPathRL(int dstIP, int assignedSpine);

};

#endif
