//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EtherMACPause.h"

Define_Module(EtherMACPause);


void EtherMACPause::processFrameFromUpperLayer(EtherFrame *frame)
{
    ASSERT(frame->getByteLength() >= MIN_ETHERNET_FRAME_BYTES);

    EV_INFO << "Received " << frame << " from upper layer." << endl;

    if(dynamic_cast<PriorityPauseFrame *>(frame) != nullptr){
        PriorityPauseFrame *pauseFramePFC = check_and_cast<PriorityPauseFrame *>(frame);

        if(pauseFramePFC->getPausePort () != getParentModule()->getIndex()) {
            EV_DETAIL << "Pause frame received from upper layers\n";
            send(pauseFramePFC, "upperLayerOut");
            requestNextFrameFromExtQueue();
            return;
        }
        else
            pauseFramePFC->setIsLocal(false);

    }

    //emit(packetReceivedFromUpperSignal, frame);

    if (frame->getDest().equals(address)) {
        throw cRuntimeError("Logic error: frame %s from higher layer has local MAC address as dest (%s)",
                frame->getFullName(), frame->getDest().str().c_str());
    }

    if (frame->getByteLength() > MAX_ETHERNET_FRAME_BYTES) {
        throw cRuntimeError("Packet from higher layer (%d bytes) exceeds maximum Ethernet frame size (%d)",
                (int)(frame->getByteLength()), MAX_ETHERNET_FRAME_BYTES);
    }

    if (!connected || disabled) {
        EV_WARN << (!connected ? "Interface is not connected" : "MAC is disabled") << " -- dropping packet " << frame << endl;
        //emit(dropPkFromHLIfaceDownSignal, frame);
        numDroppedPkFromHLIfaceDown++;
        delete frame;

        requestNextFrameFromExtQueue();
        return;
    }

    // fill in src address if not set
    if (frame->getSrc().isUnspecified())
        frame->setSrc(address);

    bool isPauseFrame = (dynamic_cast<EtherPauseFrame *>(frame) != nullptr);
    //bool isPauseFrame = false;
    if (!isPauseFrame) {
        numFramesFromHL++;
        //emit(rxPkFromHLSignal, frame);
    }

    if (txQueue.extQueue) {
        ASSERT(curTxFrame == nullptr);
        curTxFrame = frame;
        fillIFGIfInBurst();
    }
    else {
        if (txQueue.innerQueue->isFull())
            throw cRuntimeError("txQueue length exceeds %d -- this is probably due to "
                                "a bogus app model generating excessive traffic "
                                "(or if this is normal, increase txQueueLimit!)",
                    txQueue.innerQueue->getQueueLimit());

        // store frame and possibly begin transmitting
        EV_DETAIL << "Frame " << frame << " arrived from higher layer, enqueueing\n";
        txQueue.innerQueue->insertFrame(frame);

        if (!curTxFrame && !txQueue.innerQueue->isEmpty())
            curTxFrame = (EtherFrame *)txQueue.innerQueue->pop();
    }

    if ((duplexMode || receiveState == RX_IDLE_STATE) && transmitState == TX_IDLE_STATE) {
        EV_DETAIL << "No incoming carrier signals detected, frame clear to send\n";
        startFrameTransmission();
    }
}


bool EtherMACPause::dropFrameNotForUs(EtherFrame *frame)
{
    // Current ethernet mac implementation does not support the configuration of multicast
    // ethernet address groups. We rather accept all multicast frames (just like they were
    // broadcasts) and pass it up to the higher layer where they will be dropped
    // if not needed.
    //
    // PAUSE frames must be handled a bit differently because they are processed at
    // this level. Multicast PAUSE frames should not be processed unless they have a
    // destination of MULTICAST_PAUSE_ADDRESS. We drop all PAUSE frames that have a
    // different muticast destination. (Note: Would the multicast ethernet addressing
    // implemented, we could also process the PAUSE frames destined to any of our
    // multicast adresses)
    // All NON-PAUSE frames must be passed to the upper layer if the interface is
    // in promiscuous mode.

    if (frame->getDest().equals(address))
        return false;

    if (frame->getDest().isBroadcast())
        return false;

    bool isPause = (dynamic_cast<EtherPauseFrame *>(frame) != nullptr);

    if (!isPause && (promiscuous || frame->getDest().isMulticast()))
        return false;

    if (isPause && frame->getDest().equals(MACAddress::MULTICAST_PAUSE_ADDRESS))
        return false;

    if(dynamic_cast<PriorityPauseFrame *>(frame) != nullptr)
        return false;

    EV_WARN << "Frame `" << frame->getName() << "' not destined to us, discarding\n";
    numDroppedNotForUs++;
    //emit(dropPkNotForUsSignal, frame);
    delete frame;
    return true;
}


void EtherMACPause::frameReceptionComplete()
{
    EtherTraffic *msg = check_and_cast<EtherTraffic *>(frameBeingReceived);
    frameBeingReceived = nullptr;

    if (dynamic_cast<EtherFilledIFG *>(msg) != nullptr) {
        delete msg;
        return;
    }

    bool hasBitError = msg->hasBitError();

    EtherFrame *frame = decapsulate(check_and_cast<EtherPhyFrame *>(msg));
    //emit(packetReceivedFromLowerSignal, frame);

    if (hasBitError) {
        numDroppedBitError++;
        //emit(dropPkBitErrorSignal, frame);
        delete frame;
        return;
    }

    if (dropFrameNotForUs(frame))
        return;

    if(dynamic_cast<PriorityPauseFrame *>(frame) != nullptr)
        EV_INFO << "Reception of pause frame" << frame << " successfully completed.\n";
    else
        EV_INFO << "Reception of " << frame << " successfully completed.\n";

    processReceivedDataFrame(check_and_cast<EtherFrame *>(frame));

}

void EtherMACPause::getNextFrameFromQueue()
{
    ASSERT(nullptr == curTxFrame);
    if (txQueue.extQueue) {
        EV << "Num pending requests = " << txQueue.extQueue->getNumPendingRequests();
        if (txQueue.extQueue->getNumPendingRequests() == 0){
            txQueue.extQueue->requestPacket();
        }
    }
    else {
        if (!txQueue.innerQueue->isEmpty())
            curTxFrame = (EtherFrame *)txQueue.innerQueue->pop();
    }
}

void EtherMACPause::handleEndTxPeriod()
{
    // we only get here if transmission has finished successfully, without collision
    if (transmitState != TRANSMITTING_STATE || (!duplexMode && receiveState != RX_IDLE_STATE))
        throw cRuntimeError("End of transmission, and incorrect state detected");

    currentSendPkTreeID = 0;

    if (curTxFrame == nullptr)
        throw cRuntimeError("Frame under transmission cannot be found");

    //emit(packetSentToLowerSignal, curTxFrame);    //consider: emit with start time of frame

    if (EtherPauseFrame *pauseFrame = dynamic_cast<EtherPauseFrame *>(curTxFrame)) {
        numPauseFramesSent++;
        //emit(txPausePkUnitsSignal, pauseFrame->getPauseTime());
    }
    else {
        unsigned long curBytes = curTxFrame->getByteLength();
        numFramesSent++;
        numBytesSent += curBytes;
        //emit(txPkSignal, curTxFrame);
    }

    EV_INFO << "Transmission of " << curTxFrame << " successfully completed.\n";
    delete curTxFrame;
    curTxFrame = nullptr;
    lastTxFinishTime = simTime();
    getNextFrameFromQueue();  // note: cannot be moved into handleEndIFGPeriod(), because in burst mode we need to know whether to send filled IFG or not

    // only count transmissions in totalSuccessfulRxTxTime if channel is half-duplex
    if (!duplexMode) {
        simtime_t dt = simTime() - channelBusySince;
        totalSuccessfulRxTxTime += dt;
    }

    backoffs = 0;

    // check for and obey received PAUSE frames after each transmission
    if (pauseUnitsRequested > 0) {
        // if we received a PAUSE frame recently, go into PAUSE state
        EV_DETAIL << "Going to PAUSE mode for " << pauseUnitsRequested << " time units\n";
        scheduleEndPausePeriod(pauseUnitsRequested);
        pauseUnitsRequested = 0;
    }
    else {
        EV_DETAIL << "Start IFG period\n";
        scheduleEndIFGPeriod();
        if (!txQueue.extQueue)
            fillIFGIfInBurst();
    }
}

void EtherMACPause::processReceivedDataFrame(EtherFrame *frame)
{
    // statistics
    unsigned long curBytes = frame->getByteLength();
    numFramesReceivedOK++;
    numBytesReceivedOK += curBytes;
    //emit(rxPkOkSignal, frame);

    numFramesPassedToHL++;
    //emit(packetSentToUpperSignal, frame);
    // pass up to upper layer
    EV_INFO << "Sending " << frame << " to upper layer.\n";
    send(frame, "upperLayerOut");
}

