//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#include <algorithm>
#include <random>

#include "RateController.h"
#include "AppQueue.h"

Define_Module(RateController);

void RateController::initialize()
{

    if(getModuleByPath("^.host[0]") == nullptr) throw cRuntimeError("Impossible to get host[0]");
    numHosts = getModuleByPath("^.host[0]")->getVectorSize();
    numSpines = (int) par("numSpines");
    linkRate = (int) par("linkRate"); // consider only TOR<->HOST links
    colorsPerSpine = numSpines * (int) par("colorsPerSpine");
    slotDurationSeconds = (simtime_t) 8 * (((int) par("slotDurationMSS")) * (MSS_BYTES + ETH_HEADER + ETH_CRC + ETH_PREAMBLE + ETH_IPG + DUMMY_BYTE)) / linkRate;

    requests = new RequestInformation**[numHosts];
    for(int i=0; i<numHosts; i++){
        requests[i] = new RequestInformation*[numHosts];
        for(int j=0; j<numHosts; j++){
            requests[i][j] = new RequestInformation(i,j, 0, 0);
        }
    }
    resetRequests();

    requestMatrix = new double*[numHosts];
    for(int i=0;i<numHosts; i++){
        requestMatrix[i] = new double[numHosts];
        for(int j=0; j<numHosts; j++){
            requestMatrix[i][j] = 0;
        }
    }

    usedScheduler = TDM;
    if(!strncmp(par("inputSchedulerType").stringValue(), "RLScheduler",10)){
        usedScheduler = RL;
        assignedRatesVector = new cOutVector*[numHosts];
        for(int i=0; i<numHosts; i++){
            assignedRatesVector[i] = new cOutVector[numHosts];
            for(int j=0;j<numHosts;j++){
                char vecName[100];
                sprintf(vecName, "RATE-%d-%d",i,j);
                assignedRatesVector[i][j].setName(vecName);
            }
        }

    }

    if(!strncmp(par("MSMType").stringValue(), "OF",2))
        usedMSM = OF;
    else if(!strncmp(par("MSMType").stringValue(), "SRJF",4))
        usedMSM = SRJF;
    else if(!strncmp(par("MSMType").stringValue(), "MMF",3))
        usedMSM = MMF;
    else
        usedMSM = RANDOM;

    slotTriggerMessage = new cMessage("Slot Triggering Message");
    scheduleAt(simTime() + slotDurationSeconds, slotTriggerMessage);

    WATCH_MAP(modulePaths);
    WATCH_MAP(moduleIDs);
    WATCH_MAP(moduleIDsReversed);
    WATCH(usedMSM);
    WATCH(usedScheduler);
    WATCH(numberOfActiveRequests);
    WATCH_MAP(moduleToRs);
    WATCH(numTors);

}

void RateController::handleMessage(cMessage *msg){

    if(moduleIDs.empty()) return;

    if(usedScheduler == TDM){
        computeMSMandSchedule();
        scheduleAt(simTime() + slotDurationSeconds, slotTriggerMessage);
    }
}

RateController::~RateController(){
    for(int i=0; i<numHosts; i++){
            for(int j=0; j<numHosts; j++) {
                delete requests[i][j];
            }
            delete[] requests[i];
            delete[] assignedRatesVector[i];
            delete[] requestMatrix[i];
        }
        delete[] requests;
        delete[] assignedRatesVector;
        delete[] requestMatrix;
};

void RateController::computeMSMandSchedule(){

    int **currentServingMatrix;
    int **nonBlockingPaths;
    int selectedPath;
    updateNumberOfActiveRequests();

    if(numberOfActiveRequests == 0) return;

    if(usedMSM == SRJF){
        printCurrentRequestMatrixResidualBytes();
        currentServingMatrix = scheduleAndServeSRJFmsm();
    }
    else if(usedMSM == OF){
        printCurrentRequestMatrixRequestTime();
        currentServingMatrix = scheduleAndServeOFmsm();
    }
    else if(usedMSM == MMF){
        printCurrentRequestMatrixRequestServed();
        currentServingMatrix = scheduleAndServeMMFmsm();

    }
    else{
        printCurrentRequestMatrixResidualBytes();
        currentServingMatrix = scheduleAndServeRandommsm();
    }

    nonBlockingPaths = selectNonBlockingPath(currentServingMatrix);

    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            if(currentServingMatrix[i][j] > 0){
                selectedPath = nonBlockingPaths[i][j];
                check_and_cast<AppQueue*>(getModuleByPath( (char*) (modulePaths.find(moduleIDsReversed.find(i)->second)->second).c_str() ))->
                        setTransmissionEnd(simTime() + slotDurationSeconds, j, selectedPath);
            }
        }
    }
    printAndDestroyNonBlockingPaths(nonBlockingPaths);
    printAndDestoryCurrentServingMatrix(currentServingMatrix);
}

int** RateController::scheduleAndServeRandommsm(){
    bool forbiddenDestinations[numHosts];
    bool forbiddenSources[numHosts];
    int **currentServingMatrix;
    std::vector<RequestInformation*> orderedRequests;

    currentServingMatrix = new int*[numHosts];
    for(int i=0;i<numHosts;i++){
        forbiddenDestinations[i] = false;
        currentServingMatrix[i] = new int[numHosts];
        for(int j=0; j<numHosts; j++){
            currentServingMatrix[i][j] = 0;
        }
    }


    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            if(requests[i][j]->getResidualBytes() > 0){
                orderedRequests.push_back(requests[i][j]);
            }
        }
    }

    for(auto it=orderedRequests.begin(); it != orderedRequests.end(); it++){
        if(!forbiddenDestinations[(*it)->getDstID()] && !forbiddenSources[(*it)->getSrcID()]){
            forbiddenDestinations[(*it)->getDstID()] = true;
            forbiddenSources[(*it)->getSrcID()] = true;
            currentServingMatrix[(*it)->getSrcID()][(*it)->getDstID()] = 1;
        }
    }
    orderedRequests.clear();
    return currentServingMatrix;
}

int** RateController::scheduleAndServeMMFmsm(){
    bool forbiddenDestinations[numHosts];
    bool forbiddenSources[numHosts];
    int **currentServingMatrix;
    std::vector<RequestInformation*> orderedRequests;

    currentServingMatrix = new int*[numHosts];
    for(int i=0;i<numHosts;i++){
        forbiddenDestinations[i] = false;
        forbiddenSources[i] = false;
        currentServingMatrix[i] = new int[numHosts];
        for(int j=0; j<numHosts; j++){
            currentServingMatrix[i][j] = 0;
        }
    }
    // Create ordered array
    for(int i=0; i< numHosts; i++){
        for(int j=0; j< numHosts; j++){
            int insertionPosition = 0;
            int currentLastServedTime = requests[i][j]->getLastServedTime();
            if(requests[i][j]->getResidualBytes() > 0){
                for(auto it = orderedRequests.begin(); it != orderedRequests.end(); it++){
                    if( currentLastServedTime < (*it)->getLastServedTime() )
                        insertionPosition++;
                    else
                        break;
                }
                orderedRequests.insert(orderedRequests.begin()+insertionPosition, requests[i][j]);
            }
        }
    }

    for(auto it=orderedRequests.begin(); it != orderedRequests.end(); it++){
        if(!forbiddenDestinations[(*it)->getDstID()] && !forbiddenSources[(*it)->getSrcID()]){
            forbiddenDestinations[(*it)->getDstID()] = true;
            forbiddenSources[(*it)->getSrcID()] = true;
            currentServingMatrix[(*it)->getSrcID()][(*it)->getDstID()] = 1;
            (*it)->setLastServedTime(0);
        }
        else{
            (*it)->setLastServedTime((*it)->getLastServedTime()+1);
        }
    }
    orderedRequests.clear();
    return currentServingMatrix;
}

int** RateController::scheduleAndServeOFmsm(){
    bool forbiddenDestinations[numHosts];
    bool forbiddenSources[numHosts];
    int **currentServingMatrix;
    std::vector<RequestInformation*> orderedRequests;

    currentServingMatrix = new int*[numHosts];
    for(int i=0;i<numHosts;i++){
        forbiddenDestinations[i] = false;
        forbiddenSources[i] = false;
        currentServingMatrix[i] = new int[numHosts];
        for(int j=0; j<numHosts; j++){
            currentServingMatrix[i][j] = 0;
        }
    }
    // Create ordered array
    for(int i=0; i< numHosts; i++){
        for(int j=0; j< numHosts; j++){
            int insertionPosition = 0;
            simtime_t currentRequestTime = requests[i][j]->getRequestTime();
            if(currentRequestTime > 0){
                for(auto it = orderedRequests.begin(); it != orderedRequests.end(); it++){
                    if((*it)->getRequestTime() < currentRequestTime)
                        insertionPosition++;
                    else
                        break;
                }
                orderedRequests.insert(orderedRequests.begin()+insertionPosition, requests[i][j]);
            }
        }
    }

    for(auto it=orderedRequests.begin(); it != orderedRequests.end(); it++){
        if(!forbiddenDestinations[(*it)->getDstID()] && !forbiddenSources[(*it)->getSrcID()]){
            forbiddenDestinations[(*it)->getDstID()] = true;
            forbiddenSources[(*it)->getSrcID()] = true;
            currentServingMatrix[(*it)->getSrcID()][(*it)->getDstID()] = 1;
        }
    }
    orderedRequests.clear();
    return currentServingMatrix;
}

int** RateController::scheduleAndServeSRJFmsm(){
    bool forbiddenDestinations[numHosts];
    bool forbiddenSources[numHosts];
    int **currentServingMatrix;
    std::vector<RequestInformation*> orderedRequests;

    currentServingMatrix = new int*[numHosts];
    for(int i=0;i<numHosts;i++){
        forbiddenDestinations[i] = false;
        forbiddenSources[i] = false;
        currentServingMatrix[i] = new int[numHosts];
        for(int j=0; j<numHosts; j++){
            currentServingMatrix[i][j] = 0;
        }
    }
    // Create ordered array
    for(int i=0; i< numHosts; i++){
        for(int j=0; j< numHosts; j++){
            int insertionPosition = 0;
            int currentRequestBytes = requests[i][j]->getResidualBytes();
            if(currentRequestBytes > 0){
                for(auto it = orderedRequests.begin(); it != orderedRequests.end(); it++){
                    if((*it)->getResidualBytes() < currentRequestBytes)
                        insertionPosition++;
                    else
                        break;
                }
                orderedRequests.insert(orderedRequests.begin()+insertionPosition, requests[i][j]);
            }
        }
    }

    for(auto it=orderedRequests.begin(); it != orderedRequests.end(); it++){
        if(!forbiddenDestinations[(*it)->getDstID()] && !forbiddenSources[(*it)->getSrcID()]){
            forbiddenDestinations[(*it)->getDstID()] = true;
            forbiddenSources[(*it)->getSrcID()] = true;
            currentServingMatrix[(*it)->getSrcID()][(*it)->getDstID()] = 1;
        }
    }
    orderedRequests.clear();
    return currentServingMatrix;
}


void RateController::updateRequest(int srcIP, int dstIP, int totalBytes, simtime_t enqueueTime, int usedScheduler){
    Enter_Method("updateRequest(...)");
    //if(totalBytes == 0) return;

    if(moduleIDs.find(srcIP) == moduleIDs.end()) throw cRuntimeError("Failed to find srcIP");
    if(moduleIDs.find(dstIP) == moduleIDs.end()) throw cRuntimeError("Failed to find dstIP");

    int srcID = moduleIDs.find(srcIP)->second;
    int dstID = moduleIDs.find(dstIP)->second;

    //if(requests[srcID][dstID] > 0) throw cRuntimeError("Trying to update an already present request");

    requests[srcID][dstID]->setRequestTime(enqueueTime);
    requests[srcID][dstID]->setResidualBytes(totalBytes);
    // Flowlet ended
    if(totalBytes == 0){
        requests[srcID][dstID]->setLastServedTime(0);
        requests[srcID][dstID]->setAssignedSpine(-1);
        requests[srcID][dstID]->setAssignedRate(0);
    }

    //For RL scheduler only
    if(usedScheduler != AppQueue::RL) return;

    updateRL(srcIP, dstIP);


}

void RateController::updateRL(int srcIP, int dstIP){
    int srcID = moduleIDs.find(srcIP)->second;
    int dstID = moduleIDs.find(dstIP)->second;
    int concurrentConnectionsToDst = 0;
    int concurrentConnectionsFromSrc = 0;
    double*** spineLoad;

    AppQueue *queueToUpdate = check_and_cast<AppQueue*>(getModuleByPath( (char*) (modulePaths.find(srcIP)->second).c_str() ));

    for(int i=0; i<numHosts; i++)
        if(requests[i][dstID]->getResidualBytes() > 0){
            concurrentConnectionsToDst++;
        }

    for(int i=0; i<numHosts; i++)
        if(requests[srcID][i]->getResidualBytes() > 0){
            concurrentConnectionsFromSrc++;
        }

    int newTokenRate = MAX(concurrentConnectionsToDst, concurrentConnectionsFromSrc);


    if((bool) par("maxMinFairness") == true){
        rebuildRM();
        //rebuildRMSRJF();
        normalizeRow(requestMatrix);
        normalizeCol(requestMatrix);
        normalizeRow(requestMatrix);
        normalizeCol(requestMatrix);
        normalizeRow(requestMatrix);
        normalizeCol(requestMatrix);
        normalizeRow(requestMatrix);
        normalizeCol(requestMatrix);
        underAllocateRow(requestMatrix);
        for(int i=0;i<numHosts;i++)
            for(int j=0; j<numHosts;j++){
                assignedRatesVector[i][j].record(requestMatrix[i][j]);
                requests[i][j]->setAssignedRate(requestMatrix[i][j]);
            }
        if((bool) par("pathSelection") == true){

            spineLoad = new double**[numSpines];
            for(int i=0;i < numSpines; i++){
                spineLoad[i] = new double*[numTors];
                for(int j=0;j<numTors; j++){
                    spineLoad[i][j] = new double[numTors];
                    for(int k=0;k<numTors; k++){
                        spineLoad[i][j][k] = 0;
                    }
                }
            }

            for(int i=0;i<numHosts;i++)
                for(int j=0; j<numHosts;j++){
                    int sourceToR = moduleToRs.find(i)->second;
                    int destinationToR = moduleToRs.find(j)->second;
                    if(requests[i][j]->getAssignedRate() > 0 && requests[i][j]->getAssignedSpine() >= 0 && sourceToR != destinationToR)
                        spineLoad[requests[i][j]->getAssignedSpine()][sourceToR][destinationToR] += requests[i][j]->getAssignedRate();
                }

            EV << "Spines loads:\n";
            for(int k=0; k<numSpines; k++){
                EV <<"Spine" << k <<"\n";
                for(int i=0; i<numTors; i++){
                    for(int j=0; j<numTors; j++){
                        EV << spineLoad[k][i][j] << " ";
                    }
                    EV <<"\n";
                }
            }
            // Path selection
            int assignedSpine = -1;

            int sourceToR = moduleToRs.find(srcID)->second;
            int destinationToR = moduleToRs.find(dstID)->second;

            if(requests[srcID][dstID]->getAssignedRate() > 0 && requests[srcID][dstID]->getAssignedSpine() < 0 && sourceToR != destinationToR){
                double* excess = new double();
                assignedSpine = getLessLoadedSpine(sourceToR, destinationToR, spineLoad, requests[srcID][dstID]->getAssignedRate(), excess);
                if(assignedSpine < 0 && sourceToR != destinationToR) throw cRuntimeError("Different tors but no spine assigned");
                requests[srcID][dstID]->setAssignedSpine(assignedSpine);
                //requests[srcID][dstID]->setAssignedRate(requests[srcID][dstID]->getAssignedRate() - (*excess));
                delete excess;
            }


            EV << "Assigned spines:\n";
            for(int i=0; i<numHosts; i++){
                for(int j=0; j<numHosts; j++){
                    EV << requests[i][j]->getAssignedSpine() <<" ";
                }
                EV <<"\n";
            }


            EV << "Load on spines for sTOR=" << sourceToR <<" dTOR="<< destinationToR << " is:\n";
            for(int i=0; i<numSpines; i++){
                EV << spineLoad[i][sourceToR][destinationToR] << " ";
            }
            EV << "\nChosen spine=" <<  assignedSpine <<"\n";


            check_and_cast<AppQueue*>(getModuleByPath( (char*) (modulePaths.find(srcIP)->second).c_str() ))->setPathRL(moduleIDs.find(dstIP)->second, assignedSpine);

            for(int i=0;i < numSpines; i++){
                for(int j=0;j<numTors; j++){
                    delete[] spineLoad[i][j];
                }
                delete[] spineLoad[i];
            }
            delete[] spineLoad;
        }
        for(auto it = modulePaths.begin(); it != modulePaths.end(); ++it ){
            double *perHostRequests = new double[numHosts];
            for(int i=0;i<numHosts;i++)
                perHostRequests[i] = requests[moduleIDs.find(it->first)->second][i]->getAssignedRate();
            check_and_cast<AppQueue*>(getModuleByPath( (char*) (it->second).c_str() ))->updateAllRates(perHostRequests);
            delete[] perHostRequests;
        }

    }
    else{

        if(requests[srcID][dstID]->getResidualBytes() == 0){
            queueToUpdate->setTokenArrivalRate(-1, dstID);
            return;
        }

        int nextPktBytesSize = queueToUpdate->getByteSizeOfHOLPacket(dstID);
        queueToUpdate->setTokenArrivalRate( getTXEndTimeOfAppPkt(nextPktBytesSize + 1) * newTokenRate, dstID);
    }

}

int RateController::getLessLoadedSpine(int sourceToR, int destinationToR, double*** spineLoad, double loadToPlace, double* excess){
    double min=-1;
    int minIndex = -1;
    for(int i=0; i<numSpines; i++){
        int loadToDstTor = 0;
        int loadFromSrcTor = 0;
        int excessR = 0;
        int excessC = 0;

        for(int j=0; j<numTors; j++) loadFromSrcTor += spineLoad[i][sourceToR][j];
        for(int j=0; j<numTors; j++) loadToDstTor += spineLoad[i][j][destinationToR];

        if((loadFromSrcTor + loadToPlace) <= 1){
            if((loadToDstTor + loadToPlace) <= 1){
                return i;
            }
        }

        excessR = loadFromSrcTor + loadToPlace -1;
        excessC = loadToDstTor + loadToPlace -1;
        if(excessR > excessC) (*excess) = excessR;
        else (*excess) = excessC;

        if( (*excess) < min || min == -1){
            min = excessR + excessC;
            minIndex = i;
        }
    }
    return minIndex;
}

void RateController::rebuildRM(){
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            requestMatrix[i][j]=0;
            if(requests[i][j]->getResidualBytes() > 0)
                requestMatrix[i][j]=1;
        }
    }
}

void RateController::rebuildRMSRJF(){
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            requestMatrix[i][j]=requests[i][j]->getResidualBytes();
        }
    }
}

double RateController::minOnRow(double** M, int rowIndex){
    double min = -1;
    for(int i=0; i<numHosts; i++){
        if( (M[i][rowIndex] < min || min < 0) && M[i][rowIndex] != 0){
            min = M[i][rowIndex];
        }
    }
    return min;
}

double RateController::minOnCol(double** M, int colIndex){
    double min = -1;
    for(int i=0; i<numHosts; i++){
        if( (M[colIndex][i] < min || min < 0) && M[colIndex][i] != 0){
            min = M[colIndex][i];
        }
    }
    return min;
}

void RateController::normalizeCol(double** M, int colIndex){
    int sum = 0;
    for(int j=0; j<numHosts; j++){
        sum+=M[j][colIndex];
    }
    if(sum != 0)
        for(int j=0; j<numHosts; j++){
            M[j][colIndex] = M[j][colIndex]/sum;
        }

}

void RateController::normalizeRow(double** M, int rowIndex){

    double sum = 0;
    for(int j=0; j<numHosts; j++){
        sum+=M[rowIndex][j];
    }
    if(sum != 0)
        for(int j=0; j<numHosts; j++){
            M[rowIndex][j] = M[rowIndex][j]/sum;
        }

}

void RateController::normalizeRow(double** M){
    for(int i=0; i<numHosts; i++){
        double sum = 0;
        for(int j=0; j<numHosts; j++){
            sum+=M[i][j];
        }
        if(sum != 0)
            for(int j=0; j<numHosts; j++){
                M[i][j] = M[i][j]/sum;
            }
    }
}

void RateController::normalizeCol(double** M){
    for(int i=0; i<numHosts; i++){
        double sum = 0;
        for(int j=0; j<numHosts; j++){
            sum+=M[j][i];
        }
        if(sum != 0)
            for(int j=0; j<numHosts; j++){
                M[j][i] = M[j][i]/sum;
            }
    }
}

void RateController::registerListener(std::string VOQPath, int VOQIPAddress, int moduleIndex, int torIndex){

    if(modulePaths.find(VOQIPAddress) != modulePaths.end()) return;

    modulePaths.insert(std::make_pair(VOQIPAddress, VOQPath));

    moduleIDs.insert(std::make_pair(VOQIPAddress, moduleIndex));
    moduleIDsReversed.insert(std::make_pair(moduleIndex, VOQIPAddress));
    moduleToRs.insert(std::make_pair(moduleIndex, torIndex));

    for(auto it : moduleToRs){
        if(it.second > (numTors-1)) numTors = (it.second + 1);
    }

}

int RateController::findMaxHostIndex(){
    int maxID = -1;

    for(auto it = moduleIDs.begin(); it != moduleIDs.end(); ++it){
        if(it->second > maxID)
            maxID = it->second;
    }
    return maxID;

}

void RateController::finish(){

    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++) {
            delete requests[i][j];
        }
        delete[] requests[i];
        delete[] requestMatrix[i];
        delete[] assignedRatesVector[i];
    }
    delete[] requests;
    delete[] assignedRatesVector;
    delete[] requestMatrix;
    cancelAndDelete(slotTriggerMessage);

//    for(int i=0;i<numHosts; i++)
//        delete[] requestMatrix[i];
//    delete[] requestMatrix;
}

/*
 * Getters/Setters from here on
 */

int RateController::getAverageFlowSize(){
    return ((int) par("maxSeqLen") + (int) par("minSeqLen"))/2;
}

void RateController::resetRequests(){
    for(int i=0;i<numHosts;i++)
        for(int j=0;j<numHosts;j++){
            requests[i][j]->setSrcID(i);
            requests[i][j]->setDstID(j);
            requests[i][j]->setResidualBytes(0);
            requests[i][j]->setRequestTime(-1);
            requests[i][j]->setLastServedTime(0);
        }

}

void RateController::resetRequest(int srcID, int dstID){
    requests[srcID][dstID]->setResidualBytes(0);
    requests[srcID][dstID]->setRequestTime(-1);
    requests[srcID][dstID]->setLastServedTime(0);

}

simtime_t RateController::getTXEndTimeOfFrame(int frameLengthBytes){
    return (simtime_t) 8 * (frameLengthBytes + ETH_PREAMBLE + ETH_CRC + ETH_IPG) / linkRate;
}

simtime_t RateController::getTXEndTimeOfAppPkt(int appPktLengthBytes){
    return getTXEndTimeOfFrame(appPktLengthBytes + IP_OH + ETH_HEADER);
}

int RateController::getIDFromIP(int IP){
    if(moduleIDs.find(IP) == moduleIDs.end()) throw cRuntimeError("Destination IP address is not registered");

    return moduleIDs.find(IP)->second;
}

void RateController::printCurrentRequestMatrixRequestServed(){
    EV << "Current request matrix service age:\n";
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            EV << requests[i][j]->getLastServedTime() << " ";
            if((j+1)%numTors == 0) EV << " ";
        }
        EV << "\n";
        if((i+1)%numTors == 0) EV << "\n";
    }
}

void RateController::printCurrentRequestMatrixResidualBytes(){
    EV << "Current request matrix bytes:\n";
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            EV << requests[i][j]->getResidualBytes() << " ";
            if((j+1)%numTors == 0) EV << " ";
        }
        EV << "\n";
        if((i+1)%numTors == 0) EV << "\n";
    }
}

void RateController::printCurrentRequestMatrixRequestTime(){
    EV << "Current request matrix request time:\n";
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            EV << requests[i][j]->getRequestTime() << " ";
            if((j+1)%numTors == 0) EV << " ";
        }
        EV << "\n";
        if((i+1)%numTors == 0) EV << "\n";
    }
}

void RateController::printAndDestoryCurrentServingMatrix(int **servingMatrix){
    EV << "Serving this slot:\n";
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            EV << servingMatrix[i][j];
            if((j+1)%(numHosts/numTors) == 0) EV << " ";
        }
        EV << "\n";
        if((i+1)%(numHosts/numTors) == 0) EV << "\n";
        delete[] servingMatrix[i];
    }
    delete[] servingMatrix;

}

void RateController::printCurrentServingMatrix(int **servingMatrix){
    EV << "Serving this slot:\n";
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            EV << servingMatrix[i][j];
            if((j+1)%(numHosts/numTors) == 0) EV << " ";
        }
        EV << "\n";
        if((i+1)%(numHosts/numTors) == 0) EV << "\n";
    }
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            if(servingMatrix[i][j]){
                EV << "Serving " << i << "-" << j <<endl;
            }
        }
    }
}

void RateController::printAndDestroyNonBlockingPaths(int** nonBlockingPaths){
    EV << "Selcted paths:\n";
    for(int i=0; i<numHosts; i++){
        for(int j=0; j<numHosts; j++){
            EV << nonBlockingPaths[i][j];
            if((j+1)%(numHosts/numTors) == 0) EV << " ";
        }
        EV << "\n";
        if((i+1)%(numHosts/numTors) == 0) EV << "\n";
        delete[] nonBlockingPaths[i];
    }
    delete[] nonBlockingPaths;
}

void RateController::updateNumberOfActiveRequests(){
    numberOfActiveRequests = 0;
    for(int i=0;i<numHosts;i++)
        for(int j=0; j<numHosts; j++)
            if(requests[i][j]->getResidualBytes() > 0){
                numberOfActiveRequests++;
            }
}

int** RateController::selectNonBlockingPath(int** currentServingMatrix){
    int torRequests[numTors][numTors];
    int **nonBlockingPaths;
    bool ***colorMatrix;

    nonBlockingPaths = new int*[numHosts];
    for(int i=0;i<numHosts;i++){
        nonBlockingPaths[i] = new int[numHosts];
        for(int j=0; j<numHosts; j++)
            nonBlockingPaths[i][j]=-1;
    }

    if((bool) par("pathSelection") == false) return nonBlockingPaths;

    printCurrentServingMatrix(currentServingMatrix);

    // Alloc
    colorMatrix = new bool**[numTors];
    for(int i=0;i<numTors; i++){
        colorMatrix[i] = new bool*[numTors];
        for(int j=0;j<numTors;j++){
            colorMatrix[i][j] = new bool[colorsPerSpine];
            torRequests[i][j] = 0;
            for(int k=0; k<colorsPerSpine; k++)
                colorMatrix[i][j][k] = false;
        }
    }

    for(int i=0;i<numHosts;i++){
        for(int j=0; j<numHosts; j++){
            int sourceToR = moduleToRs.find(i)->second;
            int destinationToR = moduleToRs.find(j)->second;

            if(currentServingMatrix[i][j] == 0 || sourceToR == destinationToR) continue;

            rearrangePaull(sourceToR, destinationToR, colorMatrix);
        }
    }

    EV << "Rearranegment DONE!!! Current color matrix:\n";
    printCurrentColorMatrix(colorMatrix);

    for(int i=0;i<numHosts;i++){
        for(int j=0; j<numHosts; j++){
            int sourceToR = moduleToRs.find(i)->second;
            int destinationToR = moduleToRs.find(j)->second;

            if(currentServingMatrix[i][j] == 0 || sourceToR == destinationToR){
                continue;
            }

            for(int k=0; k<colorsPerSpine; k++){
                if(colorMatrix[sourceToR][destinationToR][k] == true){
                    nonBlockingPaths[i][j] = k%numSpines;
                    colorMatrix[sourceToR][destinationToR][k] = false;
                    break;
                }
            }

        }
    }

    // Dealloc
    for(int i=0;i<numTors; i++){
        for(int j=0;j<numTors;j++)
            delete[] colorMatrix[i][j];
        delete[] colorMatrix[i];
    }
    delete[] colorMatrix;

    return nonBlockingPaths;
}

bool RateController::rearrangePaull(int sourceTor, int destinationTor, bool*** colorMatrix){

    bool ST_FC[colorsPerSpine];
    bool DT_FC[colorsPerSpine];
    bool C_ST_FC[colorsPerSpine];
    int ST = sourceTor;
    int DT = destinationTor;

    EV << "Starting Placement in:\n.";
    EV << "ST: " << ST <<"\n";
    EV << "DT: " << DT <<"\n";
    EV<< "Current color matrix:\n";
    printCurrentColorMatrix(colorMatrix);
    for(int k=0; k<colorsPerSpine; k++){
        ST_FC[k] = false;
        DT_FC[k] = false;
        C_ST_FC[k] = false;
    }
    // Get already used colors
    for(int i=0; i<numTors; i++){
        for(int k=0; k<colorsPerSpine; k++){
            if(colorMatrix[ST][i][k] == true) ST_FC[k] = true;
            if(colorMatrix[i][DT][k] == true) DT_FC[k] = true;
        }
    }

    // Find common free color place and return if found
    int CToPlace = getCommonFreeColors(ST_FC, DT_FC, -1);
    if(CToPlace >= 0){
        colorMatrix[ST][DT][CToPlace] = true;
        return true;
    }
    // No common color found. Get first free color on the row
    for(int k=0; k<colorsPerSpine; k++) if(ST_FC[k] == false){ CToPlace = k; break;}
    if(CToPlace < 0) throw cRuntimeError("Failed to find a free color on the row. It should not occur.");

    // Find conflicting source for color CToPlace
    int C_ST = getConflictOnColumn(colorMatrix, DT, CToPlace, -1);
    if(C_ST < 0) throw cRuntimeError("Failed to find a a conflicting source tor");

    // Place color and remove the conflicting one
    colorMatrix[ST][DT][CToPlace] = true; DT_FC[CToPlace] = true;
    colorMatrix[C_ST][DT][CToPlace] = false;

    /*
     * Start to check colors on conflicting source
     */

    // Find free colors on C_ST
    for(int i=0; i<numTors; i++){
        for(int k=0; k<colorsPerSpine; k++){
            if(colorMatrix[i][C_ST][k] == true){ C_ST_FC[k] = true; break; }
        }
    }

    int CToRearrange = -1;
    for(int k=0; k<colorsPerSpine; k++) if(DT_FC[k] == false && k != CToPlace){ CToRearrange = k; break;}
    // No free color on C_ST. Pick a random one.
    if(CToRearrange < 0) throw cRuntimeError("More than 4 colors in column");

    int C_DT = getConflictOnRow(colorMatrix, C_ST, CToRearrange, -1);

    if(C_DT < 0){
        colorMatrix[C_ST][DT][CToRearrange] = true;
        return true;
    }
    colorMatrix[C_ST][DT][CToRearrange] = true;
    colorMatrix[C_ST][C_DT][CToRearrange] = false;
    colorMatrix[C_ST][C_DT][CToPlace] = true;

    EV << "Second iteration done:\n";
    EV << "ST: " << ST <<"\n";
    EV << "DT: " << DT <<"\n";
    EV << "CToRearrange: " << CToRearrange <<"\n";
    EV << "CToPlace: " << CToPlace <<"\n";
    EV << "C_ST: " << C_ST <<"\n";
    EV << "C_DT: " << C_DT <<"\n";
    EV << "\n";
    printCurrentColorMatrix(colorMatrix);

    // Find conflicting destination for color CToRearrange
    int numIterations = 0;
    while(true){
        DT = C_DT;
        ST = C_ST;
        EV << "\n";
        EV << "Doing subsequent iterations with:\n";
        EV << "ST: " << ST <<"\n";
        EV << "DT: " << DT <<"\n";
        EV << "CToRearrange: " << CToRearrange <<"\n";
        EV << "CToPlace: " << CToPlace <<"\n";

        colorMatrix[ST][DT][CToRearrange] = true;
        colorMatrix[ST][DT][CToPlace] = false;

        C_ST = getConflictOnColumn(colorMatrix, DT, CToPlace, ST);
        EV << "C_ST: " << C_ST <<"\n";

        if(C_ST < 0) {
            return true;
        }

        colorMatrix[C_ST][DT][CToRearrange] = true;
        colorMatrix[C_ST][DT][CToPlace] = false;

        int C_DT = getConflictOnRow(colorMatrix, C_ST, CToRearrange, DT);
        EV << "C_DT: " << C_DT <<"\n";

        if(C_DT < 0){
            return true;
        }

        colorMatrix[C_ST][C_DT][CToRearrange] = false;
        colorMatrix[C_ST][C_DT][CToPlace] = true;


        printCurrentColorMatrix(colorMatrix);

        if(numIterations++ > colorsPerSpine*colorsPerSpine) throw cRuntimeError("oopsie!!");

        int cTemp = CToRearrange;
        CToRearrange = CToPlace;
        CToPlace = cTemp;
    }
}

int RateController::getCommonFreeColors(bool* freeColors1, bool* freeColors2, int exclusionColor){
    for(int k=0; k<colorsPerSpine; k++){
        if(freeColors1[k] == false && freeColors2[k] == false && k != exclusionColor){
            return k;
        }
    }
    return -1;
}

int RateController::getConflictOnColumn(bool*** colorMatrix, int column, int color, int exclusion){
    for(int i=0; i<numTors; i++){
        if(colorMatrix[i][column][color] == true && i != exclusion){
            return i;
        }
    }
    return -1;
}

int RateController::getConflictOnRow(bool*** colorMatrix, int row, int color, int exclusion){
    for(int i=0; i<numTors; i++){
        if(colorMatrix[row][i][color] == true && i != exclusion){
            return i;
        }
    }
    return -1;
}

void RateController::printCurrentColorMatrix(bool*** colorMatrix){
    for(int i=0; i<numTors; i++){
        for(int j=0; j<numTors; j++){
            EV << "{";
            for(int k=0; k<colorsPerSpine; k++){
                if(colorMatrix[i][j][k])
                    EV << k << " ";
            }
            EV << "}\t";
        }
        EV << "\n";
    }
    EV << "\n";
}

void RateController::underAllocateRow(double** M){
    for(int i=0; i<numHosts; i++){
        double tSum=0;
        for(int j=0; j<numHosts; j++){
            tSum += M[i][j];
        }
        if(tSum > 1){
            for(int j=0; j<numHosts; j++){
                M[i][j] = M[i][j]/tSum;
            }
        }
    }

}

double RateController::getAccumulatedBytesWithRate(double rate, simtime_t time){
    return (linkRate*rate/8*time).dbl();

}
