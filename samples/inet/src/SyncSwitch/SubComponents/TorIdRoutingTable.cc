/*
 * TorIdRoutingTable.cpp
 *
 *  Created on: Aug 8, 2019
 *      Author: alessandrocornacchia
 */

#include "TorIdRoutingTable.h"
#include <stdlib.h>     /* srand, rand */

void TorIdRoutingTable::addRoute(int torId, int outInterface, int priority) {
    if(torId < 0 || outInterface < 0) {
        throw cRuntimeError("AddRoute: Invalid Id");
    }
    auto result = this->routes[torId].insert(make_pair(priority, vector<int>({outInterface})));
    // if priority already present, add another route with same priority
    if(!result.second) {
       result.first->second.push_back(outInterface);
    }
    routeExist[torId] = true;
}

int TorIdRoutingTable::getNumRoutesOfPriority(int torId, int priority) {
    if(routeExist.size() <= torId || routeExist[torId] == false) {
        return 0;
    }
    return this->routes[torId][priority].size();
}

int TorIdRoutingTable::getRouteOfPriority(int torId, int priority, int routeIdx) {
    if(routeIdx == RANDOM_ROUTE_IDX) {   // extract random index
        routeIdx = rand() % getNumRoutesOfPriority(torId, priority);
    }
    return this->routes[torId][priority][routeIdx];
}

void TorIdRoutingTable::setSize(int numEntries) {
    routes.resize(numEntries);
    routeExist.resize(numEntries, false);
}

