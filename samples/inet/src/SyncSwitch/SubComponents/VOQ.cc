//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "VOQ.h"
#include "PFCController.h"
#include "inet/linklayer/common/MACAddress.h"
#include "inet/networklayer/ipv4/IPv4Datagram.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "FatTreeIPv4NetworkConfigurator.h"

Define_Module(VOQ);

int VOQ::numInitStages() const{
    return 10;
}

void VOQ::initialize(int stage)
{
    if(stage != 9) return;
    if(getModuleByPath("^.^.eth[0]") == nullptr) throw cRuntimeError("Impossible to find number of interfaces.");

    numInputQueues = getModuleByPath("^.^.eth[0]")->getVectorSize();

    if(numInputQueues == 0) numInputQueues = 1;

    numPriorities = (int) par("numPriorities");
    iqFrameCapacity = (int) par("iqFrameCapacity");

    if((bool) par("pfcEnabled") == true)
        pfcEnabled = true;
    else
        pfcEnabled = false;

    if(pfcEnabled && dynamic_cast<PFCController *>(getModuleByPath(par("pfcControllerPath"))) == nullptr) throw cRuntimeError("PFC Controller not found");
        pfcController = check_and_cast<PFCController *>(getModuleByPath(par("pfcControllerPath")));

    // XXX Alessandro
    viQueues = new cQueue*[numInputQueues];
    queueStatus = new int*[numInputQueues];

    //viQueues = new cQueue[numPriorities];
    //queueStatus = new int[numPriorities];

    pauseMessages = new cMessage**[numInputQueues];
    queueOccupancyVector = new cOutVector*[numInputQueues];
    queueOccupancyStatistics = new cStdDev*[numInputQueues];

    for(int i = 0; i< numInputQueues; i++){
        // XXX Alessandro
        viQueues[i] = new cQueue[numPriorities];
        queueStatus[i] = new int[numPriorities];
        pauseMessages[i] = new cMessage*[numPriorities];
        queueOccupancyVector[i] = new cOutVector[numPriorities];
        queueOccupancyStatistics[i] = new cStdDev[numPriorities];

    }

    for(int i = 0; i < numInputQueues; i++)
        for(int j = 0; j < numPriorities; j++){
            // XXX Alessandro
            queueStatus[i][j] = OPERATIONAL;
            //queueStatus[j] = OPERATIONAL;
            pauseMessages[i][j] = new cMessage();
            pauseMessages[i][j]->setKind(PAUSE_EVENT);

            VOQPauseControlInfo *pauseControlInfo = new VOQPauseControlInfo();
            pauseControlInfo->setSourcePort(i);
            pauseControlInfo->setPriority(j);
            pauseMessages[i][j]->setControlInfo(pauseControlInfo);

            char name[100];

            sprintf(name, "I%dP%d", i, j);
            viQueues[i][j].setName(name);
            //viQueues[j].setName(name);

            sprintf(name, "QOCC-I%dP%d", i, j);
            queueOccupancyVector[i][j].setName(name);

            sprintf(name, "QOCCstats-I%dP%d", i, j);
            queueOccupancyStatistics[i][j].setName(name);


        }
    //totalQueueOccupancy.setName("ToitalQueueOccupancy");

    /*
     * Manage ECMP discovery.   XXX Alessandro NO DISCOVERY

    cMessage* discoveryMessageEvent = new cMessage();
    discoveryMessageEvent->setKind(DISCOVERY);
    if((double) par("discoveryFrameTxTime") >= 0)
        scheduleAt((double) par("discoveryFrameTxTime"), discoveryMessageEvent);
    */

    /*
     * Setup scheduler
     */
    usedScheduler = RANDOM; // Default

    /*
     * Watchers
     */
    WATCH(numInputQueues);
    WATCH(packetsRequestedFromUs);
    WATCH(usedScheduler);

   // if (stage == INITSTAGE_NETWORK_LAYER) {
        FatTreeIPv4NetworkConfigurator* confg;
        if((confg = check_and_cast<FatTreeIPv4NetworkConfigurator*>(getModuleByPath("configurator"))) == nullptr){
            throw cRuntimeError("NetworkConfigurator path invalid or not found");
        }
        cModule* pfcSwitch = getParentModule()->getParentModule();
        set<int>& priorities = const_cast<set<int>&>(confg->getSwitchPriorities(pfcSwitch->getIndex(), pfcSwitch->par("switchType").stdstringValue()));
        WATCH_SET(priorities);
  //  }

}

void VOQ::finish(){
    for(int i = 0; i< numInputQueues; i++){
        // XXX Alessandro
        delete[] viQueues[i];
        for(int j=0; j<numPriorities; j++){
            queueOccupancyStatistics[i][j].record();
            delete pauseMessages[i][j];
        }
        delete[] viQueues;
        // XXX
        delete[] queueStatus[i];
        delete[] queueStatus;
        delete[] pauseMessages[i];
        delete[] queueOccupancyVector[i];
        delete[] queueOccupancyStatistics[i];
    }
    delete[] queueStatus;
    delete[] viQueues;
    delete[] queueOccupancyVector;
    delete[] queueOccupancyStatistics;

}

void VOQ::handleMessage(cMessage *msg)
{
    if(msg->isSelfMessage())
        manageSelfMessages(msg);
    else
        manageFrameArrivals(msg);

}

void VOQ::manageSelfMessages(cMessage *msg){
    if(msg->getKind() == PAUSE_EVENT){
        VOQPauseControlInfo *pauseControlInfo = check_and_cast<VOQPauseControlInfo*>(msg->getControlInfo());
        unpauseQueue(pauseControlInfo->getSourcePort(), pauseControlInfo->getPriority());
    }
    else if(msg->getKind() == DISCOVERY){
        sendDiscoveryMessage();
        delete msg;
    }
}

void VOQ::manageFrameArrivals(cMessage *msg){
    int pqIndex = (int) par("defaultPQIndex");
    int iqIndex = 0;    // Input Queue index

    if(dynamic_cast<EtherFrameCoS *>(msg) == nullptr) return;

    EtherFrameCoS *frame = check_and_cast<EtherFrameCoS *>(msg);
    // Sure it is in the range supported by the switch (support for spatial diversity priority scheduler)
    pqIndex = frame->getPcp() % this->numPriorities;
    iqIndex = frame->getArrivalPort();
    // Overriden logic here
    iqIndex = 0;
    //
    if(frame->getIsPriorityPausePacket())
        viQueues[iqIndex][pqIndex].insert(frame);
        //viQueues[pqIndex].insert(frame);
    else{
        if(pfcEnabled == true){
            if(dynamic_cast<PFCController*>(pfcController) == nullptr) throw cRuntimeError("Cannot cast pfcController to the right class");
            pqIndex = check_and_cast<PFCController *>(pfcController)->notifyPacketArrival(frame);
        }

        if(pqIndex < 0) delete msg;

        viQueues[iqIndex][pqIndex].insert(frame);
        //viQueues[pqIndex].insert(frame);
    }

    // XXX Alessandro
    queueOccupancyVector[iqIndex][pqIndex].record(viQueues[iqIndex][pqIndex].getLength());
    queueOccupancyStatistics[iqIndex][pqIndex].collect(viQueues[iqIndex][pqIndex].getLength());
    //queueOccupancyVector[iqIndex][pqIndex].record(viQueues[pqIndex].getLength());
    //queueOccupancyVector[iqIndex][pqIndex].record(viQueues[pqIndex].getLength());
    int totQOCC = 0;    // total queue occupancy
    for(int i = 0; i<numInputQueues; i++)
        for(int j=0; j< numPriorities; j++)
            totQOCC += viQueues[i][j].getLength();
            //totQOCC += viQueues[j].getLength();
    //totalQueueOccupancy.record(totQOCC);

    if(packetsRequestedFromUs > 0){
        sendOut(iqIndex, pqIndex);
    }


}

void VOQ::requestPacket(){
    Enter_Method_Silent();

    if(packetsRequestedFromUs == 0)
        packetsRequestedFromUs++;

    serveQueueRandomScheduler();

}

/*
 *
 * MISC
 *
 */


void VOQ::generatePausePacket(int pqIndex, double timeToStop){
    //TODO
}

int VOQ::getFrameCapacityPerInputPriority(int iqIndex, int pqIndex){
    //TODO: finish in order to make it more versative
    return iqFrameCapacity;
}

int VOQ::getFrameOccupancyPerInputPriority(int iqIndex, int pqIndex){
    // XXX Alessandro
    return viQueues[iqIndex][pqIndex].getLength();
    //return viQueues[pqIndex].getLength();
}

void VOQ::pauseQueue(int iqIndex, int pqIndex, double timeToPause){
    // XXX Alessandro
    queueStatus[iqIndex][pqIndex] = PAUSED;
    //queueStatus[pqIndex] = PAUSED;
    cancelEvent(pauseMessages[iqIndex][pqIndex]);
    scheduleAt(simTime() + timeToPause, pauseMessages[iqIndex][pqIndex]);
}

void VOQ::unpauseQueue(int iqIndex, int pqIndex){
    // XXX Alessandro
    queueStatus[iqIndex][pqIndex] = OPERATIONAL;
    //queueStatus[pqIndex] = OPERATIONAL;
    cancelEvent(pauseMessages[iqIndex][pqIndex]);
}

void VOQ::sendOut(int iqIndex, int pqIndex){
    // XXX Alessandro
    if(viQueues[iqIndex][pqIndex].getLength() == 0) throw cRuntimeError("Trying to pop from an empty queue");
    //if(viQueues[pqIndex].getLength() == 0) throw cRuntimeError("Trying to pop from an empty queue");

    // XXX Alessandro
    send(check_and_cast<cMessage* >(viQueues[iqIndex][pqIndex].pop()), "out");
    //send(check_and_cast<cMessage* >(viQueues[pqIndex].pop()), "out");
    queueOccupancyVector[iqIndex][pqIndex].record(viQueues[iqIndex][pqIndex].getLength());
    //queueOccupancyVector[iqIndex][pqIndex].record(viQueues[pqIndex].getLength());
    queueOccupancyStatistics[iqIndex][pqIndex].collect(viQueues[iqIndex][pqIndex].getLength());
    //queueOccupancyStatistics[iqIndex][pqIndex].collect(viQueues[pqIndex].getLength());

    packetsRequestedFromUs--;

}

// among the non empty queues, exctract an index at random
int VOQ::getRandomIndexToServe(bool* nonEmptyQueues){
    int queueToServe = 0;
    int totalNonEmptyQueues = 0;
    int j=0;

    for(int i=0; i<numInputQueues; i++){
        if(nonEmptyQueues[i] == true)
            totalNonEmptyQueues++;
    }

    queueToServe=intuniform(0, totalNonEmptyQueues-1);

    for(int i=0; i<numInputQueues; i++){
        if(nonEmptyQueues[i] == true){
            if(j == queueToServe){
                return i;
            }
            j++;
        }
    }
    return -1;
}

// serve a random queue among those non-empty at a given priority. Low priority IDs are served first
bool VOQ::serveQueueRandomScheduler(){

    bool *nonEmptyQueues = new bool[numInputQueues];
    int priority = getNonEmptyQueues(nonEmptyQueues);
//    int priority = -1;
//    for(int p=0; p < numPriorities; p++){
//        if(!viQueues[p].isEmpty()) {
//           priority = p;
//           break;
//        }
//    }
//    if(priority == -1) return false;
//    sendOut(priority, priority);
//    // end changes

    if(nonEmptyQueues == nullptr) return false;
    if(priority == -1) return false;

    for(int i=0;i<numInputQueues;i++){
        if(nonEmptyQueues[i])
            EV << " 1";
        else
            EV << " 0";
    }
    EV << "\n";

    int queueToServe = getRandomIndexToServe(nonEmptyQueues);
    EV << "Queue to serve=" << queueToServe <<"\n";
    if(queueToServe < 0) throw cRuntimeError("Error during random queue selection");

    sendOut(queueToServe, priority);

    delete[] nonEmptyQueues;
    return true;
}

// Finds non-empty queues, marking them in nonEmptyQueues vector. Scanning in order of priority rturns the first
//priority that has a queue with at least one packet to serve (non empty)
int VOQ::getNonEmptyQueues(bool* nonEmptyQueues){
    bool nonEmptyQueueFound = false;
    int priorityToServe = -1;

    for(int i=0; i<numInputQueues; i++)
        nonEmptyQueues[i]=false;

    for(int priority=0; priority < numPriorities; priority++){
         for(int inputQueue=0; inputQueue < numInputQueues; inputQueue++){
            //XXX Alessandro
            nonEmptyQueues[inputQueue] = !viQueues[inputQueue][priority].isEmpty();
            if(nonEmptyQueues[inputQueue] == true)
                nonEmptyQueueFound = true;
        }
        if(nonEmptyQueueFound == true){
            priorityToServe=priority;
            break;
        }
    }

    for(int i=0;i<numInputQueues;i++){
        if(nonEmptyQueues[i])
            EV << " 1";
        else
            EV << " 0";
    }
    EV << "\n";

    if(nonEmptyQueueFound == false){
        delete nonEmptyQueues;
        nonEmptyQueues = nullptr;
        return -1;
    }

    return priorityToServe;
}

int VOQ::getNumPendingRequests(){
    return 0;
}

int VOQ::getTotalQueueSize(){
    int size = 0;

    // XXX Alessandro
    for(int i=0; i < numInputQueues; i++)
        for(int j = 0; j< numPriorities; j++)
            size += viQueues[i][j].getLength();
            //size += viQueues[j].getLength();

    return size;
}

bool VOQ::isEmpty(){
    for(int i=0; i < numInputQueues; i++){
        for(int j = 0; j< numPriorities; j++){
            if(viQueues[i][j].isEmpty() == false)
            //if(viQueues[j].isEmpty() == false)
                return false;
        }
    }
    return true;
}

void VOQ::clear(){
    return;
}

cMessage* VOQ::pop(){
    return nullptr;
}

void VOQ::addListener(IPassiveQueueListener *listener){
    return;
}

void VOQ::removeListener(IPassiveQueueListener *listener){
    return;
}

void VOQ::sendDiscoveryMessage(){
    EtherFrameCoS* discoverFrame = new EtherFrameCoS();
    discoverFrame->setName("DISCOVER");
    discoverFrame->setDest(MACAddress::BROADCAST_ADDRESS);
    discoverFrame->setPcp(0);
    discoverFrame->setByteLength(200);
    discoverFrame->setEtherType(ETHERTYPE_INET_GENERIC);
    send(discoverFrame, "out");
}
