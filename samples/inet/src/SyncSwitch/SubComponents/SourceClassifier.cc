//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "SourceClassifier.h"

Define_Module(SourceClassifier);

void SourceClassifier::initialize()
{
    // TODO - Generated method body
}

void SourceClassifier::handleMessage(cMessage *msg)
{
    EtherFrame *message = check_and_cast<EtherFrame *>(msg);
    if(dynamic_cast<PriorityPauseFrame *>(msg) != nullptr){
        PriorityPauseFrame *ppFrame = check_and_cast<PriorityPauseFrame *>(msg);
        send(ppFrame, "out", ppFrame->getPausePort());
        return;
    }

    if(dynamic_cast<EtherFrameCoS *>(msg) != nullptr){
        EtherFrameCoS *message = check_and_cast<EtherFrameCoS *>(msg);
        send(msg,"out", message->getArrivalPort());
    }
    else
        send(msg, "out", (int) par("defaultQueue"));
}
