//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TcpFlowCounters.h"

#include <iostream>
#include <string>
#include <bitset>
#include <algorithm>

using namespace tcp;

long TcpFlowCounters::getFlowBytes(IPv4Datagram const *msg){
    int key = computeKeyFromPacket(msg);
    if(byteSent.count(key)) {   // Flow already present
        return byteSent[key].first;
    }
    return 0;
}

int TcpFlowCounters::computeKeyFromPacket(IPv4Datagram const *datagram) {
    int key = datagram->getSrcAddress().getInt() +
              datagram->getDestAddress().getInt() +
              datagram->getTransportProtocol();

    if(datagram->getTransportProtocol() != IP_PROT_TCP) {
        throw cRuntimeError("Protocol not supported");
    }

    TCPSegment* segment = check_and_cast<TCPSegment*>(datagram->getEncapsulatedPacket());
    key += segment->getSourcePort() +
           segment->getDestinationPort();

      /*  // source and destination port concatenation
            int sp,dp = 0;
           sp = segment->getSourcePort();
            dp = segment->getDestinationPort();
           int portConcat = dp + (sp<<16);
           string ssP = bitset<32>( sp ).to_string();
        string sdP = bitset<32>( dp ).to_string();

        string sportConcat = bitset<32>( portConcat ).to_string();

        key += portConcat;  // XOR with IP fields */

    return key;
}

void TcpFlowCounters::notifyPacketArrival(IPv4Datagram const *msg) {
    int key = computeKeyFromPacket(msg);
    int L4bytes = msg->getEncapsulatedPacket()->getByteLength();

    if(byteSent.count(key)) {   // Flow already present, update entry
        byteSent[key].first = byteSent[key].first + L4bytes;
        byteSent[key].second = simTime(); //msg->getTimestamp();
    } else {
        if(isSyn(msg)) {            // If SYN add flow
            byteSent.insert(make_pair(key, make_pair(L4bytes,msg->getTimestamp())));
        } else throw cRuntimeError("Non SYN packet rx, but flow never seen");
    }
    checkAndFlush();
}

void TcpFlowCounters::checkAndFlush(){
    flushCounter--;
    if(flushCounter == 0) {
        flushCounter = 1000;                        // every 1000 packets
        for(auto it = byteSent.begin(), ite = byteSent.end(); it != ite;) {
            if(simTime() - it->second.second > 5){  // 1sec aged
                it = byteSent.erase(it);
            }
            else {
                ++it;
            }
        }
    }
}

bool TcpFlowCounters::isSyn(IPv4Datagram const* datagram) {
    if(datagram!=nullptr) {
        TCPSegment* segment = dynamic_cast<TCPSegment*>(datagram->getEncapsulatedPacket());
        if(segment != nullptr) {
            return segment->getSynBit();
        }
    }
    return false;
}
