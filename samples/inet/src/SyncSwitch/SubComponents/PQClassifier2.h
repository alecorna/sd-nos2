//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __SYNCNET_PQCLASSIFIER2_H_
#define __SYNCNET_PQCLASSIFIER2_H_

#include "PQClassifier.h"

using namespace inet;

/**
 * Extension of PQClassifier where classification is taken from stack of priority labels
 */
class PQClassifier2 : public PQClassifier, public IQueueAccess
{
    protected:
        int numPriorities;
        std::set<IQueueAccess *> outQueueSet;    // set of out queues; comparing pointers is ok

    protected:
        virtual void initialize() override;
        virtual int classify(cMessage *msg) override;

    public:
        virtual int getLength() const override;
        virtual int getByteLength() const override;
};

#endif
