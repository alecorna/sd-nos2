//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "EtherEncapCos.h"
#include "inet/common/INETUtils.h"
#include "inet/common/ModuleAccess.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "inet/networklayer/contract/IInterfaceTable.h"
#include "inet/linklayer/common/Ieee802Ctrl.h"
#include "inet/transportlayer/udp/UDPPacket.h"

Define_Module(EtherEncapCos);

#define IP_PROT_ROCE 19

void EtherEncapCos::initialize(int stage) {
    EtherEncap::initialize(stage);
    if(INITSTAGE_NETWORK_LAYER_3==stage) {
        this->controller = check_and_cast<RoutingController*>(getModuleByPath(getAncestorPar("routingControllerPath")));
    }
};

void EtherEncapCos::processPacketFromHigherLayer(cPacket *msg)
{
    if (msg->getByteLength() > MAX_ETHERNET_DATA_BYTES)
        throw cRuntimeError("packet from higher layer (%d bytes) exceeds maximum Ethernet payload length (%d)", (int)msg->getByteLength(), MAX_ETHERNET_DATA_BYTES);

    totalFromHigherLayer++;
    emit(encapPkSignal, msg);

    // Creates MAC header information and encapsulates received higher layer data
    // with this information and transmits resultant frame to lower layer

    // create Ethernet frame, fill it in from Ieee802Ctrl and encapsulate msg in it
    EV_DETAIL << "Encapsulating higher layer packet `" << msg->getName() << "' with COS for MAC\n";

    IMACProtocolControlInfo *controlInfo = check_and_cast<IMACProtocolControlInfo *>(msg->removeControlInfo());
    Ieee802Ctrl *etherctrl = dynamic_cast<Ieee802Ctrl *>(controlInfo);
    //Ieee802CtrlCoS *etherctrl = dynamic_cast<Ieee802CtrlCoS *>(controlInfo);
    EtherFrame *frame = nullptr;

    if (useSNAP) {
        EtherFrameWithSNAP *snapFrame = new EtherFrameWithSNAP(msg->getName());

        snapFrame->setSrc(controlInfo->getSourceAddress());    // if blank, will be filled in by MAC
        snapFrame->setDest(controlInfo->getDestinationAddress());
        snapFrame->setOrgCode(0);
        if (etherctrl)
            snapFrame->setLocalcode(etherctrl->getEtherType());
        frame = snapFrame;
    }
    else {
        EV_DETAIL << "Encapsulating with EtherFrameCoS\n";
        EtherFrameCoS *eth2Frame = new EtherFrameCoS(msg->getName());

        eth2Frame->setSrc(controlInfo->getSourceAddress());    // if blank, will be filled in by MAC
        eth2Frame->setDest(controlInfo->getDestinationAddress());

        if (etherctrl) {
            eth2Frame->setEtherType(etherctrl->getEtherType());
        }

        RoutingInfoCoS routingInfo = controller->requestRoutingInformation(msg);
        eth2Frame->setOutPortStack(routingInfo.outPorts); // add label stack to pkt
        eth2Frame->setPriorities(routingInfo.priorities);  // add priority
        eth2Frame->setPcp(routingInfo.desiredPriority); // add ideal priority

        frame = eth2Frame;

    }
    delete controlInfo;

    ASSERT(frame->getByteLength() > 0); // length comes from msg file

    frame->encapsulate(msg);
    if (frame->getByteLength() < MIN_ETHERNET_FRAME_BYTES)
        frame->setByteLength(MIN_ETHERNET_FRAME_BYTES + ETHER_8021Q_HEADER_LENGTH); // "padding"

    EV_INFO << "Sending " << frame << " to lower layer.\n";
    send(frame, "lowerLayerOut");
}

void EtherEncapCos::processFrameFromMAC(EtherFrame *frame)
{
    if (dynamic_cast<EtherFrameCoS *>(frame) != nullptr){
        if(((EtherFrameCoS *)frame)->getEtherType() == ETHERTYPE_INET_GENERIC){
            delete frame;
            return;
        }
    }

    // decapsulate and attach control info
    cPacket *higherlayermsg = frame->decapsulate();

    // add Ieee802Ctrl to packet
    Ieee802Ctrl *etherctrl = new Ieee802Ctrl();
    etherctrl->setSrc(frame->getSrc());
    etherctrl->setDest(frame->getDest());
    etherctrl->setInterfaceId(interfaceId);
    if (dynamic_cast<EtherFrameCoS *>(frame) != nullptr)
        etherctrl->setEtherType(((EtherFrameCoS *)frame)->getEtherType());
    else if (dynamic_cast<EtherFrameWithSNAP *>(frame) != nullptr)
        etherctrl->setEtherType(((EtherFrameWithSNAP *)frame)->getLocalcode());
    higherlayermsg->setControlInfo(etherctrl);

    EV_DETAIL << "Decapsulating frame `" << frame->getName() << "', passing up contained packet `"
            << higherlayermsg->getName() << "' to higher layer\n";

    totalFromMAC++;
    emit(decapPkSignal, higherlayermsg);

    // pass up to higher layers.
    EV_INFO << "Sending " << higherlayermsg << " to upper layer.\n";
    send(higherlayermsg, "upperLayerOut");
    delete frame;
}

