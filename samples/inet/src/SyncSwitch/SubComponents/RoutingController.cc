//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "RoutingController.h"
#include "RdmaApp/RDMAPacket_m.h"
#include "inet/transportlayer/contract/ITransportPacket.h"
#include "inet/transportlayer/udp/UDPPacket.h"
#include "inet/transportlayer/tcp_common/TCPSegment.h"
#include "inet/transportlayer/tcp/TCPConnection.h"
#include "inet/applications/tcpapp/GenericAppMsg_m.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
#include <set>
#include <typeinfo>

using namespace omnetpp;
using namespace inet;
using namespace tcp;

Define_Module(RoutingController);

void RoutingController::finish(){};

void RoutingController::routingLookup(Node* currentSwitch, Node* destTor, IPv4Datagram* datagram, RoutingInfoCoS* ri) {
    // last step of recursion stack
    // we are on the destination ToR, push the proper interface label
    if(currentSwitch == destTor) {

        int outIf = this->getInterfaceToHost(datagram->getDestAddress());
        ri->outPorts.push(outIf);
        FatTreeInterfaceInfo* iface = check_and_cast<FatTreeInterfaceInfo*>(currentSwitch->interfaceInfos[outIf]);
        this->fillPriorityAtInterface(iface, ri);
        return;

    } else {

        int outIf = forward(datagram, currentSwitch, destTor, ri);
        Node* nextHop = (Node*)currentSwitch->getLinkOut(outIf)->getRemoteNode();
        /* DEBUG */
        int outIfcheck = currentSwitch->getLinkOut(outIf)->getLocalGate()->getIndex();
        ASSERT(outIfcheck == outIf);
        /***/

        routingLookup(nextHop, destTor, datagram, ri); // recursive call

        // KEEP here otherwise recursion is not going to work
        ri->outPorts.push(outIf);
        FatTreeInterfaceInfo* iface = check_and_cast<FatTreeInterfaceInfo*>(currentSwitch->interfaceInfos[outIf]);
        this->fillPriorityAtInterface(iface, ri);
    }
}

// if we have less physical queues available than announced priorities, it is a typical case
// of "downrouting problem" where we must mix priorities. 'passive-to-priority' parameter set to
// false means that on this interface has been explicitly requested to reassign locally the priority
// so we ask to controller
void RoutingController::fillPriorityAtInterface(FatTreeInterfaceInfo* iface, RoutingInfoCoS* ri) {
    if (iface->queues.size() < iface->priorities.size() || iface->passive_to_priority == false) {
        ri->priorities.push(thCtrl->getPacketPriority(ri->transmittedBytes, ri->packetSize, iface->queues.size()));
    } else {
        ri->priorities.push(ri->desiredPriority);
    }
}

RoutingInfoCoS RoutingController::requestRoutingInformation(cPacket* msg) {
    Enter_Method("route(...)");

    RoutingInfoCoS routeInfo;

    IPv4Datagram *datagram = check_and_cast<IPv4Datagram*>(msg);

    switch(datagram->getTransportProtocol()) {
       case(IP_PROT_UDP): {
           cPacket* pk = datagram->getEncapsulatedPacket()->getEncapsulatedPacket();
           // This should be an interface instead of specific class..... TODO
           ApplicationPacket* appPk = dynamic_cast<ApplicationPacket*>(pk);
           if (appPk == nullptr) {
               throw cRuntimeError("Not yet implemented");
           }
           routeInfo.transmittedBytes = appPk->getSequenceNumber();
           routeInfo.packetSize = appPk->getByteLength();
           routeInfo.desiredPriority = (uint8_t)thCtrl->getPacketPriority(routeInfo.transmittedBytes, appPk->getByteLength(), numPriorities);
           break;
       }
       case IP_PROT_TCP: {
           TCPSegment* segment = dynamic_cast<TCPSegment*>(datagram->getEncapsulatedPacket());
           TCPFlowVariables* flowStateVariables = (TCPFlowVariables*)segment->getContextPointer();

           routeInfo.packetSize = segment->getPayloadLength();

           // classification of the flow done depending on flow length (known in advance)
           if(staticClassification) {
               routeInfo.transmittedBytes = flowStateVariables->flowLength;
               // priority assigned at the beginning of the flow
               routeInfo.desiredPriority = (uint8_t)thCtrl->getFlowPriority(routeInfo.transmittedBytes, numPriorities);
           } else { // priority assigned packet by packet
                // SYN, ACKs, FIN => max priority (i.e. use 0 as transmitted bytes)
                /*if (segment->getPayloadLength()==0 && (segment->getAckBit() || segment->getSynBit() || segment->getFinBit())){
                  routeInfo.desiredPriority = 0;
                  routeInfo.transmittedBytes = 0;
                } else{*/
                routeInfo.transmittedBytes = flowStateVariables->totalSentBytes;
                routeInfo.desiredPriority = (uint8_t)thCtrl->getPacketPriority(routeInfo.transmittedBytes,
                        segment->getPayloadLength(), numPriorities);
                //}
           }
           delete flowStateVariables;
           break;
       }
       default: {
           throw cRuntimeError("Transport protocol not supported by routing controller");
       }
    }
    // routing on the DCN
    Node* src = getTorFor(datagram->getSrcAddress());
    Node* dst = getTorFor(datagram->getDestAddress());
    this->routingLookup(src, dst, datagram, &routeInfo);

    // Last step : add to the stack of priority labels created by routingLookup
    // the priority for src host NIC
    Node* srcHost = ipToNodePtr[datagram->getSrcAddress()];
    int outIf = forward(datagram, srcHost, dst, &routeInfo);
    // complex but neeeded (hosts have loopback interface so outIf which is the network layer index of the interface do not correspond
    // to the index in srcHost->interfaceInfos[] because in this vector the loopback interface is not present)
    FatTreeInterfaceInfo* iface = check_and_cast<FatTreeInterfaceInfo*>(topology->interfaceInfos[srcHost->interfaceTable->getInterface(outIf)]);
    this->fillPriorityAtInterface(iface, &routeInfo);

    return routeInfo;
}

/*
 * Given a datagram and the flow state, find the interface where to forward
 * and push priority label for that interface. Returns the interface chosen
 */
int RoutingController::forward(IPv4Datagram* datagram, Node* src, Node* dst, RoutingInfoCoS* routeInfo) {
    int destTorId = dst->getModule()->getIndex();
    int numECMPPorts = src->getTorRoutingTable().getNumRoutesOfPriority(destTorId, routeInfo->desiredPriority);
    if(numECMPPorts == 0) {
        throw cRuntimeError("No routes for destination");
    }
    int idx = loadBalance(datagram, numECMPPorts);    // find ECMP port index
    // get outgoing interface id, for route 'idx' among all possible routes of priority 'desiredPriority'
    int outIf = src->getTorRoutingTable().getRouteOfPriority(destTorId, routeInfo->desiredPriority, idx);
    return outIf;
}

// helper function to extract the lookup table <IPv4Address, Node*> from topology
void RoutingController::createIPAddressToNodeMapping() {
    for(int i=0; i < this->topology->getNumNodes(); i++) {
        // iterate over all network nodes
        Node* node = (Node *)topology->getNode(i);
        // add K,V <Ip,Node*> pair to map for fast search
        IInterfaceTable *interfaceTable = node->interfaceTable;
        if (interfaceTable) {
            for (int j = 0; j < interfaceTable->getNumInterfaces(); j++) {
                if(interfaceTable->getInterface(j)->ipv4Data()) {       // add only if IP layer is there (avoid adding <UNDEFINED> ip addresses)
                    InterfaceEntry *interfaceEntry = interfaceTable->getInterface(j);
                    IPv4Address ipAddr = interfaceEntry->getIPv4Address();
                    this->ipToNodePtr[ipAddr] = node;
                }
            }
        }
    }
}

// initialize static routes towards ToRs in all the switches
// TODO Move to Configurator
void RoutingController::setStaticRoutes() {
    // calculate shortest paths from everywhere to all ToRs and set output interfaces
    for(int i=0; i < topology->getNumNodes(); i++) {
        auto targetTor = (Node *)topology->getNode(i);
        // if it's a ToR
        if(((Node*)targetTor)->getFatTreeLayer() == 1) {  // OLD isToR(targetTor)
            int targetTorId = targetTor->getModule()->getIndex();
            string target = targetTor->getModule()->par("switchType").stdstringValue() + to_string(targetTorId);
            // SP towards ToR
            topology->calculateUnweightedSingleShortestPathsTo(targetTor);
            // node by node add routes
            for(int j=0; j < topology->getNumNodes(); j++) {
                if(j == i){ // except targetToR
                    continue;
                }
                auto node = (Node *)topology->getNode(j);
                // if current node is not a switch go on
                string src;
                if(node->isSwitch()) {  // OLD isSwitch(node)
                    src = node->getModule()->par("switchType").stdstringValue()
                            + to_string(node->getModule()->getIndex());
                } else {
                    src = "HOST" + to_string(node->getModule()->getIndex());
                }

                //int sieze = node->getAllPaths().size();
                // for all interfaces of a given node
                for (auto l :  node->getAllPaths()) {
                    NetworkConfiguratorBase::Link *link = (NetworkConfiguratorBase::Link *)l;
                    int outIfIndex = link->sourceInterfaceInfo->interfaceEntry->getNetworkLayerGateIndex();
                   // int outIfIndex = link->sourceInterfaceInfo->interfaceEntry->getInterfaceModule()->getIndex();
                   // int outIfIndex = link->sourceInterfaceInfo->interfaceEntry->getInterfaceModule()->gate("upperLayerIn")->getPreviousGate()->getIndex();

                    FatTreeIPv4NetworkConfigurator::InterfaceInfo* iface =
                            dynamic_cast<FatTreeIPv4NetworkConfigurator::InterfaceInfo*>(link->sourceInterfaceInfo);
                    Node* nextHopNode = (Node *)link->destinationInterfaceInfo->node;
                    string nextHop;
                    if(nextHopNode->isSwitch()){
                        nextHop = nextHopNode->getModule()->par("switchType").stdstringValue();
                    } else {
                        nextHop = "HOST";
                    }
                    nextHop = nextHop + to_string(link->destinationInterfaceInfo->node->getModule()->getIndex());
                    // for all priorities add route to node routing table (always add best effort)
                    node->getTorRoutingTable().addRoute(targetTorId, outIfIndex, TorIdRoutingTable::BEST_EFFORT);

                    for(auto& p : iface->getPriorities()) {
                        node->getTorRoutingTable().addRoute(targetTorId, outIfIndex, p);
                    }
                }
            }
        }
    }
}

void RoutingController::handleMessage(cMessage* msg){
    throw cRuntimeError("this module doesn't handle messages, it runs only in initialize()");
};

void RoutingController::initialize(int stage){
    if(stage == INITSTAGE_NETWORK_LAYER_2) {      //INIT_STAGE_LAST
        FatTreeIPv4NetworkConfigurator* confg = dynamic_cast<FatTreeIPv4NetworkConfigurator*>(getModuleByPath("^.configurator"));
        // get reference to network configurator
        if(confg == nullptr){
            throw cRuntimeError("NetworkConfigurator path invalid or not found");
        }
        topology = confg->getTopology();
        this->createIPAddressToNodeMapping();
        //this->initializeRoutingTables(); OLD
        this->setStaticRoutes();
        numPrioritiesPerSpine = (int)getAncestorPar("numPriorities");
        numSpines = (int)getAncestorPar("numSpines");
        spatialDiversity = getAncestorPar("enableSpatialDiversity");
        numPriorities = confg->getNumPriorities();
    }

    switch((int) this->getParentModule()->par("loadBalancing")) {
        case 1:
            this->lbpolicy = LoadBalancing::PER_FLOW_ECMP;
            break;
        case 2:
            this->lbpolicy = LoadBalancing::PACKET_SPRAYING;
            break;
        default: break;
    }
    if((thCtrl = check_and_cast<ThresholdController*>(getModuleByPath("^.thresholdController"))) == nullptr){
                throw cRuntimeError("NetworkConfigurator path invalid or not found");
    }
    staticClassification = !(bool)par("LAS");


};

/*
 * Returs the Node* to the Top of Rack switch to which an host with IP "addr" is connected
 */
Node* RoutingController::getTorFor(const IPv4Address& addr){
    try {
        return dynamic_cast<Node*>(this->ipToNodePtr.at(addr)->getLinkOut(0)->getRemoteNode());
    } catch (const out_of_range e) {
        throw cRuntimeError("Invalid or not existing IP address");
    }
}

/*
 * Returns the index of a ToR's gate cabled with a given host.
 * First the ip address is mapped into a node pointer, then the only link of the host is used
 * to navigate the topology and get to the ToR. Finally the gate index is extracted.
 *
 * Does not work with Multi-homed hosts.
 */
int RoutingController::getInterfaceToHost(const IPv4Address& host) {
    NetworkConfiguratorBase::Link * link = (NetworkConfiguratorBase::Link *)this->ipToNodePtr[host]->getLinkOut(0);
    return link->destinationInterfaceInfo->interfaceEntry->getNetworkLayerGateIndex();
}

/*
 * Given an incoming datagram, determine ECMP out port with two possible algorithms:
 * 1) Flow-level load balancing : IP SRC + IP DST + PROTOCOL + TRANSPORT SRC PORT + TANSP DST PORT
 * 2) Packet-level (packet spraying) : random uniform for each packet
 */
int RoutingController::loadBalance(IPv4Datagram* ipv4Datagram, int numECMPPorts) {

    if(lbpolicy == PACKET_SPRAYING) {
        return intuniform(0, numECMPPorts-1);
    }
    // sum fields for ECMP
    int outputPort = ipv4Datagram->getSrcAddress().getInt() +            // SRC IP
                    ipv4Datagram->getDestAddress().getInt() +        // DST IP
                    ipv4Datagram->getTransportProtocol();            // PROTO

    if(ipv4Datagram->getTransportProtocol() == IP_PROT_ROCE ||
            ipv4Datagram->getTransportProtocol() == IP_PROT_UDP){
        UDPPacket *udpPacket = dynamic_cast<UDPPacket*>(ipv4Datagram->getEncapsulatedPacket());
        if(udpPacket != nullptr) {
            outputPort += udpPacket->getSourcePort();
            outputPort += udpPacket->getDestinationPort();
            if(dynamic_cast<RDMAPacket*>(udpPacket->getEncapsulatedPacket()) != nullptr){
                RDMAPacket* rdmaPacket = check_and_cast<RDMAPacket*>(udpPacket->getEncapsulatedPacket());
                if(rdmaPacket->getExplixitRouting() == true){
                    rdmaPacket->setExplixitRouting(false);
                    return rdmaPacket->getExplicitSpineIndex();
                }
            }
        }
    } else if (ipv4Datagram->getTransportProtocol() == IP_PROT_TCP) {
        tcp::TCPSegment* segment = dynamic_cast<tcp::TCPSegment*>(ipv4Datagram->getEncapsulatedPacket());
        if(segment != nullptr) {
            outputPort += segment->getSourcePort();
            outputPort += segment->getDestinationPort();
        }
    }
    return outputPort % numECMPPorts;
}
