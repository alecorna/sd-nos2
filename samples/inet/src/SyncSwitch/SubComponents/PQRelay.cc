//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "PQRelay.h"
#include "inet/common/LayeredProtocolBase.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/common/ModuleAccess.h"
#include "inet/linklayer/ethernet/EtherFrame.h"
#include "inet/linklayer/ethernet/EtherMACBase.h"
#include "inet/linklayer/ethernet/Ethernet.h"
#include "inet/linklayer/ethernet/switch/MACRelayUnit.h"

Define_Module(PQRelay);

void PQRelay::handleAndDispatchFrame(EtherFrame *frame)
{
    int inputport = frame->getArrivalGate()->getIndex();

    numProcessedFrames++;

    // update address table
    addressTable->updateTableWithAddress(inputport, frame->getSrc());

    // handle broadcast frames first
    if (frame->getDest().isBroadcast()) {
        EV << "Broadcasting broadcast frame " << frame << endl;
        broadcastFrame(frame, inputport);
        return;
    }

    /*if(dynamic_cast<PriorityPauseFrame *>(frame) != nullptr){
        PriorityPauseFrame *ppFrame = check_and_cast<PriorityPauseFrame *>(frame);
        EV << "Received priority pause frame destined to port " << ppFrame->getPausePort() << " and priority " << ppFrame->getPausePriority() << ".\n";
        send(frame, "ifOut", ppFrame->getPausePort());
        return;
    }*/

    // Finds output port of destination address and sends to output port
    // if not found then broadcasts to all other ports instead
    //int outputport = addressTable->getPortForAddress(frame->getDest());
    int outputport = -1;
    if(dynamic_cast<EtherFrameCoS *>(frame) != nullptr){
        outputport = check_and_cast<EtherFrameCoS *>(frame)->getOutPortStack().top();  // get the last label
        frame->getOutPortStack().pop();                // remove it
    }
    // should not send out the same frame on the same ethernet port
    // (although wireless ports are ok to receive the same message)
    if (inputport == outputport) {
        EV << "Output port is same as input port, " << frame->getFullName()
           << " dest " << frame->getDest() << ", discarding frame\n";
        numDiscardedFrames++;
        delete frame;
        return;
    }

    if (outputport >= 0) {
        EV << "Sending frame " << frame << " with dest address " << frame->getDest() << " to port " << outputport << endl;
        emit(LayeredProtocolBase::packetSentToLowerSignal, frame);
        if(dynamic_cast<EtherFrameCoS *>(frame) != nullptr){
            check_and_cast<EtherFrameCoS *>(frame)->setOutPort(outputport);
        }
        send(frame, "ifOut", outputport);
    }
    else {
        EV << "Dest address " << frame->getDest() << " unknown, broadcasting frame " << frame << endl;
        broadcastFrame(frame, inputport);
    }
}

void PQRelay::sendPauseFrame(int sourcePort, int PQIndex, EtherFrameCoS* frame, double timeToStop){
    Enter_Method("sendPauseFrame(...)");
    PriorityPauseFrame *ppFrame = new PriorityPauseFrame();
    ppFrame->setDest(frame->getSrc());
    ppFrame->setSrc(frame->getDest());
    ppFrame->setPausePriority(PQIndex);
    ppFrame->setPauseTime(timeToStop);
    ppFrame->setPausePort(sourcePort);
    ppFrame->setByteLength(64);
    send(ppFrame, "ifOut", sourcePort);
}
