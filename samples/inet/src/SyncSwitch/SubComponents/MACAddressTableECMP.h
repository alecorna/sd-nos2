//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_MACADDRESSTABLEECMP_H_
#define __INET_MACADDRESSTABLEECMP_H_

#include <omnetpp.h>
#include "inet/linklayer/ethernet/switch/MACAddressTable.h"
#include "CentralizedECMPRouting.h"
using namespace inet;

/**
 * TODO - Generated class
 */
class MACAddressTableECMP : public MACAddressTable
{
    protected:
        //virtual void initialize() override;
        std::map<MACAddress, int[64]> portMappings; //TODO: remove static assignment

    public:
        virtual bool updateTableWithAddress(int portno, MACAddress& address, unsigned int vid) override;
        //virtual int getPortForAddress(MACAddress& address, unsigned int vid = 0) override;

};

#endif
