//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 
#include <stdio.h>

#include "FlowGenerator.h"
#include "inet/common/INETUtils.h"

Define_Module(FlowGenerator);

simsignal_t FlowGenerator::interArrivalTime = registerSignal("interArrivalTime");

void FlowGenerator::initialize(int stage) {
    if (stage == INITSTAGE_LOCAL) {

        parseDistributionParameters(); // read traffic distribution parameters
        numTors = (int)getAncestorPar("numTors");
        requestedFlows = 0;
        computeArrivalRate();
    }

}

// Search in the vector 'tcpApp' TcpBasicClientApp and/or TcpMultiSessionApp and returns the total number of
// occurrences. Another way could be finding tcpApps with parameter 'connectAddress' (only clients have it)
//int FlowGenerator::getNumberTcpClient(){
//    int clients = 0;
//
//    for(int i=0; i<getVectorSize(); i++) {
//        string fqn = getParentModule()->getSubmodule("tcpApp", i)->getNedTypeName();
//        int pos = fqn.find_last_of(".");
//        string appType = fqn.substr(pos+1);
//        transform(appType.begin(), appType.end(), appType.begin(), ::tolower);
//        if(appType == "tcpbasicclientapp" || appType== "tcpmultisessionapp") {
//            clients++;
//        }
//    }
//
//        // Finds number of TCP client of type TcpBasicClientApp or TcpMultiSessionApp inside the topology
//        // get number of servers in the topology
////
////        int numServer = this->getParentModule()->getVectorSize();
////            for(int i=0; i<numServer; i++) {
////                char buf[100];
////                memset(buf,0,100);
////                sprintf(buf, "%s%d%s", "<root>.host[", i, "]");
////                auto server = getModuleByPath(buf);
////                if(server->getSubmodule("tcpApp", 0)) {
////                    int numApps = server->getSubmodule("tcpApp", 0)->getVectorSize();
////                    // for each server find tcp client app
////                    for(int j=0; j<numApps; j++) {
////                        string fqn = server->getSubmodule("tcpApp", j)->getNedTypeName();
////                        int pos = fqn.find_last_of(".");
////                        string appType = fqn.substr(pos+1);
////                        transform(appType.begin(), appType.end(), appType.begin(), ::tolower);
////                        if(appType == "tcpbasicclientapp" || appType== "tcpmultisessionapp") {
////                            clients++;
////                        }
////                    }
////                }
////            }
//
//    return clients;
//}

// extract random destination among servers available excluding the client itself (all hosts need to be in a unique
// vector in the NED definition)
int FlowGenerator::getRandomDestination(char** buf) {

    bool disableRackLocalFlows = (bool) par("disableRackLocalTraffic");
    int rackSize = (int) getAncestorPar("rackSize");
    int myID = getParentModule()->getIndex();
    int myTor =  myID / rackSize;

    int dst;
    if(!disableRackLocalFlows) {   // choose among all hosts
        dst = intuniform_except(0, rackSize*numTors-1, myID, myID);
    } else {        // choose only other racks
        int torID_first =  myTor * rackSize;
        int torID_last = myTor * rackSize + rackSize - 1;
        dst = intuniform_except(0, rackSize*numTors-1, torID_first, torID_last);
    }

    const char* hostNames = getParentModule()->getName();
    *buf = new char[2*strlen(hostNames)];
    sprintf(*buf, "%s[%d]", hostNames, dst);
    return dst;
}


int FlowGenerator::intuniform_except(int a, int b, int excluded_from, int excluded_to){
    int numExcluded = excluded_to - excluded_from + 1;
    int dst = intuniform(a, b - numExcluded);
    if (dst >= excluded_from) {
        dst = dst+numExcluded;
    }
    return dst;
}

// helper to translate string parameters into enums to easily switch over them
void FlowGenerator::parseDistributionParameters(){
    // read flow length distribution
    string fld = par("flowLengthDistribution").stdstringValue();
    transform(fld.begin(), fld.end(), fld.begin(), ::tolower);

    if(fld == "pareto") {
        flowLengthDistribution = FlowSizeDistribution::PARETO;
    }
    else if(fld == "pareto_websearch") {
            flowLengthDistribution = FlowSizeDistribution::PARETO_WS;
    }
    else if(fld == "pareto_datamining") {
            flowLengthDistribution = FlowSizeDistribution::PARETO_DM;
    }
    else if (fld == "exp") {
        flowLengthDistribution = FlowSizeDistribution::EXPONENTIAL;
    }
    else if (fld == "uniform") {
        flowLengthDistribution = FlowSizeDistribution::UNIFORM;
    } else if (fld == "bimodal") {
        flowLengthDistribution = FlowSizeDistribution::BIMODAL;
    } else if (fld == "") {
        flowLengthDistribution = FlowSizeDistribution::NONE;
    }
    string transport_protocol = par("tprotocol").stdstringValue();
    transform(transport_protocol.begin(), transport_protocol.end(), transport_protocol.begin(), ::tolower);
    if (transport_protocol == "udp") {
        protocol = IP_PROT_UDP;
    } else if (transport_protocol == "tcp") {
        protocol = IP_PROT_TCP;
    } else {
        throw cRuntimeError("Protocol unspecified or unsupported");
    }
}

void FlowGenerator::computeArrivalRate() {
    int mss;
    if (protocol == IP_PROT_TCP) {
        mss = MAX_ETHERNET_DATA_BYTES - IP_HEADER_BYTES - TCP_HEADER_OCTETS;
    } else if (protocol == IP_PROT_UDP) {
        mss = MAX_ETHERNET_DATA_BYTES - IP_HEADER_BYTES - UDP_HEADER_BYTES;
    }
    double offeredLoad = (double)par("offeredLoad");
    if(offeredLoad < 0) {
        throw cRuntimeError("Offered load or link rate cannot be negative!");
    }
    cDatarateChannel* drc = check_and_cast<cDatarateChannel*>(getModuleByPath("<root>.T[0].eth[0]")->gate("phys$o")->getTransmissionChannel());
    double aggLinkRate = drc->getDatarate();
    int hostPerRack = (int)getAncestorPar("rackSize");
    int numSpines = (int)getAncestorPar("numSpines");

    // compute average flow size depending on distribution
    double averageFlowSize = getAverageFlowSize();
    // tot_bits / payload_per_packet (i.e. average number of packets) * header overhead
    double protocolHeadersOverhead = averageFlowSize / mss
              * (MAX_ETHERNET_DATA_BYTES - mss + ETHER_MAC_FRAME_BYTES + ETHER_8021Q_HEADER_LENGTH + PREAMBLE_BYTES + SFD_BYTES);

    // if traffic only outside rack, probability should be one.
    double probDestServerOutRack = 1;
    if(!(bool)par("disableRackLocalTraffic")) {
        probDestServerOutRack = (double)((numTors -1) * hostPerRack) / (numTors * hostPerRack - 1);
    }
    double gamma = 0;
    // ack load should be considered by default... TODO
    if((bool)par("considerAckLoad") && protocol == IP_PROT_TCP) {
        // MIN(L2) / MAX(L2) (i.e. ACK / MSS)
        gamma =  (double)(MAX_ETHERNET_DATA_BYTES - mss + ETHER_MAC_FRAME_BYTES + ETHER_8021Q_HEADER_LENGTH + PREAMBLE_BYTES + SFD_BYTES)
                / (mss + ETHER_MAC_FRAME_BYTES + ETHER_8021Q_HEADER_LENGTH + PREAMBLE_BYTES + SFD_BYTES);
    }
    lambda = numSpines * offeredLoad * aggLinkRate
                / (hostPerRack * probDestServerOutRack * 8 * (averageFlowSize + protocolHeadersOverhead) * (1 + gamma));
}

// Returns average flow size in bytes depending on the distribution. If no distribution is provided
// returns replyLength parameter. Lazy initialization
double FlowGenerator::getAverageFlowSize(){
    int mss;
    if (protocol == IP_PROT_TCP) {
        mss = MAX_ETHERNET_DATA_BYTES - IP_HEADER_BYTES - TCP_HEADER_OCTETS;
    } else if (protocol == IP_PROT_UDP) {
        mss = MAX_ETHERNET_DATA_BYTES - IP_HEADER_BYTES - UDP_HEADER_BYTES;
    }
    switch(flowLengthDistribution) {
        case FlowSizeDistribution::UNIFORM: {
            if((long)par("maxFlowSize") == (long)par("minFlowSize")) {
                return (double)par("maxFlowSize");
            }
            return ((double)par("maxFlowSize")+(double)par("minFlowSize"))/2;
            break;
        }
        case FlowSizeDistribution::EXPONENTIAL:
            return (double) par("expFlowSizeMean");
            break;
        case FlowSizeDistribution::PARETO:
        case FlowSizeDistribution::PARETO_WS:
        case FlowSizeDistribution::PARETO_DM:
        {
            double alpha,k,p;

            if (flowLengthDistribution == FlowSizeDistribution::PARETO) {
                alpha = (double) par("paretoShape");
                k = (double) par("minFlowSize");
                p = (double) par("maxFlowSize");
            } else if (flowLengthDistribution == FlowSizeDistribution::PARETO_WS) {
                alpha = 0.125;
                k = 3 * mss;
                p = 29200 * mss;
            } else if (flowLengthDistribution == FlowSizeDistribution::PARETO_DM) {
                alpha = 0.26;
                k = .1 * mss;
                p = 100000 * mss;
            }
            return alpha / ((1-alpha) * (pow(p,alpha) - pow(k,alpha))) * (p * pow(k,alpha) - k * pow(p,alpha));
            break;
        }
        case FlowSizeDistribution::BIMODAL : {
            double a = (double) par("minFlowSize");
            double b = (double) par("maxFlowSize");
            double p_a = (double) par("bimodalMinFlowSizeProbability");

            return a * p_a + b * (1-p_a);
        }
        case FlowSizeDistribution::NONE:
            return getGivenFlowLength();
            break;
        default:
            return -1;
            break;
    }
}

long FlowGenerator::getGivenFlowLength() {
    long fl = par("flowLength").longValue();
    return fl;
}

simtime_t FlowGenerator::getGivenInterFlowTime() {
    return (simtime_t)par("interFlowArrivalTime");
}

// Depending on distribution extract flow size
long FlowGenerator::getNewFlowLength() {
    if (requestedFlows > par("stopAfterFlows").longValue()) {
        endSimulation();
    }
    requestedFlows++;
    ;
    int mss;
    if (protocol == IP_PROT_TCP) {
        mss = 1460;
    } else if (protocol == IP_PROT_UDP) {
        mss = 1472;
    } else {
        throw cRuntimeError("Protocol not set");
    }

    long fl;
    switch(flowLengthDistribution) {
        case FlowSizeDistribution::UNIFORM:
            fl = intuniform((int) par("minFlowSize"), (int)par("maxFlowSize"));
            break;
        case FlowSizeDistribution::EXPONENTIAL: {
            fl = geometric(1./(double)par("expFlowSizeMean"))+1;    // we use geometric distribution since bytes are a discrete quantity
            break;
        }
        case FlowSizeDistribution::PARETO:
            fl = round(truncated_pareto((double) par("paretoShape"), (double) par("minFlowSize"), (double) par("maxFlowSize")));
            break;
        case FlowSizeDistribution::PARETO_WS:
            fl = round(truncated_pareto(0.125, (double) 3*mss, (double)29200 * mss));
            break;
        case FlowSizeDistribution::PARETO_DM:
            fl = round(truncated_pareto(0.26, (double) .1 * mss, (double) 100000 * mss));
            break;
        case FlowSizeDistribution::BIMODAL:
        {
            double p = uniform(0, 1);
            if (p <= (double)par("bimodalMinFlowSizeProbability")){
                fl = (long)par("minFlowSize");
            } else {
                fl = (long)par("maxFlowSize");
            }
            break;
        }
        case FlowSizeDistribution::NONE:
            fl = getGivenFlowLength();
    }
    return fl;
}

/*
 * Generate a sample from truncated / bounded Pareto distribution
 */
double FlowGenerator::truncated_pareto(double alpha, double L, double H){
    if(L > H || L <= 0) {
        throw cRuntimeError("Lower bound or upper bound invalid");
    }
    if(alpha<=0) {
        throw cRuntimeError("Invalid alpha, it must be > 0");
    }
    // inverse-transform method from uniform r.v.
    double U = uniform(0,1);
    return pow(( U*pow(L,alpha) + pow(H,alpha) - U*pow(H,alpha) ) / pow(L*H, alpha), -1.0/alpha);
}

// control sleep time between flow arrivals
simtime_t FlowGenerator::getSleepTime(){
    simtime_t intrArrTime = (simtime_t) exponential(1./lambda);
    emit(interArrivalTime, intrArrTime);
    return simTime() + intrArrTime;
}



