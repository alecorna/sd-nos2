//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET_TIMESLOTTEDSCHEDULER_H_
#define __INET_TIMESLOTTEDSCHEDULER_H_

#include <omnetpp.h>
#include "inet/common/queue/SchedulerBase.h"
#include "TDMController.h"
using namespace inet;

/**
 * TODO - Generated class
 */
class TimeSlottedScheduler : public SchedulerBase
{

    protected:
        virtual bool schedulePacket() override;
        virtual void handleMessage(cMessage *msg) override;
        virtual void initialize() override;
        virtual int getRandomIndexToServe(int* nonEmptyQueues, int totalNonEmptyQueues, int numQueues);
        virtual void finalize() override;
        virtual void requestPacket() override;
        virtual int numNonEmptyQueues();
        virtual void scheduleAdaptiveSlot(cMessage *msg);
        virtual void scheduleNonAdaptiveSlot(cMessage *msg);
        static simsignal_t MSSPerSlotSignal;
        static simsignal_t MSSPerSlotQueueEmptySignal;



    /*public:
        virtual int getNumPendingRequests() override { return 0; }*/

    public:
        virtual int getNumNonEmptyQueues();
        virtual void startSending(simtime_t slotDurationSeconds);
        virtual void startSendingMSS(int MSStoSend);


    protected:
        enum SLOT_MESSAGE_TYPE  { START, END, WAIT };
        TDMController* tdmController;
        int numInputs;
        int currentlyServingQueue;
        simtime_t frameDuration;
        simtime_t slotDuration;
        simtime_t slotEndTime;
        int currentSlot;
        int* timeSlots;
        cMessage* transmissionEvent;
        simtime_t transmissionStartTime;
        bool allQueuesEmpty = false;
        int MSStoSendPerSlot = 0;
        int MSSSentPerslot = 0;
};

#endif
