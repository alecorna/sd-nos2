//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef __INET_RDMABASICAPP_H
#define __INET_RDMABASICAPP_H

#include <vector>

#include "inet/common/INETDefs.h"

#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/contract/udp/UDPSocket.h"
#include "RDMAApp.h"
using namespace inet;
//namespace inet {

/**
 * UDP application. See NED for more info.
 */
class RDMABasicApp : public ApplicationBase
{
#define TOTAL_ETH_OH 66//54
protected:
    enum SelfMsgKinds { START = 1, SEND, STOP, BIND};
    enum IPGTypes {IPG_DISABLED = 0, IPG_EXP = 1, IPG_LINE_RATE = 2};
    enum onOffTypes {ON_OFF_DISABLED = 0, ON_OFF_EXP = 1, ON_OFF_GIVEN = 2};
    enum exponentialSleepTypes {EXP_SLEEP_FLOW = 0, EXP_SLEEP_FLOW_PROPORTIONAL = 1, EXP_SLEEP_GIVEN = 2};
    // parameters
    std::vector<L3Address> destAddresses;
    int localPort = -1, destPort = -1;

    int linkRate = 0;
    int bitrate = 0;
    int messageLength = 0;
    int flowLengthBytes = 0;
    int assignedPerFlowBimodalPktSize;
    int flowID = 0;
    simtime_t startTime;
    simtime_t stopTime;
    const char *packetName = nullptr;
    cOutVector networkLatencyVector;
    cOutVector flowCompletionTime;
    // state
    UDPSocket socket;
    cMessage *selfMsg = nullptr;

    // statistics
    simtime_t flowStartTime;

    int numSent = 0;
    int numReceived = 0;

    int seqNumber = 0;
    int maxSeqNumber = 100;
    int OOSPackets = 0;
    double averageFlowSize = 0;
    double averagePayloadSize = 0;
    static simsignal_t sentPkSignal;
    static simsignal_t rcvdPkSignal;
    static simsignal_t rcvdPauseSignal;
    static simsignal_t flwCmplTimeSignal;
    static simsignal_t flwSizeSignal;
    cOutVector fctVector;
    cOutVector oosVector;
    std::map<std::pair<int,int>, bool*> sequenceMap; //< <senderID, flowID>, window >
    std::map<std::pair<int,int>, int> sequences; //< <senderID, flowID>, window >


protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;
    virtual void handleMessageWhenUp(cMessage *msg) override;
    virtual void finish() override;
    virtual void refreshDisplay() const override;
    virtual simtime_t getNextPacketArrival();
    virtual simtime_t getSleepTime();
    // chooses random destination address
    virtual L3Address chooseDestAddr();
    virtual void sendPacket();
    virtual void processPacket(cPacket *msg);
    virtual void setSocketOptions();
    virtual bool checkIfIsOss(cPacket *pk);
    virtual bool allPktsReceived(cPacket *pk);

    virtual int getAveragePayloadSize();
    virtual int getNewFlowSize();
    virtual int getNewPayloadSize();
    virtual double getAverageFlowSize();

    virtual void processStart();
    virtual void processSend();
    virtual void processStop();

    virtual bool handleNodeStart(IDoneCallback *doneCallback) override;
    virtual bool handleNodeShutdown(IDoneCallback *doneCallback) override;
    virtual void handleNodeCrash() override;

    //virtual void stopApp();

public:
    RDMABasicApp() {}
    ~RDMABasicApp();
    virtual void pauseRDMATransmission(double timeToPause);
    virtual void recordFlowCompletionTime(simtime_t fct);

};

//} // namespace inet



#endif // ifndef __INET_RDMABASICAP_H

