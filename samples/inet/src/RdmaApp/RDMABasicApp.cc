//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "RDMABasicApp.h"

#include "inet/applications/base/ApplicationPacket_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"
#include "RDMAPacket_m.h"
//namespace inet {

Define_Module(RDMABasicApp);

simsignal_t RDMABasicApp::sentPkSignal = registerSignal("sentPk");
simsignal_t RDMABasicApp::rcvdPkSignal = registerSignal("rcvdPk");
simsignal_t RDMABasicApp::rcvdPauseSignal = registerSignal("rcvdPause");
simsignal_t RDMABasicApp::flwCmplTimeSignal = registerSignal("flwCmplTime");
simsignal_t RDMABasicApp::flwSizeSignal = registerSignal("flwSize");
RDMABasicApp::~RDMABasicApp()
{
    cancelAndDelete(selfMsg);
}

void RDMABasicApp::initialize(int stage)
{
    ApplicationBase::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {

        numSent = 0;
        numReceived = 0;
        WATCH(numSent);
        WATCH(numReceived);
        //if(getIndex() == getParentModule()->getIndex()) return;
        localPort = par("localPort");
        destPort = 4792;
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        packetName = par("packetName");
        linkRate = par("linkRate");
        averagePayloadSize = getAveragePayloadSize();
        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            throw cRuntimeError("Invalid startTime/stopTime parameters");

        selfMsg = new cMessage("sendTimer");
        getNewFlowSize();
        //WATCH(averageFlowSize);
        //WATCH_MAP(sequenceMap);
        //fctVector.setName("perHostFCT");
        networkLatencyVector.setName("NetPerPacketLatency");
        flowCompletionTime.setName("PerFlowFlowCompletionTime");
    }

}

void RDMABasicApp::finish()
{
    recordScalar("packets sent", numSent);
    recordScalar("packets received", numReceived);
    recordScalar("OOSPackets", OOSPackets);
    ApplicationBase::finish();
}

void RDMABasicApp::setSocketOptions()
{
    int timeToLive = par("timeToLive");
    if (timeToLive != -1)
        socket.setTimeToLive(timeToLive);

    int typeOfService = par("typeOfService");
    if (typeOfService != -1)
        socket.setTypeOfService(typeOfService);

    const char *multicastInterface = par("multicastInterface");
    if (multicastInterface[0]) {
        IInterfaceTable *ift = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this);
        InterfaceEntry *ie = ift->getInterfaceByName(multicastInterface);
        if (!ie)
            throw cRuntimeError("Wrong multicastInterface setting: no interface named \"%s\"", multicastInterface);
        socket.setMulticastOutputInterface(ie->getInterfaceId());
    }

    bool receiveBroadcast = par("receiveBroadcast");
    if (receiveBroadcast)
        socket.setBroadcast(true);

    bool joinLocalMulticastGroups = par("joinLocalMulticastGroups");
    if (joinLocalMulticastGroups) {
        MulticastGroupList mgl = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this)->collectMulticastGroups();
        socket.joinLocalMulticastGroups(mgl);
    }
    EV << "Bind with socket successful\n";
}

L3Address RDMABasicApp::chooseDestAddr()
{
    int k = intrand(destAddresses.size());
    if (destAddresses[k].isLinkLocal()) {    // KLUDGE for IPv6
        const char *destAddrs = par("destAddresses");
        cStringTokenizer tokenizer(destAddrs);
        const char *token = nullptr;

        for (int i = 0; i <= k; ++i)
            token = tokenizer.nextToken();
        destAddresses[k] = L3AddressResolver().resolve(token);
    }
    return destAddresses[k];
}

void RDMABasicApp::sendPacket()
{
    std::ostringstream str;
    str << packetName << "-" << numSent;

    if((bool) par("fbMessageLengthDistribution") == false && (bool) par("perFlowBimodal") == false){
        // Set initial flowStartTime for the first ever packet
        if(seqNumber == 0)
            flowStartTime = simTime();


        RDMAPacket *payload = new RDMAPacket(str.str().c_str());
        payload->setByteLength(getNewPayloadSize());
        payload->setSequenceNumber(seqNumber++);
        payload->setFlowStartTime(flowStartTime);
        payload->setFlowID(flowID);
        payload->setOriginModuleName(this->getFullPath().c_str());
        payload->setTotalSeqs(maxSeqNumber);
        //payload->setSenderModuleId(this->getId());

        if(seqNumber == maxSeqNumber) payload->setIsFinal(true);

        RDMAPacket* totalSizePayload = new RDMAPacket(str.str().c_str());
        totalSizePayload->setByteLength(payload->getByteLength() + TOTAL_ETH_OH);
        emit(sentPkSignal, totalSizePayload);
        delete totalSizePayload;

        socket.sendTo(payload, destAddresses[0], destPort);
        numSent++;
    }
    else if((bool) par("fbMessageLengthDistribution") == true){
        int newPayloadSize = 0;
        // Set initial flowStartTime for the first ever packet
        if(seqNumber == 0){
            flowStartTime = simTime();
        }


        RDMAPacket *payload = new RDMAPacket(str.str().c_str());
        if(flowLengthBytes > 1472){
            newPayloadSize = getNewPayloadSize();
        }
        else
            newPayloadSize = flowLengthBytes;
        flowLengthBytes -= newPayloadSize;
        payload->setByteLength(newPayloadSize);
        payload->setSequenceNumber(seqNumber++);
        payload->setFlowStartTime(flowStartTime);
        payload->setFlowID(flowID);
        payload->setOriginModuleName(this->getFullPath().c_str());
        payload->setTotalSeqs(maxSeqNumber);
        payload->setFlowSizeBytes(maxSeqNumber*1472);
        //payload->setSenderModuleId(this->getId());

        if(flowLengthBytes==0) payload->setIsFinal(true);

        RDMAPacket* totalSizePayload = new RDMAPacket(str.str().c_str());
        totalSizePayload->setByteLength(payload->getByteLength() + TOTAL_ETH_OH);
        emit(sentPkSignal, totalSizePayload);
        delete totalSizePayload;

        socket.sendTo(payload, destAddresses[0], destPort);
        numSent++;
    }
    else if((bool) par("perFlowBimodal") == true){
        int newPayloadSize = 0;
        // Set initial flowStartTime for the first ever packet
        if(seqNumber == 0){
            flowStartTime = simTime();
        }

        RDMAPacket *payload = new RDMAPacket(str.str().c_str());
        if(flowLengthBytes > assignedPerFlowBimodalPktSize){
            newPayloadSize = assignedPerFlowBimodalPktSize;
        }
        else
            newPayloadSize = flowLengthBytes;

        flowLengthBytes -= newPayloadSize;
        EV << "flowLengthBytes=" << flowLengthBytes << " assignedPerFlowBimodalPktSize=" << assignedPerFlowBimodalPktSize <<endl;
        payload->setByteLength(newPayloadSize);
        payload->setSequenceNumber(seqNumber++);
        payload->setFlowStartTime(flowStartTime);
        payload->setFlowID(flowID);
        payload->setOriginModuleName(this->getFullPath().c_str());
        payload->setTotalSeqs(maxSeqNumber);
        payload->setFlowSizeBytes(maxSeqNumber*1472);
        //payload->setSenderModuleId(this->getId());

        if(flowLengthBytes==0) payload->setIsFinal(true);

        RDMAPacket* totalSizePayload = new RDMAPacket(str.str().c_str());
        totalSizePayload->setByteLength(payload->getByteLength() + TOTAL_ETH_OH);
        emit(sentPkSignal, totalSizePayload);
        delete totalSizePayload;

        socket.sendTo(payload, destAddresses[0], destPort);
        numSent++;
    }
    else{
        throw cRuntimeError("Something wrong with the packet distribution");
    }
}

void RDMABasicApp::processStart()
{
    socket.setOutputGate(gate("udpOut"));
    const char *localAddress = par("localAddress");
    socket.bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
    setSocketOptions();
    //if(getIndex() == getParentModule()->getIndex()) return;

    const char *destAddrs = par("destAddresses");
    cStringTokenizer tokenizer(destAddrs);
    const char *token;

    while ((token = tokenizer.nextToken()) != nullptr) {
        L3Address result;
        L3AddressResolver().tryResolve(token, result);
        if (result.isUnspecified())
            EV_ERROR << "cannot resolve destination address: " << token << endl;
        else
            destAddresses.push_back(result);
    }

    if (!destAddresses.empty()) {
        selfMsg->setKind(SEND);
        processSend();
    }

}

void RDMABasicApp::processSend()
{
    simtime_t nextPktArrivalTime = SIMTIME_ZERO;
    EV << "New flow with total number of packets= " << maxSeqNumber <<"\n";
    flowLengthBytes = maxSeqNumber*1472;
    assignedPerFlowBimodalPktSize = getNewPayloadSize();
    EV << "Assigned bimodal size=" << assignedPerFlowBimodalPktSize << endl;
    //if(assignedPerFlowBimodalPktSize < 1000) throw cRuntimeError("It's working");
    if((int) par("IPGType") == IPG_DISABLED && (bool) par("fbMessageLengthDistribution") == false && (bool) par("perFlowBimodal") == false){
        for(int i=0; i<maxSeqNumber; i++)
            sendPacket();
    }
    else if((int) par("IPGType") == IPG_DISABLED && ((bool) par("fbMessageLengthDistribution") == true || (bool) par("perFlowBimodal") == true))
        while(flowLengthBytes > 0)
            sendPacket();
    else{
        sendPacket();
        nextPktArrivalTime = getNextPacketArrival();
    }

    EV << "Finished sending\n";

    if (stopTime < SIMTIME_ZERO || nextPktArrivalTime < stopTime) {
        selfMsg->setKind(SEND);

        if(seqNumber == maxSeqNumber && (bool) par("fbMessageLengthDistribution") == false && (bool) par("perFlowBimodal") == false){
            seqNumber = 0;
            getNewFlowSize();
            flowStartTime = simTime();
            flowID++;
            simtime_t sleepTime = getSleepTime();
            if(sleepTime < stopTime)
                scheduleAt(sleepTime, selfMsg);
        }
        else if(flowLengthBytes==0 && ((bool) par("fbMessageLengthDistribution") == true || (bool) par("perFlowBimodal") == true)){
            seqNumber = 0;
            getNewFlowSize();
            flowStartTime = simTime();
            flowID++;
            simtime_t sleepTime = getSleepTime();
            EV << "psend sleep time=" << sleepTime <<endl;
            if(sleepTime < stopTime)
                scheduleAt(sleepTime, selfMsg);
        }
        else
            scheduleAt(nextPktArrivalTime, selfMsg);
    }
}

simtime_t RDMABasicApp::getNextPacketArrival(){
    simtime_t mssTxDuration;
    simtime_t sleepTime;
    messageLength = par("messageLength");
    mssTxDuration = (simtime_t) 8 * (messageLength + TOTAL_ETH_OH)/linkRate;
    switch((int) par("IPGType")){
    case(IPG_DISABLED):
        sleepTime = (simtime_t) simTime();
        break;
    case(IPG_EXP):
        sleepTime = (simtime_t) simTime() + exponential(mssTxDuration);
        break;
    case(IPG_LINE_RATE):
        sleepTime = (simtime_t) simTime() + mssTxDuration*((int) par("IPGLineRateMultiplier"));
        break;
    default:
        throw cRuntimeError("Invalid IPGType parameter.");
        break;
    }
    return sleepTime;
}

simtime_t RDMABasicApp::getSleepTime(){
    simtime_t sleepTime;
    simtime_t flowDuration;
    if((bool) par("fbMessageLengthDistribution") == false && (bool) par("perFlowBimodal") == false)
        flowDuration = (simtime_t) getAverageFlowSize() * 8
        * (getAveragePayloadSize() + TOTAL_ETH_OH)
        / ((int) par("linkRate"));
    else if((bool) par("fbMessageLengthDistribution") == true)
        flowDuration = (simtime_t) (getAverageFlowSize() * 8 * 1472
                + (getAverageFlowSize() * 1472)/getAveragePayloadSize()*TOTAL_ETH_OH*8)
                / ((int) par("linkRate"));
    else if((bool) par("perFlowBimodal") == true){
        flowDuration = (simtime_t) (getAverageFlowSize() * 8 * 1472
                + (getAverageFlowSize() * 1472)/getAveragePayloadSize()*TOTAL_ETH_OH*8)
                / ((int) par("linkRate"));
    }
    EV << "averageFlowSize=" << getAverageFlowSize() << " getAveragePayloadSize="<< getAveragePayloadSize() <<"\n";
    EV << "Flow duration: " << flowDuration  << "\n";
    switch ((int) par("onOffType")) {
        case (ON_OFF_DISABLED):
            sleepTime = simTime();
            break;
        case (ON_OFF_EXP):
            switch ((int) par("exponentialSleepType")) {
            case (EXP_SLEEP_FLOW):
                sleepTime = simTime() + (simtime_t) exponential(flowDuration);
                break;
            case (EXP_SLEEP_FLOW_PROPORTIONAL):
                //sleepTime = simTime() + (simtime_t) exponential(flowDuration * ((double) par("OnOffTimeMultiplier")));
                sleepTime = simTime()
                        + (simtime_t) exponential(
                                flowDuration * (this->getVectorSize() - 1)
                                        / ((double) par("OnOffTimeMultiplier")));
                break;
            case (EXP_SLEEP_GIVEN):
                sleepTime = simTime()
                        + (simtime_t) exponential((double) par("sleepTime"));
                break;
            default:
                throw cRuntimeError("Invalid exponentialSleepType parameter.");
                break;
        }
        break;
    case (ON_OFF_GIVEN):
        sleepTime = simTime() + (double) par("sleepTime");
        break;
    default:
        throw cRuntimeError("Invalid onOffType parameter.");
        break;
    }
    EV << "sleep time=" << sleepTime <<endl;
    return sleepTime;
}

void RDMABasicApp::processStop(){
    socket.close();
}

void RDMABasicApp::handleMessageWhenUp(cMessage *msg){
    if (msg->isSelfMessage()) {
        ASSERT(msg == selfMsg);
        switch (selfMsg->getKind()) {
        case START:
            processStart();
            break;

        case SEND:
            processSend();
            break;

        case STOP:
            processStop();
            break;

        default:
            throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
        }
    }
    else if (msg->getKind() == UDP_I_DATA) {
        // process incoming packet
        processPacket(PK(msg));
    }
    else if (msg->getKind() == UDP_I_ERROR) {
        EV_WARN << "Ignoring UDP error report\n";
        delete msg;
    }
    else {
        throw cRuntimeError("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
    }
}

void RDMABasicApp::pauseRDMATransmission(double timeToPause) {
    Enter_Method("processPause(...)");
    if(destAddresses.size() == 0) return; // RX only
    cancelEvent(selfMsg);
    selfMsg->setKind(SEND);
    emit(rcvdPauseSignal, 1);
    scheduleAt(simTime()+timeToPause, selfMsg);
}

void RDMABasicApp::refreshDisplay() const
{
    char buf[100];
    sprintf(buf, "rcvd: %d pks\nsent: %d pks", numReceived, numSent);
    getDisplayString().setTagArg("t", 0, buf);
}

void RDMABasicApp::processPacket(cPacket *pk)
{
    RDMAPacket* totalSizePayload = new RDMAPacket();

    totalSizePayload->setByteLength(pk->getByteLength() + TOTAL_ETH_OH);
    emit(rcvdPkSignal, totalSizePayload);
    delete totalSizePayload;

    EV_INFO << "Received packet: " << UDPSocket::getReceivedPacketInfo(pk) << endl;
    RDMAPacket *payload = check_and_cast<RDMAPacket *>(pk);
    networkLatencyVector.record(simTime() - payload->getEmissionTime());
    //checkIfIsOss(pk);
    //bool flowEnded = allPktsReceived(pk);
    simtime_t normalizedFCT;
    if(payload->getIsFinal()){
        if((bool) par("fbMessageLengthDistribution"))
            normalizedFCT = (simtime_t) (simTime()-payload->getFlowStartTime())/(payload->getFlowSizeBytes())*1472;
        else
            normalizedFCT = (simtime_t) (simTime()-payload->getFlowStartTime())/(payload->getSequenceNumber()+1);
        EV << "Recording FCT: " << normalizedFCT <<"\n";
        emit(flwCmplTimeSignal, normalizedFCT);
        emit(flwSizeSignal, (long) payload->getSequenceNumber());
        flowCompletionTime.record(simTime()-payload->getFlowStartTime());

        //RDMABasicApp* originModule = (RDMABasicApp*) getModuleByPath(payload->getOriginModuleName());
        //originModule->recordFlowCompletionTime( (simtime_t) normalizedFCT);
    }

    delete pk;
    numReceived++;
}

bool RDMABasicApp::allPktsReceived(cPacket *pk){
    RDMAPacket *payload = check_and_cast<RDMAPacket *>(pk);
    RDMABasicApp* originModule = (RDMABasicApp*) getModuleByPath(payload->getOriginModuleName());
    int senderId = originModule->getId();
    auto it = sequenceMap.find(std::make_pair(senderId, payload->getFlowID()));

    for(int i=0; i<payload->getTotalSeqs(); i++){
        if(it->second[i] == false)
            return false;
    }
    delete[] it->second;
    sequenceMap.erase(it);

    return true;
}


bool RDMABasicApp::checkIfIsOss(cPacket *pk){
    RDMAPacket *payload = check_and_cast<RDMAPacket *>(pk);
    RDMABasicApp* originModule = (RDMABasicApp*) getModuleByPath(payload->getOriginModuleName());
    int senderId = originModule->getId();
    int totSegments = payload->getTotalSeqs();
    if(totSegments == 0) totSegments = 1;

    if( sequenceMap.find(std::make_pair(senderId, payload->getFlowID())) == sequenceMap.end() ){
        EV << "Adding new entry with senderID=" << senderId << " flowID=" << payload->getFlowID() << " maxSeq=" << totSegments << "\n";
        bool *rxW = new bool[totSegments];
        for(int i=0; i< totSegments; i++) rxW[i] = false;
        sequenceMap.insert(std::make_pair(std::make_pair(senderId, payload->getFlowID()), rxW));
        sequences.insert(std::make_pair(std::make_pair(senderId, payload->getFlowID()), totSegments));
    }

    auto it = sequenceMap.find(std::make_pair(senderId, payload->getFlowID()));

    int firstExpectedSeqNo = 0;
    for(int i=0; i< totSegments; i++){
        if(it->second[i] == false){
            firstExpectedSeqNo = i;
            break;
        }
    }
    auto is = sequences.find(std::make_pair(senderId, payload->getFlowID()));

    if(is == sequences.end())   throw cRuntimeError("NOPE");

    if(payload->getSequenceNumber() > is->second){
        throw cRuntimeError("NOPE");
        for(int i=0; i< is->second ; i++){
            if(it->second[i])
                EV << "1 ";
            else
                EV << "0 ";
        }
        EV << "\n";
    }

    it->second[payload->getSequenceNumber()] = true;
    if( firstExpectedSeqNo != payload->getSequenceNumber() ){
        OOSPackets++;
        return true;
    }
    return false;
}

bool RDMABasicApp::handleNodeStart(IDoneCallback *doneCallback)
{
    simtime_t start = std::max(startTime, simTime());

    if((bool) par("asyncStart") == true)
        start = (double) par("startTime") + getSleepTime();
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}

bool RDMABasicApp::handleNodeShutdown(IDoneCallback *doneCallback)
{
    if (selfMsg)
        cancelEvent(selfMsg);
    //TODO if(socket.isOpened()) socket.close();
    return true;
}

void RDMABasicApp::handleNodeCrash()
{
    if (selfMsg)
        cancelEvent(selfMsg);
}

void RDMABasicApp::recordFlowCompletionTime(simtime_t fct){
    return;
    Enter_Method_Silent();
    fctVector.record(fct);
}

double RDMABasicApp::getAverageFlowSize(){
    if((bool) par("paretoFlowSize") == true)
        averageFlowSize = ( (double) par("paretoShape") * (double) par("paretoScale") ) / ( (double) par("paretoShape") - 1 );
    else if((bool) par("expFlowSize") == true)
        averageFlowSize = (double) par("expFlowSizeMean");
    else
        averageFlowSize = ( (int) par("maxSeqLen") - (int) par("minSeqLen") ) / 2;
    return averageFlowSize;
}

int RDMABasicApp::getNewFlowSize(){

    if((bool) par("paretoFlowSize") == true && (bool) par("expFlowSize") == true) throw cRuntimeError("Both pareto and exp flow size are present");


    if((bool) par("paretoFlowSize") == true){
        if((double) par("paretoShape") <= 0 || (double) par("paretoScale") <= 0 ) throw cRuntimeError("Flow size is set to Pareto but shape or scale are incorrect.");
        maxSeqNumber = ceil(pareto_shifted((double) par("paretoShape"), (double) par("paretoScale"), (double) par("paretoShift")));
        averageFlowSize = ( (double) par("paretoShape") * (double) par("paretoScale") ) / ( (double) par("paretoShape") - 1 );
        // Truncating Pareto
        if(maxSeqNumber > 100*averageFlowSize)
            maxSeqNumber = 100*averageFlowSize;
    }
    else if((bool) par("expFlowSize") == true){
        EV << "Setting averageFlowSize\n";
        averageFlowSize = (double) par("expFlowSizeMean");
        EV << "averageFlowSize="<<averageFlowSize<<"\n";

        maxSeqNumber = ceil(exponential(averageFlowSize));
    }
    else {
        maxSeqNumber = intuniform((int) par("minSeqLen"), (int) par("maxSeqLen"));
        averageFlowSize = ( (int) par("maxSeqLen") - (int) par("minSeqLen") ) / 2;
    }
    return maxSeqNumber;
}

int RDMABasicApp::getAveragePayloadSize(){
    if((bool) par("bimodalMessageLengthDistribution") == true){
        return ceil( ((double) par("bimodalLowP")) * ((int) par("bimodalLowSize")) + (1 - (double) par("bimodalLowP")) * ((int) par("messageLength")) );
    }
    else if((bool) par("fbMessageLengthDistribution") == true){
        return(0.45*60 + 0.05*100 + 0.1*200 + 0.2*300 + 0.05*400 + 0.15*1472);
    }
    else
        return (int) par("messageLength");
}

int RDMABasicApp::getNewPayloadSize(){
    if((bool) par("bimodalMessageLengthDistribution") == true){
        if(uniform(0, 1) < (double) par("bimodalLowP")) return (int) par("bimodalLowSize");
    }
    else if((bool) par("fbMessageLengthDistribution") == true){
        double p = uniform(0,1);
        if(p<0.45) return 60;
        else if(p<0.5) return 100;
        else if(p<0.6) return 200;
        else if(p<0.8) return 300;
        else if(p<0.85) return 400;
        else return 1472;
    }
    return (int) par("messageLength");

}



