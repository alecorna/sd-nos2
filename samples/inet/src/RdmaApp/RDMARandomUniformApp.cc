//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "RDMARandomUniformApp.h"
#include "inet/networklayer/common/L3AddressResolver.h"

Define_Module(RDMARandomUniformApp);


L3Address RDMARandomUniformApp::chooseDestAddr()
{
    char dstModuleName[100];
    sprintf(dstModuleName, "host[%d]", this->getIndex());
    destAddresses.push_back(L3AddressResolver().resolve(dstModuleName));
    return destAddresses[0];
}

void RDMARandomUniformApp::processStart()
{
    if (!destAddresses.empty()) {
        selfMsg->setKind(SEND);
        processSend();
    }

}

bool RDMARandomUniformApp::handleNodeStart(IDoneCallback *doneCallback){
    simtime_t start = std::max(startTime, simTime());
    simtime_t avgFlowDuration = 0;
    if((bool) par("asyncStart") == true)
        start = (double) par("startTime") + getSleepTime();
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        if(this->getIndex() != this->getParentModule()->getIndex())
            scheduleAt(start, selfMsg);
    }

    socket.setOutputGate(gate("udpOut"));
    const char *localAddress = par("localAddress");
    socket.bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
    setSocketOptions();
    chooseDestAddr();

    return true;
}


