//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004-2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include <algorithm>
#include <string>
#include "RDMA.h"
#include "RDMABasicApp.h"
#include "RDMAApp.h"
//namespace inet {
Define_Module(RDMA);

void RDMA::initialize(int stage) {

    cSimpleModule::initialize(stage);

    pollingMessage = new cMessage();
    pollingMessage->setKind(PULL_FOR_NEW_MSG);

    if (stage == INITSTAGE_LOCAL) {
        //WATCH_PTRMAP(socketsByIdMap);
        //WATCH_MAP(socketsByPortMap);

        lastEphemeralPort = EPHEMERAL_PORTRANGE_START;
        ift = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this);
        icmp = nullptr;
        icmpv6 = nullptr;

        numSent = 0;
        numPassedUp = 0;
        numDroppedWrongPort = 0;
        numDroppedBadChecksum = 0;
        WATCH(numSent);
        WATCH(numPassedUp);
        WATCH(numDroppedWrongPort);
        WATCH(numDroppedBadChecksum);

        isOperational = false;
    }
    else if (stage == INITSTAGE_TRANSPORT_LAYER) {
        IPSocket ipSocket(gate("ipOut"));
        ipSocket.registerProtocol(IP_PROT_ROCE);

        NodeStatus *nodeStatus = dynamic_cast<NodeStatus *>(findContainingNode(this)->getSubmodule("status"));
        isOperational = (!nodeStatus) || nodeStatus->getState() == NodeStatus::UP;
    }

}

void RDMA::handleMessage(cMessage *msg)
{
    if (!isOperational)
        throw cRuntimeError("Message '%s' received when UDP is OFF", msg->getName());

    // received from IP layer
    if (msg->arrivedOn("ipIn")) {
        if (dynamic_cast<UDPPacket *>(msg) != nullptr)
            processUDPPacket((UDPPacket *)msg);
        else
            processICMPError(PK(msg)); // assume it's an ICMP error
    }
    /*else if(msg->isSelfMessage()){ // Self message.
        if(msg->getKind() == PULL_FOR_NEW_MSG){
            RDMAApp *app = gate("udpOut", appToServe)->getPathEndGate()->getOwnerModule();
            if(app->hasToSend()){
                processPacketFromApp(app->getNextPacket());
            }
            appToServe = (appToServe + 1)%gateSize("udpOut");
            scheduleAt(simTime(), msg);
        }
    }*/
    else {    // received from application layer
        if (msg->getKind() == UDP_C_DATA)
            processPacketFromApp(PK(msg));
        else
            processCommandFromApp(msg);
    }
}

void RDMA::sendDown(cPacket *appData, const L3Address& srcAddr, ushort srcPort, const L3Address& destAddr, ushort destPort,
        int interfaceId, bool multicastLoop, int ttl, unsigned char tos)
{
    if (destAddr.isUnspecified())
        throw cRuntimeError("send: unspecified destination address");
    if (destPort <= 0 || destPort > 65535)
        throw cRuntimeError("send invalid remote port number %d", destPort);

    UDPPacket *udpPacket = createUDPPacket(appData->getName());
    udpPacket->setByteLength(UDP_HEADER_BYTES);
    udpPacket->encapsulate(appData);

    // set source and destination port
    udpPacket->setSourcePort(srcPort);
    udpPacket->setDestinationPort(destPort);

    if (destAddr.getType() == L3Address::IPv4) {
        // send to IPv4
        EV_INFO << "Sending app packet " << appData->getName() << " over IPv4.\n";
        IPv4ControlInfo *ipControlInfo = new IPv4ControlInfo();
        ipControlInfo->setProtocol(IP_PROT_ROCE);
        ipControlInfo->setSrcAddr(srcAddr.toIPv4());
        ipControlInfo->setDestAddr(destAddr.toIPv4());
        ipControlInfo->setInterfaceId(interfaceId);
        ipControlInfo->setMulticastLoop(multicastLoop);
        ipControlInfo->setTimeToLive(ttl);
        ipControlInfo->setTypeOfService(tos);
        udpPacket->setControlInfo(ipControlInfo);

        emit(LayeredProtocolBase::packetSentToLowerSignal, udpPacket);
        emit(sentPkSignal, udpPacket);
        send(udpPacket, "ipOut");
    }
    else if (destAddr.getType() == L3Address::IPv6) {
        // send to IPv6
        EV_INFO << "Sending app packet " << appData->getName() << " over IPv6.\n";
        IPv6ControlInfo *ipControlInfo = new IPv6ControlInfo();
        ipControlInfo->setProtocol(IP_PROT_ROCE);
        ipControlInfo->setSrcAddr(srcAddr.toIPv6());
        ipControlInfo->setDestAddr(destAddr.toIPv6());
        ipControlInfo->setInterfaceId(interfaceId);
        ipControlInfo->setMulticastLoop(multicastLoop);
        ipControlInfo->setHopLimit(ttl);
        ipControlInfo->setTrafficClass(tos);
        udpPacket->setControlInfo(ipControlInfo);

        emit(LayeredProtocolBase::packetSentToLowerSignal, udpPacket);
        emit(sentPkSignal, udpPacket);
        send(udpPacket, "ipOut");
    }
    else {
        // send to generic
        EV_INFO << "Sending app packet " << appData->getName() << endl;
        IL3AddressType *addressType = destAddr.getAddressType();
        INetworkProtocolControlInfo *ipControlInfo = addressType->createNetworkProtocolControlInfo();
        ipControlInfo->setTransportProtocol(IP_PROT_ROCE);
        ipControlInfo->setSourceAddress(srcAddr);
        ipControlInfo->setDestinationAddress(destAddr);
        ipControlInfo->setInterfaceId(interfaceId);
        //ipControlInfo->setMulticastLoop(multicastLoop);
        ipControlInfo->setHopLimit(ttl);
        //ipControlInfo->setTrafficClass(tos);
        udpPacket->setControlInfo(dynamic_cast<cObject *>(ipControlInfo));

        emit(LayeredProtocolBase::packetSentToLowerSignal, udpPacket);
        emit(sentPkSignal, udpPacket);
        send(udpPacket, "ipOut");
    }
    numSent++;
}

void RDMA::processUDPPacket(UDPPacket *udpPacket)
{
    emit(LayeredProtocolBase::packetReceivedFromLowerSignal, udpPacket);
    emit(rcvdPkSignal, udpPacket);

    // simulate checksum: discard packet if it has bit error
    EV_INFO << "Packet " << udpPacket->getName() << " received from network, dest port " << udpPacket->getDestinationPort() << "\n";

    if (udpPacket->hasBitError()) {
        EV_WARN << "Packet has bit error, discarding\n";
        emit(droppedPkBadChecksumSignal, udpPacket);
        numDroppedBadChecksum++;
        delete udpPacket;

        return;
    }

    L3Address srcAddr;
    L3Address destAddr;
    bool isMulticast, isBroadcast;
    int srcPort = udpPacket->getSourcePort();
    int destPort = udpPacket->getDestinationPort();
    int interfaceId;
    int ttl;
    unsigned char tos;

    cObject *ctrl = udpPacket->removeControlInfo();
    if (dynamic_cast<IPv4ControlInfo *>(ctrl) != nullptr) {
        IPv4ControlInfo *ctrl4 = (IPv4ControlInfo *)ctrl;
        srcAddr = ctrl4->getSrcAddr();
        destAddr = ctrl4->getDestAddr();
        interfaceId = ctrl4->getInterfaceId();
        ttl = ctrl4->getTimeToLive();
        tos = ctrl4->getTypeOfService();
        isMulticast = ctrl4->getDestAddr().isMulticast();
        isBroadcast = ctrl4->getDestAddr().isLimitedBroadcastAddress();    // note: we cannot recognize other broadcast addresses (where the host part is all-ones), because here we don't know the netmask
    }
    else if (dynamic_cast<IPv6ControlInfo *>(ctrl) != nullptr) {
        IPv6ControlInfo *ctrl6 = (IPv6ControlInfo *)ctrl;
        srcAddr = ctrl6->getSrcAddr();
        destAddr = ctrl6->getDestAddr();
        interfaceId = ctrl6->getInterfaceId();
        ttl = ctrl6->getHopLimit();
        tos = ctrl6->getTrafficClass();
        isMulticast = ctrl6->getDestAddr().isMulticast();
        isBroadcast = false;    // IPv6 has no broadcast, just various multicasts
    }
    else if (dynamic_cast<GenericNetworkProtocolControlInfo *>(ctrl) != nullptr) {
        GenericNetworkProtocolControlInfo *ctrlGeneric = (GenericNetworkProtocolControlInfo *)ctrl;
        srcAddr = ctrlGeneric->getSourceAddress();
        destAddr = ctrlGeneric->getDestinationAddress();
        interfaceId = ctrlGeneric->getInterfaceId();
        ttl = ctrlGeneric->getHopLimit();
        tos = 0;    // TODO: ctrlGeneric->getTrafficClass();
        isMulticast = ctrlGeneric->getDestinationAddress().isMulticast();
        isBroadcast = false;    // IPv6 has no broadcast, just various multicasts
    }
    else if (ctrl == nullptr) {
        throw cRuntimeError("(%s)%s arrived from lower layer without control info",
                udpPacket->getClassName(), udpPacket->getName());
    }
    else {
        throw cRuntimeError("(%s)%s arrived from lower layer with unrecognized control info %s",
                udpPacket->getClassName(), udpPacket->getName(), ctrl->getClassName());
    }

    if (!isMulticast && !isBroadcast) {
        // unicast packet, there must be only one socket listening
        SockDesc *sd = findSocketForUnicastPacket(destAddr, destPort, srcAddr, srcPort);
        if (!sd) {
            EV_WARN << "No socket registered on port " << destPort << "\n";
            processUndeliverablePacket(udpPacket, ctrl);
            return;
        }
        else {
            cPacket *payload = udpPacket->decapsulate();
            sendUp(payload, sd, srcAddr, srcPort, destAddr, destPort, interfaceId, ttl, tos);
            delete udpPacket;
            delete ctrl;
        }
    }
    else {
        // multicast packet: find all matching sockets, and send up a copy to each
        std::vector<SockDesc *> sds = findSocketsForMcastBcastPacket(destAddr, destPort, srcAddr, srcPort, isMulticast, isBroadcast);
        if (sds.empty()) {
            EV_WARN << "No socket registered on port " << destPort << "\n";
            processUndeliverablePacket(udpPacket, ctrl);
            return;
        }
        else {
            cPacket *payload = udpPacket->decapsulate();
            unsigned int i;
            for (i = 0; i < sds.size() - 1; i++) // sds.size() >= 1
                sendUp(payload->dup(), sds[i], srcAddr, srcPort, destAddr, destPort, interfaceId, ttl, tos); // dup() to all but the last one
            sendUp(payload, sds[i], srcAddr, srcPort, destAddr, destPort, interfaceId, ttl, tos);    // send original to last socket
            delete udpPacket;
            delete ctrl;
        }
    }
}


void RDMA::pauseAllRDMATransmissions(double timeToPause){
    //pauseAllRMDAAppPolling(timeToPause);
    //return;
    // For single RDMAApplications pause.
    for(auto it : socketsByIdMap){
        check_and_cast<RDMABasicApp *>(gate("appOut", it.second->appGateIndex)->getPathEndGate()->getOwnerModule())->pauseRDMATransmission(timeToPause);
    }
}

void RDMA::pauseAllRMDAAppPolling(double timeToPause){
    if(pollingMessage->isScheduled()){
        cancelEvent(pollingMessage);
    }
    scheduleAt(simTime()+timeToPause, pollingMessage);
}

void RDMA::finish(){
    cancelAndDelete(pollingMessage);
}
