#/bin/bash

if (($# > 2)) ; then
	echo "Invalid number of arguments. Usage ./find.sh <keyword>";	
	exit 0;
fi

KEYWORD=$1;

if [[ -z $2 ]] ; then
	DIR=$(pwd);
else
	DIR=$2;
fi

find $DIR \( -name "*.h" -o -name "*.cc" -o -name "*.ned" \) -type f -exec grep -q "${KEYWORD}" {} \; -print

exit $?;

