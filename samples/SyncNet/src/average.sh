#!/bin/bash

sed '/^$/d' | sed 's/[^0-9.]*//g' | awk '{ sum = sum + $1}END{print "\navg : "sum/NR "\n"}'
