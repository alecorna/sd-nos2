#!/bin/bash

# Wrapper to run the simulation, controlling express mode, logging and runnumber.

if (( $# < 2 )) ; then
	echo "Usage ${0} <express-mode> <runs> <output-log-file?>"
	exit 1;
fi

FILEPATH="";
if [[ ! -z $3 && $1=="false" ]] ; then
	if [[ $3 == "default" ]] ; then
		OUTPUTFILE=""
	else 
		OUTPUTFILE="--cmdenv-output-file=$3"
	fi
	
	FILEPATH="--cmdenv-redirect-output=true $OUTPUTFILE";
fi
##DEBUG
#echo "output file: $FILEPATH";
/home/alessandrocornacchia/omnetpp-5.2.1/samples/SyncNet/src/SyncNet_dbg -m -u Cmdenv -c prioritiesNoPFC -n .:../src:../../inet/examples:../../inet/showcases:../../inet/src:../../inet/tutorials --image-path=../../inet/images -l ../../inet/src/INET FatTree_tmp.ini -r $2 --cmdenv-express-mode=$1 $FILEPATH


