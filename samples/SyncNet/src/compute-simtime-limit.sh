#!/bin/bash

if (( $# != 4 )) ; then
	echo "Error. Usage $0 <minfs> <maxfs> <load> <numflows>";
	exit 1;
fi

# MACS + CRC + TYPE + 8021Q (12+4+2+4) mac header
# Works for Poisson process flow arrival with uniform flow size distribution
echo  "$*" | awk '{
B=100e6;
minfs=$1;
maxfs=$2;
numHost=$5;
avgfs=(maxfs - minfs)/2; 
if(minfs == maxfs) {
 avgfs=minfs;
}
ipH=20; 
transportH=20; 
ethH=22; 	 	
MTU=1500; 
nPkts=avgfs / (MTU-ipH-transportH); 
PCI_overhead=nPkts * (ipH+transportH+ethH); 
totBits=8*(avgfs+PCI_overhead);
fduration=totBits/B; 
load=$3; 
numflows=$4; 
lambda=load*B/totBits; 
simtime=numflows/lambda; 
print "flow duration: " fduration*1e3 "ms";
print "average ia time: " 1/lambda*1e3 "ms";
print "simtime: " simtime*1e3 "ms";
print "num packets per flow: " nPkts;
print "total fs: " totBits;
}' 

